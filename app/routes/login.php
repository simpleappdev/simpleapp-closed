<?php

// Confide URL                                      Controller
Route::get( 'signup',                               'UserController@create');
Route::post('user',                                 'UserController@store');
Route::get( 'login',                                'UserController@login');
Route::post('login',                                'UserController@do_login');
Route::get( 'user/confirm/{code}',                  'UserController@confirm');
Route::get( 'user/forgot_password',                 'UserController@forgot_password');
Route::post('user/forgot_password',                 'UserController@do_forgot_password');
Route::get( 'user/reset_password/{token}',          'UserController@reset_password');
Route::post('user/reset_password',                  'UserController@do_reset_password');
Route::get( 'logout',                               'UserController@logout');