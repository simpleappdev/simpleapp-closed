<?php

class PermissionsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('permissions')->truncate();
        
		\DB::table('permissions')->insert(array (
			0 => 
			array (
				'id' => '1',
				'name' => 'Watch Logs Level 1',
				'display_name' => 'Allows a user to watch Logs with priority 1',
				'created_at' => '2015-01-17 21:44:42',
				'updated_at' => '2015-01-17 21:44:42',
			),
			1 => 
			array (
				'id' => '2',
				'name' => 'Watch Logs Level 2',
				'display_name' => 'Allows a user to watch Logs with priority 2',
				'created_at' => '2015-01-17 21:44:49',
				'updated_at' => '2015-01-17 21:44:49',
			),
			2 => 
			array (
				'id' => '3',
				'name' => 'Watch Logs Level 3',
				'display_name' => 'Allows a user to watch Logs with priority 3',
				'created_at' => '2015-01-17 21:44:56',
				'updated_at' => '2015-01-17 21:44:56',
			),
			3 => 
			array (
				'id' => '4',
				'name' => 'Watch Logs Level 4',
				'display_name' => 'Allows a user to watch Logs with priority 4',
				'created_at' => '2015-01-17 21:45:01',
				'updated_at' => '2015-01-17 21:45:01',
			),
			4 => 
			array (
				'id' => '5',
				'name' => 'Watch Logs Level 5',
				'display_name' => 'Allows a user to watch Logs with priority 5',
				'created_at' => '2015-01-17 21:45:08',
				'updated_at' => '2015-01-17 21:45:08',
			),
			5 => 
			array (
				'id' => '6',
				'name' => 'Invoices',
				'display_name' => 'Allows a user to see the invoices',
				'created_at' => '2015-01-17 21:45:24',
				'updated_at' => '2015-01-17 21:45:24',
			),
			6 => 
			array (
				'id' => '7',
				'name' => 'Manage Users',
				'display_name' => 'Allows a user to manage all Users',
				'created_at' => '2015-01-17 21:45:44',
				'updated_at' => '2015-01-17 21:45:44',
			),
			7 => 
			array (
				'id' => '8',
				'name' => 'Manage Invoices',
				'display_name' => 'Allows a role or user to manage Invoices',
				'created_at' => '2015-01-17 21:50:44',
				'updated_at' => '2015-01-17 21:50:44',
			),
		));
	}

}
