<?php

use Helpers\RenderAppH as RenderApp;

class RenderAppController extends \BaseController {

    public $data = array();

    public function __construct(){

    }
	/**
	 * Display a listing of the resource.
	 * GET /renderapp
	 *
	 * @return Response
	 */
	public function index($id)
	{
        $company = Company::find($id);
		$this->data['company'] = $company;
        $this->data['modules'] = $company->modules;
        $this->data['pages'] = $company->activePages;



        return View::make('app.render.index', $this->data);
	}

    public function save($id){
        $company = Company::find($id);
        $buttons = $company->buttonsXML;
        $buttons->version_number = +1;
        $buttons->update();

        $xml = '<?xml version="1.0" encoding="utf-8"?><buttons>';
        $xml .= '<version>'.$buttons->version_number.'</version>';
        $counter = 0;
        $modules = Input::get('render');

        $counter = 0;
        foreach($modules as $key => $value){
            $counter++;

            if(isset($value['page'])){
                $item_number = $value['page'][0];
                $page = PagesM::find($item_number);
                $xml .= RenderApp::renderPage($page, 'page', $counter);
            }

            if(isset($value['module'])){
                echo 'Module <br />';
                $item_number = $value['module'][0];
                $module = Module::find($item_number);
                $xml .= RenderApp::renderModule($module, $counter);
            }
        }

        $xml .= '</buttons>';

        RenderApp::setMenu($company, $xml);

        return Redirect::to('modules/overview/'.$id.'/');
    }

}