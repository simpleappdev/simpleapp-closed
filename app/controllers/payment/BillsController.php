<?php namespace payment;

use Bill, Company, Redirect, View, Input, Response;

class BillsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /company/bills
	 *
	 * @return Response
	 */
	public function index($company_id)
	{
		$company = Company::find($company_id);

		$open_bills = Bill::where('payed_date', '=', null)->paginate(20);

		return View::make('company.payments.bills.index', ['company' => $company, 'open_bills' => $open_bills]);
	}

	public function payment_history()
	{
		$payed_bills = Bill::where('payed_date', '>', '0000-00-00 00:00:00')->paginate(20);

		return View::make('company.payments.bills.history', ['payed_bills' => $payed_bills]);
	}

	public function download($bill_id){
		$bill = Bill::find($bill_id);

		return Response::download('index.php');

	}

	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /company/bills
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /company/bills/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /company/bills/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /company/bills/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /company/bills/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}