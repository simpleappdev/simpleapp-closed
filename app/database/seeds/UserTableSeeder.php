<?php

// Composer: "fzaninotto/faker": "v1.3.0"


class UserTableSeeder extends Seeder {

	public function run()
	{

		$users[] = array(
			'username' => 'donny5300',
			'email' => 'wavangrondelle@live.nl',
			'password' => Hash::make('admin'),
			'confirmed' => 1,
		);

		$users[] = array(
			'username' => 'admin',
			'email' => 'admin@admin.nl',
			'password' => Hash::make('admin'),
			'confirmed' => 1,
		);

		$users[] = array(
			'username' => 'Dennis',
			'email' => 'd_sweben@hotmail.com',
			'password' => Hash::make('admin'),
			'confirmed' => 1,
		);

		$users[] = array(
			'username' => 'Kevin',
			'email' => 'kkromjong@gmail.com',
			'password' => Hash::make('admin'),
			'confirmed' => 1,
		);

		foreach($users as $item){
			DB::table('users')->insert($item);
			$this->command->info('User created: ' . $item['username']);
		}
		
	}

}