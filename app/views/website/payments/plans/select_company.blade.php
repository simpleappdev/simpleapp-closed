@extends('master')
@section('content')
    <style>

    </style>
    <div class="container" id="payment-view">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Modules in development</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">Soundcloud</a>
                    <a href="#" class="list-group-item">Instagram</a>
                    <a href="#" class="list-group-item">Facebook</a>
                    <a href="#" class="list-group-item">Agenda</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row">
                    <h3 class="text-center">
                        {{{ Session::get('message') != null ? Session::get('message') : Lang::get('general.choosen_plan') }}}
                    </h3>
                    <hr>
                    <div class="col-sm-4">
                        <div class="panel panel-plan panel-danger text-center">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{$plan->name}}</h3>
                            </div>
                            <div class="panel-body">
                                <h3 class="panel-title price">&euro; {{$plan->price}}<span class="price-month">{{Lang::get('general.a_month')}}</span></h3>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item">1 app</li>
                                <li class="list-group-item">5 different themes</li>
                                <li class="list-group-item">1 user</li>
                                <li class="list-group-item">5 push notification a month</li>
                                <li class="list-group-item">E-mail Support</li>
                                <li class="list-group-item"><a href="{{url('plans/'.$plan->id)}}" class="btn btn-danger">Sign Up Now!</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{Lang::get('general.select_company')}}</h3>
                            </div>
                        </div>
                            {{$company_list}}
                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
