@extends('admin.template.master')
@section('admin_content')

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">{{Lang::get('permissions.PermissionHeader');}}</h1>
            </div>


            <div class="col-sm-12 col-lg-12 col-md-12">

                <h2>{{Lang::get('permissions.PermissionShow')}}</h2>

                <a href="{{URL::route('admin.permissions.index') }}" class="btn btn-default">{{Lang::get('permissions.PermissionBackButton')}}</a>

                <div class="table-responsive">
                    <table class="table table-bordered" border="1">
                        <tr>
                            <td>{{Lang::get('permissions.PermissionId');}}</td>
                            <td>{{Lang::get('permissions.PermissionName');}}</td>
                            <td>{{Lang::get('permissions.PermissionDescription');}}</td>
                            <td>{{Lang::get('permissions.PermissionCreatedAt');}}</td>
                            <td>{{Lang::get('permissions.PermissionUpdatedAt');}}</td>
                        </tr>

                        <tr>
                            <td>{{ $permission->id }}</td>
                            <td>{{ $permission->name }}</td>
                            <td>{{ $permission->display_name }}</td>
                            <td>{{ $permission->created_at }}</td>
                            <td>{{ $permission->updated_at }}</td>
                        </tr>
                </table>
            </div>
        </div>
    </div>
@endsection