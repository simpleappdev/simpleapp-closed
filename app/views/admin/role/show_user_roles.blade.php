@extends('master')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3">
            @include('admin.defaults.sidemenu')
        </div>

	    <div class="col-sm-9 col-lg-9 col-md-9">
	    	<h1>Alle rollen die de gebruiker {{$user->username . '('. $user->id .')' }} beheerd</h1>
	    	
	    </div>
	</div>
</div>
@endsection