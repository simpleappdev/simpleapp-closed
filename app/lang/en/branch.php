<?php

return array (
  'branchHeader' => 'Branches',
  'branchCreateButton' => 'Create Branch',
  'branchId' => 'ID',
  'branchDescription' => 'Description',
  'branchLanguage' => 'Language',
  'branchCreatedAt' => 'Created At',
  'branchUpdatedAt' => 'Updated at',
  'branchActions' => 'Actions',
  'branchCreate' => 'Create',
  'branchDescriptionName' => 'Title',
  'branchLabelLanguage' => 'Language',
  'branchBackButton' => 'Return to branches',
  'branchEdit' => 'Edit Branch',
);
