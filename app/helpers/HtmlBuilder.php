<?php namespace Helpers;


use Company;
use Theme;
use File;
/**
 * Class to build the HTML and CSS page
 **/

class HtmlCreator
{
    public $file_name;
    public $company_slug;
    public $theme;

    public function __construct($company, $file_name){

        //$company        = Company::find($company_id);
        $this->file_name = $file_name;

        $this->company_slug = $company->slug;
        $get_theme = Theme::where('default', '=', 1)->where('company_id', '=', $company->id)->firstOrFail();
        $this->theme    = $get_theme->file;
        //dd($get_theme->file);

    }

    /**
     * Create basics for HTML page
     */

    public static function html($data, $css = false){
        if($css == false){
            $css = '* {margin:0px; padding: 0px;}';
        }
        $html = '';
        $html .= '<html>';
        $html .= '<head><title></title><style>'.$css.'</style></head>';
        $html .= '<body>';
        $html .= $data;
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }
}