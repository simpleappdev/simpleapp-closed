<?php

class Theme extends \Eloquent {
	protected $fillable = ['name', 'default'];

	public function company(){
		return $this->belongsTo('Company');
	}

    public function pages(){
        return $this->belongsToMany('Page');
    }
}