$(document).ready(function(){

    /*
     * Hack to keep correct width (parent width) upon affix
     *
     * Todo: fix bug after affix-bottom to affix
     */
    var $affix  = $('#preview-panel');
    var $col    = $('#preview-column');

    var fixaffix = function() {
        $affix.width( $col.width() );
    }

    $('#preview-panel').on('affix.bs.affix', fixaffix);
    $('#preview-panel').on('affix-bottom.bs.affix', fixaffix);
    $('#preview-panel').on('affix-top.bs.affix', fixaffix);

    // AFFIX

    $('#preview-panel').affix({
        offset: {
            top: 60
            , bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    });

    var     selector,
        preview,
        rgbcolor;

    $('#article-header, #article-paragraph, #article-article').click(function(){
        selector = $(this).attr('id');
    });

    $('.color-picker-font').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {

            rgbcolor = rgb.r + ',' + rgb.g + ',' + rgb.b;
            //$(el).css('background','#'+hex);

            //$('.'+selector + '-input #font-color').css({background: '#'+hex});
            $('.'+selector).css({color: '#'+hex});

            $(el).siblings('span').css({background: '#' + hex});

            // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
            if(!bySetColor) $(el).val(rgbcolor);
        }
    });

    $('.color-picker-background').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {

            rgbcolor = rgb.r + ',' + rgb.g + ',' + rgb.b;
            //$(el).css('background','#'+hex);
            $(el).siblings('span').css({background: '#' + hex});

            createBackground(selector);

            // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
            if(!bySetColor) $(el).val(rgbcolor);
        }
    });

    $('.color-picker').colpick({
        layout:'hex',
        submit:0,
        colorScheme:'dark',
        onChange:function(hsb,hex,rgb,el,bySetColor) {
            var position = $(this).parents('.theme-builder-row').data('position');

                rgbcolor = rgb.r + ',' + rgb.g + ',' + rgb.b;
            //$(el).css('background','#'+hex);

        //    $('.'+selector + '-input').css({border: '#'+hex});
        //    $('.'+selector).css({color: '#'+hex});
            createBorders(selector);
            $(el).siblings('span').css({background: '#' + hex});

            // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
             if(!bySetColor) $(el).val(rgbcolor);
        }
    });

    $('.image-bg, .x-alignment, .y-alignment, .image-repeat').change(function(){
        createBackground(selector);
    });

    $('.border-radius').change(function(){
        createBorderRadius(selector);
    });

    $('.padding-selection').change(function(){
        var type='padding';
        createSpacing(selector, type);
//        console.log('A change has been made');
    });

    $('.margin-selection').change(function(){
        createSpacing(selector, 'margin');
//        console.log('A change has been made');
    });

    $('.generate-borders').change(function(){
        createBorders(selector);
//        console.log('Border changes')
    });

    $('.font-size').change(function(){
        $('.'+selector).css({'font-size': $(this).val() + 'px'});
    });

    $('.font-weight').change(function(){
        if($(this).val() == 'bold' || $(this).val() == 'bolder'){
            $('.'+selector).css({'font-weight': $(this).val()});
            $('.'+selector).css({'font-style': ''});
//            console.log('bold');
        } else if($(this).val() == 'italic') {
            $('.'+selector).css({'font-style': $(this).val()});
            $('.'+selector).css({'font-weight': ''});
//            console.log('Italic')
        } else {
            $('.'+selector).css({'font-style': ''});
            $('.'+selector).css({'font-weight': ''});
        }

    });

    /*
     * @var second var is selection for margin, padding
     */

    function createSpacing(selector, type){
        var     result = '',
            counter = 0;
        $('#'+selector+'-input .'+type+'-selection').each(function(){
            if(counter != 4){
                result += $(this).val() + 'px ';
            } else {
                result += $(this).val() + 'px;';
            }
//            console.log('each')
        });

        if(type == 'margin')
        {
            $('.'+selector).css({margin: result});
        } else if (type == 'padding')
        {
            $('.'+selector).css({padding: result});
        }
    }

    function createFontSize(selector){

    }

    function createBorderRadius(selector){
        var     radius = '',
                counter = 0;
        $('#'+selector+'-input .border-radius').each(function(){
            if(counter != 4){
                radius += $(this).val() + 'px ';
            } else {
                radius += $(this).val() + 'px;';
            }
        });

        $('.'+selector).css({borderRadius: radius});
    }

    function createBackground(selector){
        var backgroundImage     = $('#' + selector + '-input .image-bg').val(),
            xAlign              = $('#' + selector + '-input .x-alignment').val(),
            yAlign              = $('#' + selector + '-input .y-alignment').val(),
            repeat              = $('#' + selector + '-input .image-repeat').val(),
            imageColor          = $('#' + selector + '-input .color-picker-background').val(),
            backgroundImageURL  = '';

        if (backgroundImage != 0){
            backgroundImageURL = 'url('+backgroundImage+')';
        }

        $('.'+selector).css({background: 'rgba(' + imageColor + ', 1) ' + backgroundImageURL + ' ' + xAlign + ' ' + yAlign + ' ' + repeat});
    }

    /**
     * Function to generate the borders for each position: left, right, top, bottom
     * @param selector      Selector will get his value when clicking on the Collapsible menu
     */
    function createBorders(selector){
        $('#'+selector + '-input .border-fields').each(function(){

            var $position = $(this).attr('data-position'),//Data-position bepaald of het de linker, rechter, bottom of top border is
                borderStyle = $(this).find("[data-border-style]").val(),
                borderWidth = $(this).find("[data-border-width]").val(),
                borderColor = $(this).find("[data-border-color]").val();

            console.log('Width: ' + borderWidth);
            console.log('Style: ' + borderStyle);
            console.log('Color: ' + borderColor);

            $('.'+selector).css('border-'+$position, borderWidth + 'px ' + borderStyle + ' rgb(' + borderColor+')');

        });
    }

    $('#screener').delay(10).animate({opacity: 0}, 100, function(){
        $(this).remove();
    });
});