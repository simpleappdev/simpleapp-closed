<?php

class Gallery extends \Eloquent {

	protected $fillable = ['title', 'company_id', 'date', 'description', 'active'];

    protected $table = 'module_galleries';

    public function getDates(){
        return ['date', 'created_at'];
    }

    public function setDateAttribute($end) {
        if ($end) {
            $this->attributes['date'] = date('Y-m-d H:i:s',(strtotime($end)));
        } else {
            $this->attributes['date'] = '';
        }
    }

    public function getPhotos(){
        return $this->hasMany('Photos');
    }
}