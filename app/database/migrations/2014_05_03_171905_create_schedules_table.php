<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schedules', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->string('description');
			$table->integer('company_id');
            $table->string('location');
			$table->dateTime('start');
			$table->dateTime('end');
			$table->timestamps();
			
		});

		Schema::create('company_schedule', function($table)
        {
            $table->increments('id')->unsigned();
            $table->integer('schedule_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies'); // assumes a users table
            $table->foreign('schedule_id')->references('id')->on('schedules');
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('company_schedule', function(Blueprint $table) {
            $table->dropForeign('company_schedule_company_id_foreign');
            $table->dropForeign('company_schedule_schedule_id_foreign');
        });
        
		Schema::drop('company_schedule');
		Schema::drop('schedules');
	}

}
