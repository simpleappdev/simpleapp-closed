<?php
/*
 * Routes for the homepage
 *
 */

//Is the users click on the "read more" text in a newsitem, redirect to item page
Route::get('/news/{any}', function()
{
    return View::make('website.home.item');
});