{{--
@Requires:
name          => string
value         => array
styles        => array

@Missing:
name attribute on input group
--}}
<div class="form-group generate-borders">
    <label class="theme-builder-label" for="">
        Border {{ ucfirst($position) }}
    </label>

    <div class="theme-builder-row border-fields form-inline" data-position="{{ $position }}">
        <!--<input type="number"  min="0" max="20"  data-border="width" name="{{ $input_name }}[border][{{ $position }}][width]">-->

        {{Form::input('number',$input_name. '[border]['. $position . '][width]',
        isset($value[$position]['width']) ? $value[$position]['width'] : 0, [
            'class' => 'border-field-value form-control border-width input-sm',
            'data-border-width',
        ]
        )}}
        {{ Form::select(
            $input_name. '[border]['. $position . '][style]',
            ['solid' => 'Solid', 'dotted' => 'Dotted', 'dashed' => 'Dashed'],
            $value[$position]['style'],
            ['class' => 'border-field-value border-style form-control input-sm', 'data-border-style' => '']
        ) }}
        {{Form::text($input_name. '[border]['. $position . '][color]',
        isset($value[$position]['color']) ? $value[$position]['color'] : '0,0,0',
        [
            'class' => 'border-field-value form-control input-sm color-picker',
            'data-border-color'
        ])}}


        <span class="input-group-addon color-picker-addon"></span>

    </div>
</div>
