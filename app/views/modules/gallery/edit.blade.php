@extends('master')
@section('content')
<style>
    textarea {
        width: 100%;
        margin-top: 10px;
    }

    .clear {
        clear: both;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('includes.manage_module')
            @include('includes.modules_menu')
        </div>

        <div class="col-sm-4 col-lg-8 col-md-4">

            @include('_partials.item_created')

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Album</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center">
                            <img alt="User Pic" src="{{url('images/modules/camera_logo.png')}}" class="img-responsive"> </div>

                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                {{Form::open()}}

                                <tr>
                                    <td>Title</td>
                                    <td>{{Form::text('title', $gallery->title)}}</td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>{{Form::text('date', $gallery->date)}}</td>
                                </tr>
                                <tr>
                                    <td>Active</td>
                                    <td>{{Form::select('active', array('' => 'Make your choice', 1 => 'Yes', 0 => 'No'), $gallery->active)}}</td>
                                </tr>



                                </tbody>
                            </table>
                            @include('_partials.errors')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            {{Form::textarea('description', '', ['placeholder' => 'Tell something about this album or leave blank for no description'])}}
                        </div>
                        <div class="col-lg-12">
                            {{Form::submit('Add')}}
                            {{Form::close()}}

                        </div>
                    </div>
                </div>
                {{--
                <div class="panel-footer">
                    <p>
                        {{Lang::get('form.signup_attention_new_company')}}
                    </p>
                </div>--}}

            </div>
            <!-- </div> -->

        </div>
    </div>
</div>

@endsection
