<?php

return array (
  'show_single_bill' => 'Show single bill',
  'bill_generated' => 'Invoice generated',
  'payed_date' => 'Payed date',
  'price' => 'Price',
  'open_bills' => 'Open invoices',
  'open_bills_price' => 'Totall amount',
);
