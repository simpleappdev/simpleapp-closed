<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {


	public function up()
    {
		Schema::create('companies', function($table) {
			$table->increments('id')->unsigned();
			$table->string('company_name');
			$table->text('information');
            $table->string('slug')->unique();
            $table->integer('branche_id');
            $table->integer('owner_id');
			$table->string('folder_location');
			$table->integer('active');
			$table->timestamps();
		});

		Schema::create('company_user', function($table)
        {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users'); // assumes a users table
            $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
        });
	}

	/**
     * Reverse the migrations.
     *
     * @return void
     */

	public function down(){
       
		Schema::table('company_user', function(Blueprint $table) {
            $table->dropForeign('company_user_user_id_foreign');
            $table->dropForeign('company_user_company_id_foreign');
        });

		Schema::drop('companies');
       	Schema::drop('company_user');
	}
}