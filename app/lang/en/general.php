<?php

return array (
  'news_created' => 'News created',
  'plan_saved' => 'The plan for company :company is saved',
  'select_choice' => 'Make your choice',
  'modules_saved' => 'Modules saved!',
  'event_item_saved' => 'Event saved!',
  'no_item_access' => 'You don\'t have the right permissions for this action.',
  'no' => 'No',
  'yes' => 'Yes',
  'save' => 'Save',
  'add_new_company' => 'Add new company',
  'company_name' => 'Company name',
  'branch' => 'Branch',
  'owner' => 'Owner',
  'email' => 'E-mail',
  'phone_number' => 'Phone number',
  'address' => 'Address',
  'city' => 'City',
  'country' => 'Country',
  'zipcode' => 'Zipcode',
  'status' => 'Status',
  'company_slug' => 'Company slug',
  'active' => 'Active',
  'published' => 'Published',
  'make_your_choice' => 'Make your choice',
  'select_language' => 'Select a language',
  'english' => 'English',
  'dutch' => 'Dutch',
  'created_at' => 'Created at',
  'app_status' => 'App status',
  'not_yet_pushed_to_store' => 'Not released',
  'pushed_to_store' => 'To app store',
  'app_waiting_approvement' => 'Waiting for approval',
  'app_rejected' => 'Rejected',
  'status_unknown' => 'Unknown',
  'download' => 'Download',
  'choosen_plan' => 'Choosen plan',
  'a_month' => 'Month',
  'select_company' => 'Select a company',
  'company_owner' => 'Company owner',
  'switching_to' => 'Switching to',
  'already_have_current_plan' => 'You already have this plan',
  'change_plan_to' => 'Change plan to',
  'switch_not_possible' => 'Switching is not possible',
  'no_companies' => 'You don\'t manage any companies',
  'login_for_plan_select' => 'Please login to select your plan',
  'to' => 'To',
  'description' => 'Description',
  'succeeded' => 'Succeeded',
  'cancel' => 'Cancel',
  'delete' => 'Delete',
  'name' => 'Name',
  'available' => 'Available',
  'in_use' => 'In use',
  'manage' => 'Manage',
  'add_event' => 'Add event',
  'start_date' => 'Start date',
  'end_date' => 'End date',
  'location' => 'Location',
  'replace' => 'Replace',
  'add' => 'Add',
  'last_created_item' => 'Last created',
  'title' => 'Title',
  'actions' => 'Actions
',
  'edit_event' => 'Edit event',
  'edit' => 'Edit',
  'validation_error' => 'Validation error',
  'update' => 'Update',
  'delete_item' => 'Delete item',
  'confirm_delete_item' => 'Are you sure that you want to delete this item',
  'action_not_allowed' => 'This action is not allowed',
  'user' => 'User',
  'welcome_back' => 'Welcome back',
  'info_about_dashboard' => 'This is your personal Dashboard. We created a overview with everything that might be important for you to keep a close eye on.',
  'last_info_about_modules' => 'Modules',
  'read_more' => 'Read more',
  'logbook_info' => 'Logs',
  'invoices_information' => 'Invoices',
  'my_account' => 'My Account',
  'my_settings' => 'My Settings',
  'my_activities' => 'My Activities',
  'show_more' => 'Show more',
  'first_name' => 'First name',
  'surname' => 'Last name',
  'website' => 'Website',
  'member_since' => 'Member since',
  'change_password' => 'Change your password',
  'save_profile' => 'Save profile',
  'edit_profile' => 'Edit your profile',
  'profile_updated' => 'Your account has been updated!',
  'updated_title' => 'Updating successfull!',
  'password_updated' => 'Your new password has been saved!',
  'new_password' => 'New password',
  'repeat_password' => 'Repeat password',
  'current_password' => 'Current password',
);
