<?php

return array (
  'my_account' => 'Mijn account',
  'login' => 'Inloggen',
  'signup' => 'Registreren',
  'change_photo' => 'Wijzig foto',
  'change_password' => 'Wijzig wachtwoord',
  'logout' => 'Uitloggen',
  'website' => 'Website',
  'password' => 'Wachtwoord',
  'repeat_password' => 'Herhaal wachtwoord',
  'create_account' => 'Account aanmaken',
  'email' => 'E-mail',
  'first_name' => 'Voornaam',
  'surname' => 'Achternaam',
  'username' => 'Gebruikersnaam',
);
