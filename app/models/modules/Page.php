<?php

class Page extends \Eloquent {

    protected $softDelete = true;

    protected $fillable = ['titel', 'pagename', 'theme','company_id', 'active', 'destroyed', 'content', 'personal_description'];

    protected $table = 'module_pages';

    public function getTheme(){
        return $this->belongsTo('Theme', 'theme_id');
    }

}