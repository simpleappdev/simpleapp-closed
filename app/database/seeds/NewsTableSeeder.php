<?php

class NewsTableSeeder extends Seeder {

    public function run()
    {

        $news_items = array();
        $news_items[] = array(
            'title' => 'This is a title',
            'content' => 'This is the body text',
            'language' => 'English',
        );

        $news_items[] = array(
            'title' => 'Dit is een titel',
            'content' => 'Dit is de body tekst',
            'language' => 'Dutch',
        );
        foreach($news_items AS $item){
            DB::table('news')->insert($item);
        }

    }

}