{{--
@Requires:
    name          => string
    description   => string
    value         => string
    input_field   => string
--}}
<div class="form-group">
    <label class="theme-builder-label" for="{{ $name }}[background-color]">{{{ $description }}}</label>
    <div class="theme-builder-row form-inline">
        <div class="input-group">
            <input type="text" class="color-picker-background form-control input-sm" value="{{ $value }}" name="{{ $name }}[background-color]">
            <span class="input-group-addon color-picker-addon"></span>
        </div>
    </div>
</div>