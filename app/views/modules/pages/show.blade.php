@extends('master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <p class="lead">Shop Name</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">Printable</a>
                    <a href="#" class="list-group-item">Cupcake Wrappers</a>
                    <a href="#" class="list-group-item">Authentic Dragon Bones</a>
                </div>
            </div>

            <div class="col-md-9">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <tr class="info">
                            <th class="col-md-2">Name</th>
                            <th class="col-md-1">Active</th>
                            <th>Information</th>
                            <th class="col-md-2">Action</th>
                        </tr>
                        @foreach($pages AS $page)
                            <tr>
                                <td class="module-page-title">{{ $page->titel }}</td>
                                @if($page->active == null)
                                    <td>No</td>
                                @else
                                    <td>Yes</td>
                                @endif
                                <td>{{ str_limit($page->content, 50) }}</td>
                                <td class="text-center">

                                    {{ Bootstrap::linkRoute(
                                        'company.modules.pages.show',
                                        '',
                                        [$page->id],
                                        ['class' => 'btn btn-sm btn-default glyphicon glyphicon-eye-open', 'title' => 'View']
                                    ) }}
                                    {{ Bootstrap::linkRoute(
                                        'company.modules.pages.edit',
                                        '',
                                        [$page->id],
                                        ['class' => 'btn btn-sm btn-primary glyphicon glyphicon-pencil', 'title' => 'Edit']
                                    ) }}
                                    {{ Form::open([
                                        'route' => ['company.modules.pages.destroy', $page->id],
                                        'method' => 'delete',
                                        'style' => 'display: inline;'
                                    ]) }}
                                        {{ Bootstrap::button(
                                            '',
                                            ['class' => 'btn btn-sm btn-danger glyphicon glyphicon-trash confirmDelete', 'title' => 'Delete']
                                        ) }}
                                    {{ Form::close() }}
                                    <!-- <a href="">Bewerken</a> - <a href="">Verwijderen</a> -->

                               </td>
                            </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
<div id="confirmDeleteModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header bg-danger">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title" id="myModalLabel">{{Lang::get('general.delete_item')}}</h3>
      </div>

      <div class="modal-body text-danger">
        {{Lang::get('general.confirm_delete_item')}}
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete">{{Lang::get('general.delete')}}</button>
        <button type="button" data-dismiss="modal" class="btn btn-primary">{{Lang::get('general.delete')}}</button>
      </div>

    </div>
  </div>
</div>
    {{HTML::script('template/front/js/scripts/modules/pages_show.js')}}
@endsection