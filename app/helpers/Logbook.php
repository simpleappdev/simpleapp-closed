<?php namespace Helpers;

use Auth;
use App;
use Logbook;
use Log;
use Company;
use Exception;

class LogbookH
{

    public static function createLog($event, $company_id, $action, $item_id, $system_log = 0)
    {

        $user = (Auth::check() ? Auth::user()->id : 0);

        $log['event'] = $event;
        $log['company_id'] = $company_id;
        $log['action'] = $action;
        $log['item_id'] = $item_id;
        $log['user_id'] = $user;
        $log['system_log'] = $system_log;


        try {
            Logbook::create($log);
        } catch (Exception $e) {
            App::error(function(PDOException $exception)
            {
                Log::error("Error connecting to database: ".$exception->getMessage());

                return "Error connecting to database";
            });
        }
    }
}