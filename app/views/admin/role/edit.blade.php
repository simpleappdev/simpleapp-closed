@extends('admin.template.master')
@section('admin_content')

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">{{Lang::get('roles.RoleHeader')}}</h1>
            </div>

            <div class="col-sm-4 col-lg-4 col-md-4">

                <h2>{{Lang::get('roles.RoleEdit')}}</h2>

                {{ Form::model($role, ['route' => ['admin.roles.update', $role->id], 'method' => 'PUT'])}}
                {{ Form::label('role_name', Lang::get('roles.RoleLabelName'))}}
                {{ Form::text('name', null, ['class' => 'form-control'])}}

                {{ Form::Label('display_name', Lang::get('roles.RoleLabelDescription'))}}
                {{ Form::text('description', null, ['class' => 'form-control'])}}

                <h2>Choose permissions</h2>

                <p>All permissions</p>
                @foreach($permissions as $permission)
                    <div class="checkbox">
                        <label>
                        @if(in_array($permission->id, $assigned_perms))
                            {{Form::checkbox('permission[' . $permission->id . ']', $permission->name, true)}}
                        @else
                            {{Form::checkbox('permission[' . $permission->id . ']', $permission->name)}}
                        @endif
                            {{ucfirst($permission->name)}}
                        </label>
                    </div>
                @endforeach

                {{ Form::submit(Lang::get('roles.RoleEdit'), ['class' => 'btn btn-default'])}}
                {{ Form::close()}}

            </div>
        </div>
    </div>
@endsection