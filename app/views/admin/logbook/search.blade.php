@extends('admin.template.master')
@section('admin_content')

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Logs</h1>
            </div>

            <div class="col-lg-11">
                <a class="btn btn-default" href="{{URL::previous()}}">{{Lang::get('logbook.back_button')}}</a>
            </div>
            <div class="col-lg-11">
                <h2>Result</h2>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{Lang::get('logbook.tableheader_hashtag')}}</th>
                            <th>{{Lang::get('logbook.tableheader_user')}}</th>
                            <th>{{Lang::get('logbook.tableheader_company')}}</th>
                            <th>{{Lang::get('logbook.tableheader_item')}}</th>
                            <th>{{Lang::get('logbook.tableheader_event')}}</th>
                            <th>{{Lang::get('logbook.tableheader_action')}}</th>
                            <th>{{Lang::get('logbook.tableheader_time')}}</th>
                        </tr>
                        </thead>

                        <tbody class="table-striped">
                        @foreach($logs as $log)
                            <tr>
                                <td>{{$log->id}}</td>
                                <td>{{$log->user_id}}</td>
                                <td>{{$log->company_id}}</td>
                                <td>{{$log['attributes']['item_id']}}</td>
                                <td>{{$log['attributes']['event']}}</td>
                                <td>{{$log['attributes']['action']}}</td>
                                <td>{{$log['attributes']['created_at']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>

@endsection