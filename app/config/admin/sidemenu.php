<?php

return [
    'links' => [
        'roles'         => Lang::get('admin.menu_item_roles'),
        'permissions'   => Lang::get('admin.menu_item_permissions'),
        'users'         => Lang::get('admin.menu_item_users'),
        'companies'     => Lang::get('admin.menu_item_companies'),
        'news'          => Lang::get('admin.menu_item_news'),
        'logbook'       => Lang::get('admin.menu_item_logbook'),
        'branches'      => Lang::get('admin.menu_item_branches')
    ]
];
