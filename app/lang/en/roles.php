<?php

return array (
  'RoleHeader' => 'Roles',
  'RoleCreateButton' => 'Create role',
  'RoleId' => 'ID',
  'RoleName' => 'Role Name',
  'RoleDescription' => 'Description',
  'RoleCreated' => 'Created at',
  'RoleUpdated' => 'Updated at',
  'Actions' => 'Actions',
  'RoleCreate' => 'Create role',
  'RoleLabelName' => 'Role Name',
  'RoleLabelDescription' => 'Give a short description about the role',
  'RoleShow' => 'Show Role',
  'RoleBackButton' => 'Back to roles',
  'RoleEdit' => 'Edit role',
);
