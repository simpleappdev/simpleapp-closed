<?php namespace modules;

use Input;
use Redirect;
use Company;
use View;
use Helpers\Helpers;
use Validator;
use Gallery;
use \Services\Validators\PhotoV;

class PhotoController extends \BaseController {

    public $data = array();

    public function __construct(){
        $this->module_id = 5;
        $this->data['module_id'] = $this->module_id;
        $this->data['module_name'] = 'photo';
    }
	/**
	 * Display a listing of the resource.
	 * GET /photo
	 *
	 * @return Response
	 */
	public function index($album_id)
	{
        $gallery = Gallery::find($album_id);
        $company = Company::find($gallery->company_id);

        $autoGenerate = Helpers::relationToArray($company->autogenerateModules, $this->module_id);

        $this->data['gallery'] = $gallery;
        $this->data['company'] = $company;
        $this->data['autogenerate'] = $autoGenerate;

		return View::make('modules.gallery.photo.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /photo/create
	 *
	 * @return Response
	 */
	public function create($gallery_id)
	{

        $gallery = Gallery::find($gallery_id);
        $company = Company::find($gallery->company_id);

        $this->data['gallery'] = $gallery;
        $this->data['company'] = $company;

        return View::make('modules.gallery.photo.create', $this->data);
	}

	public function delete($id){
		$image = Photos::find($id);

		$image->deleted = 1;
		$image->save();

		//return Redirect::back();
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /photo
	 *
	 * @return Response
	 */
	public function store($gallery_id)
	{
        $validator = Validator::make(Input::all(), PhotoV::rules());

        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $image = Input::file('image');

        $album = Gallery::find($gallery_id);

        $exceptions = array(" ", ':', "!", "@", "+", "/", "\'", "*", ".",);
        $album_title = str_replace($exceptions,"_",strtolower(trim($album->title).trim(date("Y-m-d H:i:s"))));

        $image_name = $album_title.'.jpg';

        $photo = new Photos;

        $input = Input::all();
        $input['company_id'] = $album->company_id;
        $input['gallery_id'] = $gallery_id;
        $input['added_by'] = Auth::user()->id;


        $photo->create($input);

        //Square thumb
        Image::make($image->getRealPath())->resize(75,75,true,false)->save('images/photo_albums/square/'.$image_name);

        //Small
        Image::make($image->getRealPath())->resize(240,240,true,false)->save('images/photo_albums/small/'.$image_name);

        //Medium
        Image::make($image->getRealPath())->resize(460,460,true,false)->save('images/photo_albums/medium/'.$image_name);

        //Original
        Image::make($image->getRealPath())->save('images/photo_albums/original/'.$image_name);

        return $this->index($id);
	}

	/**
	 * Display the specified resource.
	 * GET /photo/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /photo/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /photo/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /photo/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}