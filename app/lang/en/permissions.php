<?php

return array (
  'PermissionHeader' => 'Permissions',
  'PermissionCreate' => 'Create permission',
  'PermissionLabelName' => 'Name of permission',
  'PermissionPlaceholderName' => 'Name',
  'PermissionLabelDisplayname' => 'Displayname of permission',
  'PermissionPlaceholderDisplayame' => 'Permission description',
  'PermissionSubmitButton' => 'Create permission',
  'PermissionEdit' => 'Edit permission',
  'PermissionCreateButton' => 'Create',
  'PermissionId' => '#',
  'PermissionName' => 'Name',
  'PermissionDescription' => 'Description',
  'PermissionCreatedAt' => 'Created at',
  'PermissionUpdatedAt' => 'Updated at',
  'PermissionAction' => 'Actions',
  'PermissionShow' => 'Show permission',
  'PermissionBackButton' => 'Back',
  'PermissionCreated' => 'Created',
  'PermissionUpdated' => 'Updated',
  'PermissionPlaceholderDisplayname' => 'Displayname',
);
