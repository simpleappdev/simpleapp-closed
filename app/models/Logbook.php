<?php

class Logbook extends \Eloquent
{
    protected $fillable = ['user_id', 'company_id', 'item_id', 'event', 'action', 'system_log', 'deleted_at'];

    protected $table = 'logbook';

    public function getDates(){
        return ['created_at', 'deleted_at'];
    }

    public function company(){
        return $this->belongsTo('Company');
    }

    public function user(){
    	return $this->belongsTo('User');
    }

}