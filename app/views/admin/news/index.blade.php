@extends('admin.template.master')
@section('admin_content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">News</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Context Classes
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td>{{Lang::get('news.item')}}</td>
                            <td>{{Lang::get('news.title')}}</td>
                            <td>{{Lang::get('news.audience')}}</td>
                            <td>{{Lang::get('news.body')}}</td>
                            <td>{{Lang::get('news.created_at')}}</td>
                            <td>{{Lang::get('news.image')}}</td>
                            <td>{{Lang::get('news.actions')}}</td>
                        </tr>

                        @foreach($news as $item)
                        <tr class="{{$item->published == false ? 'danger' : 'success'}}">
                            <td>{{$item->id}}</td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->audience}}</td>
                            <td>{{Str::limit($item->body, 40)}}</td>
                            <td>{{$item->created_at->format('d-m-Y')}}</td>
                            <td><img src="{{$item->image_thumbnail}}"/></td>
                            <td><a href="{{URL::route('admin.news.show', $item->id)}}" class="btn btn-sm btn-default fa fa-info"></a></td>
                            <td><a href="{{URL::route('admin.news.edit', $item->id)}}" class="btn btn-sm btn-default fa fa-pencil"></a></td>

                            {{ Form::open(['route' => ['admin.news.destroy', $item->id]])}}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <td><button class="btn btn-sm btn-info fa fa-trash"></button></td>
                            {{ Form::close() }}
                            @endforeach
                        </tr>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                {{$news->links()}}
            </div>
        </div>
        <!-- /.panel -->
    </div>
</div>
@endsection