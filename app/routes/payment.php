<?php



/**
 * Routes for unauthenticated users.
 */

Route::get('plans/go-pro', 'PlansController@index');

/**
 * Routes for authenticated users.
 */

Route::group(['before' => 'auth'], function(){
    Route::get('plans/{plan_id}', 'PlansController@subscribe');
    Route::get('plans/{plan_id}/{company_id}', 'PlansController@subscribe_confirm');

});