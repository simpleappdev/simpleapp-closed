@extends('master')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
{{HTML::script('template/front/js/scripts/app_render.js')}}


<div class="container">
    <div class="col-lg-3">
        @include('includes.modules_menu')
    </div>


    <div class="col-md-9">
        {{Form::open()}}
        <header class="page-header">
            <h1 class="page-title">You're almost there!</h1>

        </header>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Title</h3>
            </div>
            <div class="panel-body">
                <?php $counter = 0; ?>
                <ul id="sortable">

                    @foreach($modules as $module)
                    <?php $counter++ ?>
                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        {{Form::checkbox('render['.$counter.'][module][]', $module->id)}}
                        {{$module->id}}
                        {{$module->title}}
                    </li>
                    @endforeach

                    @foreach($pages as $page)
                    <?php $counter++ ?>
                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        {{Form::checkbox('render['.$counter.'][page][]', $page->id)}}
                        {{$page->id}}
                        {{$page->titel}}
                    </li>
                    @endforeach
                </ul>
            </div>



            <div class="panel-footer">
                {{Lang::get('general.render_your_app')}}


                {{Form::submit('Save')}}
                {{Form::close()}}
            </div>

        </div>
    </div>
</div>
<!-- /.container -->

@endsection

