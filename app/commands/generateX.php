<?php


use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class generateX extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'xml:companylist';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate a XML Company list for the iPhone App (cronjob)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//Find all the companies
		$companies = Company::all();

		//Set counter start at zero
		$counter = 0;

		//Start for the XML
		$result = '<?xml version="1.0"?>'.PHP_EOL;
		$result .= '<companies>';
		foreach($companies AS $company){
			//Generate folders
			if($company->folder_created == 0){
				$dir = '/home/donny/domains/bedrijvenadministratie.nl/public_html/xml/companies/'.$company->slug;
				if (!is_dir($dir)) {
				    @mkdir($dir);
				}
				if(!is_file($dir.'/home.xml')){
					file_put_contents($dir.'/home.xml', $result);
				}
			}

			//Generate XML
			$counter++;
			$result .= '
	<company'.$counter.'>
		<name>'.$company->company_name.'</name>
		<uid>'.$company->slug.'</uid>
		<description>'.$company->description.'</description>
		<coords>51.12312,004.123123</coords>
	</company'.$counter.'>';
		}
		//echo $result;
		$result .= '
</companies>';
		//Path where the XML file should be standing and is
		$current_xml = '/home/donny/domains/bedrijvenadministratie.nl/public_html/xml/company_list/company_list.xml';

		//Check if the file exist
		if(file_exists($current_xml)){
			rename($current_xml, '/home/donny/domains/bedrijvenadministratie.nl/public_html/xml/company_list/old'.date('d-m-Y_H:i:s').'.xml');
			file_put_contents($current_xml, $result);
			$this->info('Found file! Write');
		} else {
			if(!@file_put_contents($current_xml, $result)){
				$this->error('Kon XML niet genereren');
			} else {
				$this->info('De XML bestond niet! Is nu aangemaakt');
			}
		}
		$this->info('Cronjob is executed!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
