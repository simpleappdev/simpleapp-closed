@extends('master')
@section('content')
{{HTML::style('css/website/themebuilder.css')}}
<div id="screener">
    <div id="loading">
        <p>Please wait...</p>
        <p>The themebuilder is loading...</p>

    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <p class="lead">Options</p>
            <div class="list-group">
                <span class="list-group-item active sidebar-item" id="theme-settings">Settings</span>
                <span class="list-group-item sidebar-item" id="theme-block">Theme</span>
                <span class="list-group-item sidebar-item" id="theme-save">Save</span>
            </div>
        </div>

        {{ Form::open(['role' => 'form']) }}


        <div class="col-md-5" id="preview-column">

            <div id="phone-holder">
                @include('modules.theme.partials.preview')
            </div>

        </div>


        <div class="col-md-5 theme-settings">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Settings
                    </h4>
                </div>
                <div class="panel-body">
                    <div class=" col-md-12 col-lg-12 ">
                        <table class="table table-user-information">
                            <tbody>
                            {{Form::open()}}
                            <tr>
                                <td>Titel</td>
                                <td>{{Form::text('title')}}</td>
                            </tr>
                            <tr>
                                <td>Default</td>
                                <td>{{Form::select('default', array('' => Lang::get('general.make_your_choice'), 1 => 'Yes', 0 => 'No'))}}</td>
                            </tr>
                            <tr>
                                <td>Load page for preview</td>
                                <td>{{Form::select('default', $theme->getThemeAbleItems())}}</td>
                            </tr>
                            </tbody>
                        </table>
                        @include('_partials.errors')
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 theme-block">
            <div class="panel-group" id="accordion">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id="article-header">
                                Header
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body" id="article-header-input" class="article-header">
                            {{ $theme->createTheme('h1') }}
                        </div>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" id="article-paragraph">
                                Paragraaf
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group" id="article-paragraph-input" class="article-paragraph">
                                {{ $theme->createTheme('paragraph') }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#articleCollapse" id="article-article">
                                Article
                            </a>
                        </h4>
                    </div>
                    <div id="articleCollapse" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group" id="article-article-input" class="article-article">
                                {{ $theme->createTheme('article') }}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-5 theme-save">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Save your theme
                    </h4>
                </div>
                <div class="panel-body">
                    <div class=" col-md-12 col-lg-12 ">


                        <p>You can save the theme and apply them to all pages, or use them later for seperated pages.</p>

                        <hr/>
                        {{ Form::submit('Save') }}
                    </div>
                </div>
            </div>
        </div>

        {{ Form::close() }}

    </div>
</div>
{{HTML::script(url('javascript/scripts/ThemeBuilder.js'))}}
{{HTML::script(url('javascript/plugins/sticky_kit/load.js'))}}
<script>
    $(document).ready(function(){
        $("#preview-panel").stick_in_parent()

        $('.sidebar-item').click(function(){
            $('.sidebar-item').removeClass('active');
            $(this).addClass('active');

            var id = $(this).attr('id');
            $('.theme-block, .theme-settings, .theme-save').hide();
            $('.'+id).show();
        });
    });
</script>
@endsection
