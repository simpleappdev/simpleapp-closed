<?php

/**
 * This file will contain all routes where no login
 * is needed for.
 */

Route::get('/',										'HomeController@welcome');