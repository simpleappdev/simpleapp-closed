<?php

return array (
  'user_email' => 'Your email',
  'placeholder_email' => 'Enter your email',
  'user_password' => 'Password',
  'placeholder_password' => 'Enter your password',
  'remember_me' => 'Remember me',
  'login_button' => 'Login',
  'forgot_password' => 'Forgot password?',
  'password_forgot_title' => 'Forgot your password?',
  'your_email' => 'Your Email',
);
