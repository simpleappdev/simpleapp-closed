<?php

class Module extends \Eloquent {
	protected $fillable = [];

    public function companies(){
        return $this->belongsToMany('Company');
    }
}