@extends('master')
@section('content')
    <div class="container">
        <div class="col-lg-2">
            @include('includes.account_menu')
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    @include('includes.success')
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{Lang::get('payment.overview_not_payed')}}</h3>
                        </div>
                        <table class="table table-striped">
                            <tr>
                                <td>{{Lang::get('payment.bill_date')}}</td>
                                <td>{{Lang::get('payment.amount')}}</td>

                            </tr>
                            {{Form::open()}}
                            @foreach($open_bills as $bill)
                                <tr>
                                    <td>{{$bill->created_at->format('d-m-Y')}}</td>
                                    <td>&euro; {{number_format($bill->price, 2, ',', '.')}}</td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                    <div class="panel-footer">
                        {{$open_bills->links()}}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /.container -->

@endsection
