@extends('master') 
@section('content')
{{HTML::script('template/front/js/scripts/modules/modules.js')}}
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Modules in ontwikkeling</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">Printable</a>
                    <a href="#" class="list-group-item">Cupcake Wrappers</a>
                    <a href="#" class="list-group-item">Authentic Dragon Bones</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row">

                    <div class="col-sm-12 col-lg-12 col-md-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Beheer modules</h3>
                            </div>
                             <div class="panel-body">

                                {{Form::open()}}
                                @if($company->getMenu)
                                <ul id="sortable" class="col-sm-6 col-md-6 col-lg-6">
                                    @foreach($company->getMenu AS $menu)
                                        <li class="list-group-item">
                                            {{Form::checkbox('delete_module[]', $menu->id), $menu->name}}
                                            <a href="{{url('events/'.$company->id)}}">Beheer</a>
                                            <span style="float:right" class="glyphicon glyphicon-resize-vertical"></span>
                                        </li>

                                    @endforeach

                                </ul>
                                   {{Form::submit('Modules bewerken')}}
                                @endif
                             </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-12 col-md-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Installeer modules</h3>
                            </div>
                            <div class="panel-body">
                                @if($modules)
                                    <table border="1">
                                        <tr>
                                            <td>Verwijderen</td>
                                            <td>Module naam</td>
                                        </tr>
                                   @foreach($modules AS $module)
                                        <tr>
                                            <td>{{Form::checkbox('add_module[]', $module->id)}}</td>
                                            <td>{{$module->name}}</td>
                                        </tr>
                                        
                                   @endforeach
                                   </table>


                                @else
                                    Er zijn geen modules beschikbaar
                                @endif
                                {{Form::submit('Modules installeren'), Form::close()}}
                            </div>
                            @include ('_partials.errors')
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Company 2013 - Template by <a href="http://maxoffsky.com/">Maks</a>
                    </p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

@endsection
