@extends('master') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <p class="lead">Empty menu</p>
            <div class="list-group">
                <a href="#" class="list-group-item">Printable</a>
            </div>
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    @if ( Session::get('error') || Session::get('message') )
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel title</h3>
                        </div>
                        <div class="panel-body">
                            {{{ Session::get('error') }}}
                            {{{ Session::get('message') }}}
                        </div>
                    </div>
                    @endif
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                
                    <div class="panel-body">
                        <div class=" col-md-9 col-lg-9 ">
                            <form role="form" action="" method="post">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">{{Lang::get('login.user_email')}}</label>
                                    <input type="email" name="email" value="{{{Session::get('email')}}}" class="form-control" id="" placeholder="{{Lang::get('login.placeholder_email')}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">{{Lang::get('login.user_password')}}</label>
                                    <input type="password" name="password" class="form-control" id="" placeholder="{{Lang::get('login.placeholder_password')}}">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> {{Lang::get('login.remember_me')}}
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-default">{{Lang::get('login.login_button')}}</button>
                            </form>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{url('user/forgot_password')}}">{{Lang::get('login.forgot_password')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
