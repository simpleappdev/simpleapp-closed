{{--
@Requires:
    name          => string
    input_fields  => string
    description   => string
    html          => string
    fields        => array[string]
--}}
<div class="form-group">
    <label class="theme-builder-label" for="{{{ $name }}}[{{$type}}]">
        {{{ $description }}}
    </label>
    <div class="theme-builder-row">
        <select name="{{$name}}[{{{ $type }}}]" class="image-repeat form-control input-sm">
            @foreach ($fields as $key => $field)
                <option value="{{{ $key }}}" {{ $key == $value ? 'selected="selected"' : '' }}>
                   {{ $field }}
                </option>
            @endforeach
        </select>
    </div>
</div>
