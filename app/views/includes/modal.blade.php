@if(isset($show_delete_dialog) == true)
<div class="modal fade" id="show_delete_dialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel">{{Lang::get('modules/general.modal_title')}}</h4>
            </div>
            <div class="modal-body">
                {{$modal_title}}
                <span id="modal-item-name"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{Lang::get('general.cancel')}}</button>
                <button type="button" class="btn btn-primary" id="delete_modal_confirmation">{{Lang::get('general.delete')}}</button>
            </div>
        </div>
    </div>
</div>

<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#show_delete_dialog">Verwijderen</button>-->
@endif

<script>
    var item_id,
        item_type,
        tr;

    $(document).ready(function(){

        /**
         * On delete click().
         */

        $('.glyphicon-trash').click(function(){
            $('#ajax-response').hide();
            item_id     = $(this).data('item-id');
            item_type   = $(this).data('item-type');
            tr          = $(this).closest('tr');
            var item_name = '';

            $('.item-name').each(function(){
               item_name += $(this).text()+' ';
            });

            /**
             * Set the item name that will be deleted.
             */
            $('#modal-item-name').text(item_name);
        });

        $('#delete_modal_confirmation').click(function(){
            /**
             * Block the "Delete" button.
             */
            $(this).attr('disabled','disabled');

            /**
             * Execute AJAX request
             */
            var request = $.ajax({
                url: base_url + 'modules/' +item_type + '/delete/'+item_id,
                type: "GET",
            });

            /**
             * Get response "done".
             */
            request.done(function( msg ) {
                $('#show_delete_dialog').modal('toggle');
                $('#delete_modal_confirmation').removeAttr('disabled');
                tr.remove();
                $('#ajax-response').text(msg);
                $('#ajax-response').show();
            });

            /**
             * Get response "fail".
             */
            request.fail(function( jqXHR, textStatus ) {
                alert( "Request failed: " + textStatus );
            });

        });
    });
</script>