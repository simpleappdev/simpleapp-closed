@extends('master')
@section('content')
<style>
    .page-header{
        text-align: center;
    }
</style>
    <div class="container">
        <div class="col-lg-3">
            @include('includes.manage_module')
            @include('includes.modules_menu')
        </div>

		
		<div class="col-md-9">
            <header class="page-header">
                <h1 class="page-title">{{ucfirst($module_name)}} items</h1>
                @if(count($company->getSchedule) >= 2)
                    <small> <i class="fa fa-clock-o"></i> Last Created on:
                        <time>
                            {{$company->getSchedule->last()->created_at->format('D-M-Y H:i')}}
                        </time>
                    </small>
                @endif
            </header>
			<div class="panel panel-default">
			    <div class="panel-heading">
			        <h3 class="panel-title">Events from: {{$company->company_name}}</h3>
			    </div>
                <div class="panel-body">
                    @if($gallery->getPhotos)
                    @foreach($gallery->getPhotos AS $item)
                    <div class="col-lg-3">
                        <div class="row">{{$item->title}}</div>
                        <div class="row">{{$item->image}}</div>
                    </div>
                    @endforeach
                    @endif
                </div>
                <div class="panel-footer">
                    Regenerate
                    {{Form::open()}}
                    {{Form::checkbox('autogenerate', $module_id, $autogenerate)}}
                    {{Form::submit('Save')}}
                    {{Form::close()}}
                </div>
				
			</div>
		</div>	
    </div>
    <!-- /.container -->

@endsection
