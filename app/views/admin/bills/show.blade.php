@extends('admin.template.master')
@section('admin_content')


    <style>

    </style>
<div class="modal fade" id="update_bill_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>{{Lang::get('payment.mark_as_payed_with_date')}} <span id="date-message"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary submit_change">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{Lang::get('payment.show_single_bill')}}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12" >
        <div class="panel panel-danger hide" id="panel-error">
            <div class="panel-heading">
                <h3 class="panel-title hide" id="payment-date-error">{{Lang::get('payment.no_valid_payment_date')}}</h3>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{$bill->company->company_name}}</h3>
            </div>
            <div class="panel-body">
                {{Form::open()}}
                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>

                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>{{Lang::get('payment.bill_generated')}}</td>
                                <td>{{$bill->created_at->format('d-m-Y H:i:s')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('payment.payed_date')}}</td>
                                <td>{{Form::text('payed_date', $bill->payed_date != '-0001-11-30 00:00:00' ? $bill->payed_date->format('d-m-Y H:i:s') : Lang::get('payment.not_payed'))}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('payment.price')}}</td>
                                <td>&euro; {{number_format($bill->price, 2, ',', '.')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('payment.open_bills')}}</td>
                                <td>{{count($bill->company->openBills)}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('payment.open_bills_price')}}</td>
                                <td>&euro; {{number_format($bill->company->openBills->sum('price'), 2, ',', '.')}}</td>
                            </tr>



                            </tbody>
                        </table>

                        <a href="#" data-url="{{url('admin/invoices/'.$bill->id.'/now')}}" data-payment-date="now" data-toggle="modal" data-target="#update_bill_modal" class="btn btn-primary update_bill">{{Lang::get('payment.mark_as_payed_now')}}</a>
                        <a href="#" data-url="{{url('admin/invoices/'.$bill->id.'/time')}}" data-payment-date="time" data-toggle="modal" data-target="#update_bill_modal" class="btn btn-primary update_bill">{{Lang::get('payment.mark_as_payed_set_time')}}</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
            <div class="panel-footer">
                <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                <span class="pull-right">
                    <span data-original-title="Edit this user" data-toggle="tooltip" id="unlock-form" type="button" data-submit-text=" " class="btn btn-sm btn-warning"><i class="fa fa-pencil fa-fw"></i><span class="hidden">{{Lang::get('general.save')}}</span></span>
                    <span data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash fa-fw"></i></span>
                </span>
            </div>

        </div>
    </div>
</div>
    {{HTML::script('template/front/js/plugins/moment/moment-with-locales.js')}}
    {{HTML::style('template/front/js/plugins/datetimepicker/jquery.datetimepicker.css')}}
    {{HTML::script('template/front/js/plugins/datetimepicker/jquery.datetimepicker.js')}}


@endsection