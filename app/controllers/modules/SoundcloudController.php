<?php namespace modules;

use View;
use SoundcloudM;
use Redirect;
use Company;
use Input;
use Helpers\Helpers;
use Helpers\RenderSoundcloudH as RenderSC;
use Cache;

class SoundcloudController extends \BaseController {

    public $data = array();

    public function __construct(){
        $this->module_id = 2;
        $this->data['module_id'] = $this->module_id;
        $this->data['module_name'] = 'soundcloud';
        $this->data['modal_title'] = 'Are you sure that you want to delete this Soundcloud song: ';

    }
	/**
	 * Display a listing of the resource.
	 * GET /soundcloud
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$company = Company::find($id);

        $this->data['title'] = 'Al uw soundcloud muziek';
        $this->data['company'] = $company;

        return View::make('modules.soundcloud.index', $this->data);

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /soundcloud/create
	 *
	 * @return Response
	 */
	public function create($id)
	{
        $company = Company::find($id);
        $this->data['company'] = $company;
		return View::make('modules.soundcloud.create', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /soundcloud
	 *
	 * @return Response
	 */
	public function store($company_id)
	{
        $input = Input::all();
        $input['company_id'] = $company_id;
		$song = new SoundcloudM;
        $song->create($input);

        return Redirect::back()->with(['id' => $company_id]);
	}

	/**
	 * Display the specified resource.
	 * GET /soundcloud/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /soundcloud/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $song = SoundcloudM::find($id);
        $company = Company::find($song->company_id);
        $this->data['company'] = $company;
        $this->data['song'] = $song;
        return View::make('modules.soundcloud.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /soundcloud/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($company_id, $id)
	{
		$song = SoundcloudM::find($id);
        $song->update(Input::all());
        $company = Company::find($company_id);

        return Redirect::to('modules/soundcloud/'.$song->company_id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /soundcloud/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}