<?php

/**
 * All show controllers, will be also available for editing
 */

// ADMIN SECTION
Route::group(['prefix' => 'admin', 'namespace' => 'admin'], function()
{
    Route::get('/', 'IndexController@index');

    Route::model('companies', 'Company');
    Route::model('users', 'UserPlain');
    Route::model('news', 'News');
    Route::model('page', 'Page');
    Route::model('branches', 'Branch');

    Route::get('users/search', 'UsersController@search');
    Route::post('users/search', 'UsersController@search');
    // USERS
    Route::get('logbook/{log_type}/{id}', 'LogbookController@show_list')->where(array('id' => '[0-9]+'));

    Route::resource('users', 'UsersController');
    Route::resource('news','NewsController');
    Route::resource('page','PagesController');


    Route::get('logbook', 'LogbookController@index');
    Route::post('logbook', 'LogbookController@index');

    Route::get('restrictions', 'RestrictionsController@index');

    Route::resource('module', 'ModuleController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('branches', 'BranchController');

    // COMPANIES
    Route::any('companies/search', 'CompanyController@search');
    Route::resource('companies', 'CompanyController');
    // COMPANIES USERS
    Route::resource('companies.users', 'CompanyUserController');
//    Route::resource('companies.albums', 'CompanyAlbumController');
//    Route::resource('companies.albums.photos', 'CompanyPhotoController');

    Route::get('invoices', 'BillsController@index');
    Route::post('invoices', 'BillsController@update_selected');
    Route::get('invoices/{any}', 'BillsController@show');
    Route::get('invoices/{bill_id}/{time_type}', 'BillsController@mark_bill');




    // When ever sidemenu is rendered we will pass this data to it
    View::composer('admin.template.navbar-left', function($view)
    {
        $view->with('AdminMenuLinks', Config::get('admin/sidemenu.links'))
            ->with('AdminMenuActiveLink', Request::segment(2));
    });
});

