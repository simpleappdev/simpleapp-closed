<?php

return array(
    'UserId' => '#',
    'UserName' => 'Name',
    'UserConfirmed' => 'Confirmed',
    'UserRegisterDate' => 'Register Data',
    'UserActions' => 'Actions',
    'UserPanelTitle' => 'Sheena Kristin A.Eschor',
    'UserFirstname' => 'First name',
    'UserLastname' => 'Last name',
    'UserEmail' => 'Email',
    'UserPhonenumber' => 'Phonenumber',
    'UserAccountConfirmed' => 'Account confirmed',
    'UserRole' => 'Role',
    'UserSales' => 'My Sales Performance',
    'UserTeamSales' => 'Team Sales Performance'

);