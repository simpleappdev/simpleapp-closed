@extends('master')
@section('content')
<style>
    .price {
        font-size: 4em;
    }

    .price-cents {
        vertical-align: super;
        font-size: 50%;
    }

    .price-month {
        font-size: 35%;
        font-style: italic;
    }
    .panel {
        -webkit-transition-property : scale;
        -webkit-transition-duration : 0.2s;
        -webkit-transition-timing-function : ease-in-out;
        -moz-transition : all 0.2s ease-in-out;
    }

    .panel:hover {
        box-shadow: 0 0 10px rgba(0,0,0,.5);
        -moz-box-shadow: 0 0 10px rgba(0,0,0,.5);
        -webkit-box-shadow: 0 0 10px rgba(0,0,0,.5);
        -webkit-transform: scale(1.05);
        -moz-transform: scale(1.05);
    }
</style>
<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead">Modules in development</p>
            <div class="list-group">
                <a href="#" class="list-group-item">Soundcloud</a>
                <a href="#" class="list-group-item">Instagram</a>
                <a href="#" class="list-group-item">Facebook</a>
                <a href="#" class="list-group-item">Agenda</a>
            </div>
        </div>

        <div class="col-md-9">

            <div class="row">
                <h3 class="text-center">Pricing plans </h3>
                <hr>
                @foreach($plans as $plan)
                    <div class="col-sm-4">
                        <div class="panel {{$plan->recommended == 0 ? 'panel-default' : 'panel-danger'}} text-center">
                            <div class="panel-heading">
                                <h3>{{$plan->name}}</h3>
                            </div>
                            <div class="panel-body">
                                <h3 class="panel-title price">&euro; {{$plan->price}}<span class="price-month">month</span></h3>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item">1 app</li>
                                <li class="list-group-item">5 different themes</li>
                                <li class="list-group-item">1 user</li>
                                <li class="list-group-item">5 push notification a month</li>
                                <li class="list-group-item">E-mail Support</li>
                                <li class="list-group-item"><a href="{{url('plans/'.$plan->id)}}" class="btn btn-danger">Sign Up Now!</a></li>
                            </ul>
                        </div>
                    </div>

                @endforeach



            </div>

        </div>

    </div>

</div>

@endsection
