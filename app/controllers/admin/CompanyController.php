<?php namespace admin;

use Company;
use Input;
use User;
use View;
use Redirect;
use Branch;
use Lang;

class CompanyController extends \BaseController
{
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function index()
    {
        $companies = $this->company->paginate(20);

        return View::make('admin.company.index',['companies' => $companies]);
    }

    public function show($company)
    {
        return View::make('admin.company.show',['company' => $company]);
    }

    public function create()
    {
        //Get list of branches and users to choose the branch for the new user
        $data['branches'] = Branch::all()->lists('description', 'id');
        $data['users'] = User::all()->lists('username', 'id');

        return View::make('admin.company.create', ['data' => $data]);
    }

    public function edit($company)
    {
        //Get a list of branches and users to choose a new one in the edit view
        $data['branches'] = Branch::lists('description', 'id');
        $data['users'] = User::lists('username', 'id');
        return View::make('admin.company.edit', ['company' => $company, 'data' => $data]);
    }

    public function store()
    {
        $input = Input::all();
        $this->company->create($input);

        return Redirect::route('admin.companies.index')->with('message', Lang::get('company.companyCreated'));
    }

    public function update($company)
    {

        $company->update(Input::all());
        return Redirect::route('admin.companies.index')->with('message', Lang::get('company.companyUpdated'));
    }

    public function destroy($company)
    {
        $company->destroy($company->id);
        return Redirect::route('admin.companies.index')->with('message', Lang::get('company.companyDeleted'));
    }

    public function search()
    {
        // validate
        $input = Input::only('search_for', 'search_value');

        $companies = Company::where( $input['search_for'], 'like', '%'. $input['search_value'] .'%' )
            ->paginate(20);
        return View::make('admin.company.index', ['companies' => $companies]);
    }
}
