@extends('admin.template.master')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{Lang::get('company.companyHeader')}}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12" >


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{Lang::get('general.add_new_company')}}</h3>
            </div>
            <div class="panel-body">

                {{ Form::open(['route' => 'admin.companies.store', 'role' => 'form']) }}
                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>

                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>{{Lang::get('general.company_name')}}</td>
                                <td>{{Form::text('company_name')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.branch')}}</td>
                                <td>{{Form::select('branch_id', $data['branches'])}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.user')}}</td>
                                <td>{{Form::select('user_id', $data['users'])}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.email')}}</td>
                                <td>{{Form::text('email')}}</td>
                            </tr>
                            <td>{{Lang::get('general.phone_number')}}</td>
                            <td>{{Form::text('phone')}}</td>
                            <tr>
                            <tr>
                                <td>{{Lang::get('general.address')}}</td>
                                <td>{{Form::text('address')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.city')}}</td>
                                <td>{{Form::text('city')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.country')}}</td>
                                <td>{{Form::text('country')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.zipcode')}}</td>
                                <td>{{Form::text('zipcode')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.status')}}</td>
                                <td>{{Form::select('confirmed', [1 => Lang::get('general.yes'), 0 => Lang::get('general.no')])}}</td>
                            </tr>

                            </tr>

                            </tbody>
                        </table>

                        <a href="#" class="btn btn-primary">{{Lang::get('company.companySalesPerformance')}}</a>
                        <a href="#" class="btn btn-primary">{{Lang::get('company.companyTeamSalesPerformance')}}</a>
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                <span class="pull-right">
                    {{Form::submit(Lang::get('company.companySaveButton'), ['class' => 'btn btn-success'])}}
                    {{Form::close()}}
                </span>
            </div>

        </div>
    </div>
</div>

@endsection