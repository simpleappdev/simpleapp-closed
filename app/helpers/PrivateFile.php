<?php 

namespace Helpers;

use Illuminate\Filesystem\Filesystem;

class PrivateFile {
	public $File;
	public $example = 'Dit is een test met een file';

	public function __construct() {

		$this->File = new Filesystem;

	}

	public function createCompanyDir($slug){
		$path = 'simpleapp/companies/'.$slug;
		$this->File->makeDirectory($path,0777, true);
		$this->File->makeDirectory($path.'/events');
		$this->File->makeDirectory($path.'/pages');
		$this->File->makeDirectory($path.'/themes');
		$this->File->makeDirectory($path.'/gallery');
		$this->File->makeDirectory($path.'/other');
		$this->File->put($path.'/pages/home.html', '');
		$this->File->put($path.'/pages/contact.html', '');
		$menu = $this->createMenuItem();
		$this->File->put($path.'/buttons.xml', $menu);
	}

	public function file($slug, $file, $content) {
		$path = 'simpleapp/companies/'.$slug.'/';
		if(!$this->File->isDirectory($path)){
			$this->File->makeDirectory($path, 0777, true);
		}
		//Anders gaan we alles aanmaken :)
		$this->File->put($path.$file, $content);
		
		return 'Gelukt!';
		
	}

}





