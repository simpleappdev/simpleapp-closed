<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PaymentsTableSeeder extends Seeder {

	public function run()
	{
		/**
		 * Set the pricing options
		 */

		$plans[] = array(
			'name' => 'bronse',
			'description' => 'A bronse plan description',
			'price' => '14.99',
		);
		$plans[] = array(
			'name' => 'silver',
			'description' => 'A silver plan description',
			'price' => '24.99',
		);
		$plans[] = array(
			'name' => 'gold',
			'description' => 'A gold plan description',
			'price' => '44.99',
		);

		foreach($plans as $data){
			Plan::create($data);
		}
	}

}