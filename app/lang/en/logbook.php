<?php

return array (
  'header_logs' => 'Logboek',
  'header_search' => 'Search for a log',
  'search_by_log' => 'Log ID',
  'search_by_user' => 'User',
  'search_by_company' => 'Company',
  'search_by_item' => 'Item',
  'search_by_event' => 'Event',
  'search_by_action' => 'Action',
  'search_button' => 'Search',
  'tableheader_hashtag' => 'Log ID',
  'tableheader_user' => 'User',
  'tableheader_company' => 'Company',
  'tableheader_item' => 'Item',
  'tableheader_event' => 'Event',
  'tableheader_action' => 'Action',
  'tableheader_time' => 'Date',
  'back_button' => 'Return to logs',
  'profile' => 'Profile',
  'search_by_system_log' => 'System log',
  'actions' => 'Actions',
);
