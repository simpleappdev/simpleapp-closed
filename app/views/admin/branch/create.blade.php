@extends('admin.template.master')
@section('admin_content')

    <div class="row">

        <div class="col-lg-12">
            <h1 class="page-header">{{Lang::get('branch.branchHeader')}}</h1>
        </div>

        <div class="col-sm-4 col-lg-4 col-md-4">

            <h2>{{Lang::get('branch.branchCreate')}}</h2>

            {{ Form::open(['route' => 'admin.branches.store', 'role' => 'form']) }}

            {{ Form::label('branch_description', Lang::get('branch.branchDescriptionName'))}}
            {{ Form::text('description', null, ['placeholder' => Lang::get('branch.branchDescriptionName'), 'class' => 'form-control'])}}

            {{ Form::Label('branch_language', Lang::get('branch.branchLabelLanguage'))}}
            {{ Form::text('language', null, ['placeholder' => Lang::get('branch.branchPlaceholderLanguage'), 'class' => 'form-control'])}}

            {{ Form::submit(Lang::get('branch.branchCreate'), ['class' => 'btn btn-default'])}}
            {{ Form::close()}}

        </div>
    </div>
    </div>
@endsection