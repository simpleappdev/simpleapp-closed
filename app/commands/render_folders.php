<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Helpers\CompanyH;

class render_folders extends Command
{

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:render_folders';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Renders basic folder structure for companies';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $companies = Company::all();
        $counter = 0;
        foreach($companies as $company){
            $counter++;
            CompanyH::render_folder_structure($company->slug);
        }

        echo 'Er zijn '.$counter . ' bedrijven toegevoegd aan de companies folder.';
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
