<?php namespace Helpers;

use File;

class CompanyH {

    /**
     * @param $slug
     * @param bool $app - If app is false, artisan will generate the folders.
     * @return string
     */
    public static function render_folder_structure($slug, $app = false){

        //File::makeDirectory($slug.'/test/folder/recursive', 0775, true,true);


        if($app == true){
            $base_path = 'simpleapp/companies/'.$slug.'/';
        } else {
            $base_path = 'public_html/simpleapp/companies/'.$slug.'/';
        }
        //die($base_path);

        $array = array (
            'app/style/images/buttons',
            'app/style/images/headers/images',
            'app/gallery',
            'app/events',
            'app/pages',
            'website',
            'website/themes',
        );

        foreach($array as $folder){
            File::makeDirectory($base_path.$folder, 0775, true,true);
        }
    }
}