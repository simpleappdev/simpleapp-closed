{{--
@Requires:
    description   => string
    name          => string
    input_fields  => string
    value         => string
--}}
<div class="form-group">
    <label class="theme-builder-label" for="{{{ $name }}}[font-color]">
        {{{ $description }}}
    </label>
    <div class="theme-builder-row form-inline">
        <div class="input-group">
            <input type="text" class="color-picker-font form-control input-sm" value="{{{ $value }}}" name="{{{ $name }}}[font-color]">
            <span class="input-group-addon color-picker-addon"></span>
            <div class="col-md-3">
                <input type="number" class="font-size form-control input-sm" min="0" max="20" value="12">
            </div>
        </div>
    </div>
</div>
