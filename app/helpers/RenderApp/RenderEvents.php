<?php

class RenderEventsH {

    public static function renderXML($data){
        $xml = '<?xml version="1.0" encoding="utf-8"?>';

        foreach($data as $key => $item){
            $xml .= '<event2>
		<uid>giobiggi</uid>
		<title>Gio + BiGGI</title>
		<location>Club Devo - Goes</location>
		<fromdate>2014-05-30 23:00:00</fromdate>
		<tilldate>2014-05-31 04:00:00</tilldate>
		<image>voorbeeld2.jpg</image>
		<description>Line up:   GIO (LIVE)
		    BIGGI
		    Jay Valentine
		    Darryl Antunez
		    Rancido
		    Live Percussion by: Vikesh
		</description>
	</event2>';
        }

    }
}