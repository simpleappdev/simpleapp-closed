<?php

use Helpers\HtmlCreator;
use Fbf\LaravelNavigation\NavigationComposer;

class TestController extends \BaseController {

    /**
     * Controller for quick testing functions
     */

    public function index(){
        $schedule = Schedule::find(65);
        $theme = Theme::where('default', '=', '1')->where('company_id', '=', 1)->first();

        $file = $theme->file;
        $data = unserialize($schedule->description);

        $html = '';

        foreach($data as $value){
            $html .= '<div class="article">';
            $html .= '<div class="h1">'.$value['title'].'</div>';
            $html .= '<div class="paragraph">'.$value['content'].'</div>';
            $html .= '</div>';
        }

        print_r($html);
        $css = File::get('simpleapp/companies/triple/website/themes/'.$file);
        $save_data = HtmlCreator::html($html, $css);

        File::put('eventtest.html', $save_data);
    }

    public function pdf($bill_id){

        $bill = Bill::find($bill_id);
        $company = $bill->company;


        $view = View::make('templates.pdf.invoice', [
            'bill' => $bill,
            'company' => $company,
        ]);

        return $view;
    }

    public function menu(){

        return View::make('test');
    }
}