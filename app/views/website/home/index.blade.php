@extends('master')
@section('content')
    {{HTML::script('template/front/js/scripts/front_page.js')}}
    <div class="container">

        <div class="row">

            <div class="col-md-2">
                <p class="lead">Shop Name</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">Printable</a>
                    <a href="#" class="list-group-item">Cupcake Wrappers</a>
                    <a href="#" class="list-group-item">Authentic Dragon Bones</a>
                </div>
            </div>

            <div class="col-md-10">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="{{url('images/banners/simpleapp.jpg')}}" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="{{url('images/banners/simpleapp.jpg')}}" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="{{url('images/banners/simpleapp.jpg')}}" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="container-fluid">
                        <div id="custom_carousel" class="carousel slide" data-ride="carousel" data-interval="2500">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php $counter = 1; ?>
                            @foreach($news as $item)
                                @if($counter == 1)
                                    <div class="item active">
                                @else
                                    <div class="item">
                                @endif
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-3"><img src="{{Config::get('default.news_resized_image_location') . $item->image}}" class="img-responsive"></div>
                                            <div class="col-md-9">
                                                <h2>{{$item->title}}</h2>
                                                <p>{{substr($item->body,0,250)}}</p><p><a href="/news/{{$item->slug}}">{{Lang::get('general.read_more')}}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                        <?php $counter++ ?>
                            @endforeach
                                    <!-- End Item -->
                            </div>
                            <!-- End Carousel Inner -->
                            <div class="controls">
                                <ul class="nav">
                                    <?php $counter = 0; ?>
                                    @foreach($news as $item)
                                        @if($counter == 0)
                                                <li data-target="#custom_carousel" data-slide-to="{{$counter}}" class="active"><a href="#"><img src="{{Config::get('default.news_thumbnail_image_location') . $item->image}}"><small>{{$item->title}}</small></a></li>
                                        @else
                                                <li data-target="#custom_carousel" data-slide-to="{{$counter}}"><a href="#"><img src="http://placehold.it/50x50"><small>{{$item->title}}</small></a></li>
                                        @endif
                                        <?php $counter++ ?>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                        <!-- End Carousel -->
                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
