<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}">SimpleApp</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">{{Lang::get('general.select_language')}}
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li>
                                <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                                    {{{ $properties['native'] }}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>



                @if (Auth::check())
                <li><a href="{{url('signup/company')}}">{{Lang::get('company.create_company')}}</a></li>
                <li><a href="{{url('myaccount')}}">{{Lang::get('account.my_account')}}</a></li>
                <li><a href="{{url('plans/go-pro')}}">Go pro!</a></li>
                @else
                    <li><a href="{{url('login')}}">{{Lang::get('account.login')}}</a>
                    <li><a href="{{url('signup')}}">{{Lang::get('account.signup')}}</a></li>

                @endif
                
                </li>

            </ul>

            @if (Auth::check())
            
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">{{Lang::get('account.my_account')}}
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="navbar-content">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img src="http://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/twDq00QDud4/s120-c/photo.jpg"
                                            alt="Alternate Text" class="img-responsive" />
                                        <p class="text-center small">
                                            <a href="#">{{Lang::get('account.change_photo')}}</a></p>
                                    </div>
                                    <div class="col-md-7">
                                        <span>{{Auth::user()->username}}</span>
                                        <p class="text-muted small">
                                            {{Auth::user()->email}}</p>
                                        <div class="divider">
                                        </div>
                                            <a href="#" class="btn btn-primary btn-sm active col-md-12">{{Lang::get('account.my_account')}}</a>
                                        @if(Auth::user()->hasRole('admin'))
                                            <a href="{{url('/admin')}}" id="show-admin-menu" class="btn btn-primary btn-sm active col-md-12">{{Lang::get('menu.admin_menu')}}</a>
                                        @endif


                                    </div>
                                </div>
                            </div>
                            <div class="navbar-footer">
                                <div class="navbar-footer-content">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="#" class="btn btn-default btn-sm">{{Lang::get('account.change_password')}}</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{url('logout')}}" class="btn btn-default btn-sm pull-right">{{Lang::get('account.logout')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            @endif
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>