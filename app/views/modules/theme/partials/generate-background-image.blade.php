{{--
@Requies:
    name          => string
    input_vield   => string
    value         => array
 --}}
 <div class="form-group">
    <label class="theme-builder-label" for="">{{ $label }}</label>
    <!-- <div class="theme-builder-row"> -->
        <select name="{{ $name }}[background-image]" class="image-bg form-control input-sm">
        @if ($images)
            <option value="0">Geen keuze</option>
            @foreach ($images as $image)
            <option value="{{ $image }}"> {{ $image }} </option>
            @endforeach
        @else
            <option>Geen afbeeldingen gevonden</option>
        @endif
        </select>
    <!-- </div> -->
</div>
