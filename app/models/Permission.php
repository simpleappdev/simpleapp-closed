<?php

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
	protected $fillable = ['id', 'name', 'display_name'];

	protected $guarded = [];

    public function getRoles(){
        return $this->hasMany('Role');
    }

    public function getRolesforPermissions(){
        return $this->belongsToMany('Role');
    }
}