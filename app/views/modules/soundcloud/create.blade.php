@extends('master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('includes.manage_module')
            @include('includes.modules_menu')
        </div>

        <div class="col-sm-4 col-lg-8 col-md-4">

            @include('_partials.item_created')
            <!-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" > -->

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Song</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center">
                            <img alt="User Pic" src="{{url('images/modules/soundcloud_logo.png')}}" class="img-responsive"> </div>

                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                {{Form::open()}}

                                <tr>
                                    <td>Artist</td>
                                    <td>{{Form::text('artist')}}</td>
                                </tr>
                                <tr>
                                    <td>Title</td>
                                    <td>{{Form::text('name')}}</td>
                                </tr>
                                <tr>
                                    <td>Embed Code</td>
                                    <td>{{Form::text('embed_code')}}</td>
                                </tr>
                                <tr>
                                    <td>Active</td>
                                    <td>{{Form::select('active', array('' => 'Make your choice', 1 => 'Yes', 0 => 'No'))}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>{{Form::submit('Add')}}</td>
                                </tr>

                                {{Form::close()}}


                                </tbody>
                            </table>
                            @include('_partials.errors')
                        </div>
                    </div>
                </div>
                {{--
                <div class="panel-footer">
                    <p>
                        {{Lang::get('form.signup_attention_new_company')}}
                    </p>
                </div>--}}

            </div>
            <!-- </div> -->

        </div>
    </div>
</div>

@endsection
