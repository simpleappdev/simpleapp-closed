<?php namespace admin;

use Permission;
use View;
use Input;
use Redirect;
use Role;

class PermissionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /permission
	 *
	 * @return Response
	 */
	public function index()
	{
		$permissions = Permission::all();
		return View::make('admin.permission.index', ['permissions' => $permissions]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /permission/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.permission.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /permission
	 *
	 * @return Response
	 */
	public function store()
	{
		$permission = new Permission;
		$permission->name = Input::get('permission_name');
		$permission->display_name = Input::get('display_name');
		$permission->save();

        return Redirect::to('admin/permissions');
	}

	/**
	 * Display the specified resource.
	 * GET /permission/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$permission = Permission::find($id);
		return View::make('admin.permission.show', ['permission' => $permission]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /permission/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$permission = Permission::find($id);
		return View::make('admin.permission.edit', ['permission' => $permission]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /permission/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$permission = Permission::find($id);
		$permission->fill(Input::all());
		$permission->save();

		return View::make('admin.permission.show', ['permission' => $permission]);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /permission/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Permission::find($id)->delete();

		return Redirect::to('admin/permissions');
	}

}