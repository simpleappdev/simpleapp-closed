<?php

class HomeController extends BaseController {

	public function welcome()
	{
        $data['news'] = News::where('language', '=', strtolower(LaravelLocalization::getCurrentLocaleName()))
			->orderBy('created_at')
			->limit(4)
			->get();

		return View::make('website.home.index', $data);
	}

}