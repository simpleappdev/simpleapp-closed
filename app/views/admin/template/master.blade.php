<!DOCTYPE html>
<html lang="nl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta current_language="{{ strtolower(LaravelLocalization::getCurrentLocaleName()) }}"/>

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    {{ HTML::style('template/admin/css/bootstrap.css'); }}

    <!-- Add custom CSS here -->
    {{ HTML::style('template/admin/css/default-css.css'); }}

    <!-- MetisMenu CSS -->
    {{ HTML::style('template/admin/css/plugins/metisMenu/metisMenu.min.css'); }}

    <!-- Timeline CSS -->
    {{ HTML::style('template/admin/css/plugins/timeline.css'); }}

    <!-- Custom CSS -->
    {{ HTML::style('template/admin/css/sb-admin-2.css'); }}

    <!-- Morris Charts CSS -->
    {{ HTML::style('template/admin/css/plugins/morris.css'); }}

    <!-- Custom Fonts -->
    {{ HTML::style('template/admin/css/font-awesome.min.css'); }}

    {{ HTML::script('//code.jquery.com/jquery-1.10.2.js') }}
    {{ HTML::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    @include('admin.template.navbar-header')
    <!-- /.navbar-header -->


    <!-- /.navbar-static-side -->


    <div id="page-wrapper">
        @yield('admin_content', 'Default content')
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
{{HTML::script('template/admin/js/jquery.js')}}

<!-- Bootstrap Core JavaScript -->
{{HTML::script('template/admin/js/bootstrap.min.js')}}


<!-- Metis Menu Plugin JavaScript -->
{{HTML::script('template/admin/js/plugins/metisMenu/metisMenu.min.js')}}


<!-- Morris Charts JavaScript -->
{{HTML::script('template/admin/js/plugins/morris/raphael.min.js')}}



<!-- Custom Theme JavaScript -->
{{HTML::script('template/admin/js/sb-admin-2.js')}}



</body>

</html>
