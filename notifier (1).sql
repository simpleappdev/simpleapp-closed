-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 03, 2015 at 05:07 PM
-- Server version: 5.5.40
-- PHP Version: 5.5.20-1+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `notifier`
--

-- --------------------------------------------------------

--
-- Table structure for table `assigned_roles`
--

CREATE TABLE IF NOT EXISTS `assigned_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_roles_user_id_foreign` (`user_id`),
  KEY `assigned_roles_role_id_foreign` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `assigned_roles`
--

INSERT INTO `assigned_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  `information` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `active` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `folder_created` int(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `owner_id`, `information`, `slug`, `branch_id`, `active`, `created_at`, `updated_at`, `folder_created`, `email`, `phone`, `address`, `city`, `country`, `zipcode`, `status`, `deleted_at`) VALUES
(1, 'Triple C', 1, 'Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. Dit is een test met heel wat tekst. ', 'triple', 1, 0, '2014-05-22 10:40:02', '2014-08-06 19:02:50', 0, 'wavangrondelle@live.nl', '010-123 45 67', 'Examplestraat 12', 'Rotterdam', 'Nederland', '1234ab', 0, '0000-00-00 00:00:00'),
(2, 'Simple App', 0, '<p>Test</p>', 'simple-app', 0, 0, '2014-06-06 10:57:01', '2014-06-06 10:57:01', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(3, 'OOOM', 0, '<p>tyhtrhhjgf</p>', 'triple-x', 0, 0, '2014-06-09 14:39:21', '2014-06-09 14:39:21', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(4, 'Test Company', 0, 'empty', 'ooo-mediae', 0, 0, '2014-07-09 17:52:04', '2014-07-09 17:52:04', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(5, 'testttt', 0, 'empty', 'tttttt', 0, 0, '2014-07-09 17:57:00', '2014-07-09 17:57:00', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(6, 'kjhnjgnkjfg', 0, 'empty', 'jnklnkdjgnbd', 0, 0, '2014-07-09 17:57:21', '2014-07-09 17:57:21', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(7, 'Test with key', 0, 'empty', 'test-with-key', 0, 1, '2014-07-09 19:57:17', '2014-07-09 19:57:17', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(8, 'Test without key', 0, 'empty', 'test-without-key', 0, 0, '2014-07-09 19:58:17', '2014-07-09 19:58:17', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00'),
(9, 'WIC', 0, 'empty', 'wic-events', 0, 0, '2014-09-19 05:20:31', '2014-09-19 05:20:31', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_create_keys`
--

CREATE TABLE IF NOT EXISTS `company_create_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `used` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_menu`
--

CREATE TABLE IF NOT EXISTS `company_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `company_menu_menu_id_foreign` (`menu_id`),
  KEY `company_menu_company_id_foreign` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `company_menu`
--

INSERT INTO `company_menu` (`id`, `menu_id`, `company_id`, `created_at`, `updated_at`) VALUES
(3, 3, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_module`
--

CREATE TABLE IF NOT EXISTS `company_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `company_module_company_id_foreign` (`company_id`),
  KEY `company_module_module_id_foreign` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `company_module`
--

INSERT INTO `company_module` (`id`, `module_id`, `company_id`, `created_at`, `updated_at`) VALUES
(16, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_private_images`
--

CREATE TABLE IF NOT EXISTS `company_private_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `company_private_images`
--

INSERT INTO `company_private_images` (`id`, `title`, `file_name`, `company_id`, `created_at`, `updated_at`) VALUES
(11, 'Test 3', 'simpleapp/companies/triple/website/private_image_gallery/y7yGDPyZAGdjTOLvuMVTTHUm19rcneJiUxvX6aZ4VHTWPfqqNojSidM7ilywgQPU9klCsSiw85r-windows_7_dark-wallpaper-1366x768.jpg', 1, '2014-08-23 14:36:51', '2014-08-23 14:36:51'),
(12, 'Windows BG', 'simpleapp/companies/triple/website/private_image_gallery/4QQ10gdOWb14BWJTdmjTvOe8G8tetEug4efQede39qouyddeErETbrbzl3MPRDTNX0w2lw6Ff2D-windows_seven_iv-wallpaper-1366x768.jpg', 1, '2014-08-23 14:40:32', '2014-08-23 14:40:32');

-- --------------------------------------------------------

--
-- Table structure for table `company_schedule`
--

CREATE TABLE IF NOT EXISTS `company_schedule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `company_schedule_company_id_foreign` (`company_id`),
  KEY `company_schedule_schedule_id_foreign` (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_user`
--

CREATE TABLE IF NOT EXISTS `company_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `company_user_user_id_foreign` (`user_id`),
  KEY `company_user_company_id_foreign` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `company_user`
--

INSERT INTO `company_user` (`id`, `user_id`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_user_followers`
--

CREATE TABLE IF NOT EXISTS `company_user_followers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `company_user_followers_company_id_foreign` (`company_id`),
  KEY `company_user_followers_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cron_job`
--

CREATE TABLE IF NOT EXISTS `cron_job` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `return` text COLLATE utf8_unicode_ci NOT NULL,
  `runtime` float(8,2) NOT NULL,
  `cron_manager_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cron_manager`
--

CREATE TABLE IF NOT EXISTS `cron_manager` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rundate` datetime NOT NULL,
  `runtime` float(8,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module` text COLLATE utf8_unicode_ci NOT NULL,
  `pagename` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `module`, `pagename`, `created_at`, `updated_at`) VALUES
(1, 'Gallery', 'images', 'images', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Events', 'events', 'events', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Soundcloud', 'page', 'soundcloud', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_04_11_164406_confide_setup_users_table', 1),
('2014_04_11_170547_entrust_setup_tables', 1),
('2014_04_27_221731_create_companies_table', 1),
('2014_05_03_171905_create_schedules_table', 1),
('2014_05_05_183444_create_company_user_followers', 1),
('2014_05_06_192847_create_phone_menu_table', 1),
('2014_05_14_192732_create_photo_table', 1),
('2014_05_14_192846_create_photo_album_table', 1),
('2013_06_27_143953_create_cronmanager_table', 2),
('2013_06_27_144035_create_cronjob_table', 2),
('2014_06_06_121048_create_soundcloud_table', 3),
('2014_06_17_044249_AlterTableUsersAddNullableRememberTokenMigration', 4),
('2014_06_19_064605_AlterCompaniesTableAddBannedBoolMigration', 4),
('2014_07_07_182826_create_company_branch_table', 5),
('2014_07_09_181918_create_company_create_keys_table', 5),
('2014_07_12_153524_create_module_pages_table', 5),
('2014_07_18_123706_create_themes_table', 5),
('2014_07_22_231108_create_modules_table', 6),
('2014_08_14_223206_create_news_table', 7),
('2014_08_17_161248_add_website_surname_first_name_to_users_table', 8),
('2014_08_18_222429_create_company_private_images_table', 9),
('2014_09_09_204700_create_subscription_table', 10),
('2014_09_10_081100_create_city_table', 10),
('2014_11_20_181911_add_extra_fields_to_companies_table', 10),
('2014_11_20_204910_add_full_option_to_soundcloud_table', 11),
('2014_11_20_205137_add_company_id_to_soundcloud_table', 12),
('2014_11_20_221509_create_modules_autogenerate_table', 13),
('2014_11_22_205000_add_active_field_to_schedules_table', 14),
('2014_11_23_000034_create_module_galleries_table', 15),
('2014_11_23_003238_add_date_field_to_module_galleries_table', 16),
('2014_11_23_120213_add_gallery_id_to_photos_table', 17),
('2014_11_23_120417_add_active_field_to_photos_table', 18),
('2014_11_26_202715_add_description_field_to_module_pages_table', 19),
('2014_11_26_211852_create_rendered_app_table', 20),
('2014_11_30_000018_add_template_to_module_pages_table', 21),
('2014_12_01_173535_add_version_to_rendered_app_table', 22),
('2014_12_01_184238_add_iframe_to_soundcloud_table', 22),
('2014_12_02_182250_add_slug_to_schedules_table', 22),
('2014_12_02_185722_add_default_theme_to_schedules_table', 22),
('2014_12_03_194230_add_soft_delete_to_module_pages_table', 22),
('2014_12_03_221400_add_soft_delete_to_soundcloud_table', 22),
('2014_12_04_191609_create_session_table', 22),
('2014_12_04_210127_add_status_table_to_companies_table', 22),
('2014_12_04_210354_add_soft_delete_to_companies_table', 22),
('2014_12_07_131121_add_soft_delete_to_news_table', 22),
('2014_12_07_132455_add_published_to_news_table', 22);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hard_coded` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `title`, `type`, `active`, `link`, `hard_coded`, `created_at`, `updated_at`) VALUES
(1, 'Events', 'events', 1, 'test/test', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Soundcloud', 'soundcloud', 1, '/module/manage/', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Pages', 'page', 1, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Gallery', 'gallery', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `modules_autogenerate`
--

CREATE TABLE IF NOT EXISTS `modules_autogenerate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `modules_autogenerate_company_id_foreign` (`company_id`),
  KEY `modules_autogenerate_module_id_foreign` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `modules_autogenerate`
--

INSERT INTO `modules_autogenerate` (`id`, `module_id`, `company_id`, `created_at`, `updated_at`) VALUES
(14, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `module_galleries`
--

CREATE TABLE IF NOT EXISTS `module_galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `destroyed` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `module_galleries`
--

INSERT INTO `module_galleries` (`id`, `title`, `company_id`, `description`, `active`, `destroyed`, `created_at`, `updated_at`, `date`) VALUES
(1, 'Test Album 1', 1, '', 0, 0, '0000-00-00 00:00:00', '2014-11-22 23:42:27', '1991-05-10 14:39:01'),
(2, 'Test Album Create', 1, 'This is a new description', 1, 0, '2014-11-22 23:36:30', '2014-11-22 23:36:30', '1991-05-10 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `module_pages`
--

CREATE TABLE IF NOT EXISTS `module_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pagename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `destroyed` int(11) NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `personal_description` text COLLATE utf8_unicode_ci NOT NULL,
  `theme` int(11) NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `module_pages`
--

INSERT INTO `module_pages` (`id`, `titel`, `pagename`, `company_id`, `active`, `destroyed`, `content`, `created_at`, `updated_at`, `personal_description`, `theme`, `deleted_at`) VALUES
(16, 'Mijn Eerste Pagina', 'test.html', 1, 1, 0, 'a:2:{i:0;a:2:{s:5:"title";s:7:"Titel 2";s:7:"content";s:7:"Tekst 2";}i:1;a:2:{s:5:"title";s:7:"Titel 1";s:7:"content";s:7:"Tekst 1";}}', '2014-11-26 20:00:23', '2014-11-26 20:01:16', 'My Description', 0, '0000-00-00 00:00:00'),
(17, 'Mijn Eerste Pagina', 'test.html', 1, 1, 0, 'a:2:{i:0;a:2:{s:5:"title";s:7:"Titel 2";s:7:"content";s:7:"Tekst 2";}i:1;a:2:{s:5:"title";s:7:"Titel 1";s:7:"content";s:7:"Tekst 1";}}', '2014-11-26 20:00:23', '2014-11-26 20:01:16', 'My Description', 0, '0000-00-00 00:00:00'),
(18, 'Mijn Eerste Pagina', 'test.html', 1, 1, 0, 'a:2:{i:0;a:2:{s:5:"title";s:7:"Titel 2";s:7:"content";s:7:"Tekst 2";}i:1;a:2:{s:5:"title";s:7:"Titel 1";s:7:"content";s:7:"Tekst 1";}}', '2014-11-26 20:00:23', '2014-11-26 20:01:16', 'My Description', 0, '0000-00-00 00:00:00'),
(19, 'Mijn Eerste Pagina', 'test.html', 1, 1, 0, 'a:2:{i:0;a:2:{s:5:"title";s:7:"Titel 2";s:7:"content";s:7:"Tekst 2";}i:1;a:2:{s:5:"title";s:7:"Titel 1";s:7:"content";s:7:"Tekst 1";}}', '2014-11-26 20:00:23', '2014-11-26 20:01:16', 'My Description', 0, '0000-00-00 00:00:00'),
(20, 'Test', 'test.html', 1, 0, 0, 'a:1:{i:0;a:2:{s:5:"title";s:0:"";s:7:"content";s:0:"";}}', '2014-11-29 23:13:35', '2014-11-29 23:13:35', '', 0, '0000-00-00 00:00:00'),
(21, 'testsertse', 'test.html', 1, 1, 0, 'a:1:{i:0;a:2:{s:5:"title";s:0:"";s:7:"content";s:0:"";}}', '2014-11-29 23:21:51', '2014-11-29 23:29:54', '', 56, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime NOT NULL,
  `published` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `language`, `created_at`, `updated_at`, `deleted_at`, `published`) VALUES
(1, 'This is a title', 'This is the body text', 'en', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Dit is een titel', 'Dit is de body tekst', 'nl', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'This is a title', 'This is the body text', 'English', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'Dit is een titel', 'Dit is de body tekst', 'Dutch', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'This is a title', 'This is the body text', 'English', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'Dit is een titel', 'Dit is de body tekst', 'Dutch', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(7, '', '', '', '2014-08-16 11:16:31', '2014-08-16 11:16:31', '0000-00-00 00:00:00', 0),
(8, '', '', '', '2014-08-16 11:16:42', '2014-08-16 11:16:42', '0000-00-00 00:00:00', 0),
(9, '', '', '', '2014-08-16 11:17:04', '2014-08-16 11:17:04', '0000-00-00 00:00:00', 0),
(10, 'This is a title', 'This is the body text', 'en', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(11, 'This is a title', 'This is the body text', 'en', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(12, 'This is a title', 'This is the body text', 'en', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(13, 'This is a title', 'This is the body text', 'en', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_foreign` (`permission_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `personel_description` text COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `photo_album_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `gallery_id` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `title`, `description`, `personel_description`, `added_by`, `photo_album_id`, `company_id`, `deleted`, `image`, `tags`, `created_at`, `updated_at`, `gallery_id`, `active`) VALUES
(1, 'Test Foto ', 'My Description', '', 1, 2, 1, 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `photo_albums`
--

CREATE TABLE IF NOT EXISTS `photo_albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `personel_description` text COLLATE utf8_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rendered_app`
--

CREATE TABLE IF NOT EXISTS `rendered_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modules` text COLLATE utf8_unicode_ci NOT NULL,
  `module_pages` text COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version_number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rendered_app`
--

INSERT INTO `rendered_app` (`id`, `modules`, `module_pages`, `company_id`, `created_at`, `updated_at`, `version_number`) VALUES
(3, 'a:2:{s:6:"module";a:2:{i:0;s:1:"3";i:1;s:1:"4";}s:4:"page";a:1:{i:0;s:2:"16";}}', '', 1, '2014-11-26 20:46:06', '2014-11-26 21:06:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'company', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_theme` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=56 ;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `name`, `description`, `image`, `company_id`, `start`, `end`, `created_at`, `updated_at`, `location`, `active`, `slug`, `default_theme`) VALUES
(52, 'Test Item 3', 'Test', '', 1, '2014-11-12 08:00:00', '2014-11-29 00:00:00', '2014-11-22 16:42:52', '2014-11-22 20:02:45', 'Rotterdam', 1, '', 0),
(53, 'Test Item', 'Test', '', 1, '2014-11-12 08:00:00', '2014-11-29 00:00:00', '2014-11-22 17:00:36', '2014-11-22 17:00:36', 'Rotterdam', 0, '', 0),
(54, 'Test Item 2', '', '', 1, '2014-11-12 08:00:00', '2014-11-29 00:00:00', '2014-11-22 17:00:46', '2014-11-22 17:00:46', 'Rotterdam', 0, '', 0),
(55, '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2014-11-22 20:25:27', '2014-11-22 20:25:27', '', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `soundcloud`
--

CREATE TABLE IF NOT EXISTS `soundcloud` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `artist` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `company_id` int(255) NOT NULL,
  `embed_code` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `soundcloud`
--

INSERT INTO `soundcloud` (`id`, `created_at`, `updated_at`, `name`, `url`, `artist`, `active`, `company_id`, `embed_code`, `deleted_at`) VALUES
(27, '2014-11-20 20:46:00', '2014-11-22 13:46:31', 'Lucky Number 13', 'https://soundcloud.com/blasterjaxx/blasterjaxx-lucky-number-13-original-mix-v2', 'Blasterjaxx', 1, 1, '', '0000-00-00 00:00:00'),
(28, '2014-11-20 20:49:04', '2014-11-20 20:49:04', '', '', '', 1, 2, '', '0000-00-00 00:00:00'),
(29, '2014-11-22 14:51:52', '2014-11-22 14:51:52', 'Physical', 'https://soundcloud.com/roul-and-doors/j-pearl-its-getting-physical-roul-and-doors-remix-strictly-rhythm-soundcloud-snippet', 'J Pearl', 1, 1, '', '0000-00-00 00:00:00'),
(30, '2014-11-26 16:05:18', '2014-11-26 16:05:18', 'You found me', 'https://soundcloud.com/blasterjaxx/blasterjaxx-ft-courtney-jenae-you-found-me-available-december-12', 'Blasterjaxx', 1, 1, '', '0000-00-00 00:00:00'),
(31, '2014-12-29 17:53:21', '2014-12-29 17:53:21', 'Supersonic Radio 73', '', 'Quintino', 0, 1, '<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/183607573&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>', '0000-00-00 00:00:00'),
(32, '2014-12-29 17:53:58', '2014-12-29 17:53:58', 'Supersonic Radio 74', '', 'Quintino', 1, 1, '<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/183607573&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `theme` text COLLATE utf8_unicode_ci NOT NULL,
  `file` text COLLATE utf8_unicode_ci NOT NULL,
  `default` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=59 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `company_id`, `theme`, `file`, `default`, `created_at`, `updated_at`) VALUES
(56, 'Testing themes', 1, 'a:3:{s:2:"h1";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:1:"1";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"2";}s:3:"top";a:3:{s:5:"width";s:1:"3";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"4";}s:5:"right";a:3:{s:5:"width";s:1:"5";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"6";}s:6:"bottom";a:3:{s:5:"width";s:1:"7";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"8";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:1:"9";i:1;s:1:"8";i:2;s:1:"7";i:3;s:1:"6";}s:7:"padding";a:4:{i:0;s:1:"5";i:1;s:1:"4";i:2;s:1:"3";i:3;s:1:"2";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:9:"no-repeat";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}s:9:"paragraph";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"0";}s:3:"top";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"0";}s:5:"right";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"0";}s:6:"bottom";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"0";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:7:"padding";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:9:"no-repeat";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}s:7:"article";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"0";}s:3:"top";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"0";}s:5:"right";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"0";}s:6:"bottom";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:5:"solid";s:12:"border-color";s:1:"0";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:7:"padding";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:9:"no-repeat";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}}', 'kpXM6u3aeGRQNcdLpLto9AtIBoybToyMAkQYMoeJ5j5vzkfhR72w1VkcEtsNRXgfd752QaM9jPG2JjDGGlZxg9zz4qKgcolAeNIZ.css', 0, '2014-08-24 11:19:25', '2014-08-24 11:19:25'),
(57, 'Testing themes', 1, 'a:5:{s:4:"name";s:14:"Testing themes";s:7:"default";s:1:"1";s:2:"h1";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:1:"1";s:12:"border-style";s:1:"2";s:12:"border-color";s:8:"0,17,255";}s:3:"top";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:8:"0,17,255";}s:5:"right";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:8:"0,17,255";}s:6:"bottom";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:8:"0,17,255";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:7:"padding";a:4:{i:0;s:2:"10";i:1;s:1:"0";i:2;s:1:"0";i:3;s:2:"10";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:9:"no-repeat";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}s:9:"paragraph";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:0:"";s:12:"border-style";s:1:"0";s:12:"border-color";s:0:"";}s:3:"top";a:3:{s:5:"width";s:0:"";s:12:"border-style";s:1:"0";s:12:"border-color";s:0:"";}s:5:"right";a:3:{s:5:"width";s:0:"";s:12:"border-style";s:1:"0";s:12:"border-color";s:0:"";}s:6:"bottom";a:3:{s:5:"width";s:0:"";s:12:"border-style";s:1:"0";s:12:"border-color";s:0:"";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:0:"";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";}s:7:"padding";a:4:{i:0;s:2:"10";i:1;s:2:"10";i:2;s:2:"10";i:3;s:2:"10";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:9:"no-repeat";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}s:7:"article";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:1:"5";s:12:"border-style";s:1:"0";s:12:"border-color";s:8:"0,17,255";}s:3:"top";a:3:{s:5:"width";s:1:"5";s:12:"border-style";s:1:"0";s:12:"border-color";s:8:"0,17,255";}s:5:"right";a:3:{s:5:"width";s:1:"5";s:12:"border-style";s:1:"0";s:12:"border-color";s:8:"0,17,255";}s:6:"bottom";a:3:{s:5:"width";s:1:"5";s:12:"border-style";s:1:"0";s:12:"border-color";s:8:"0,17,255";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"5";i:3;s:1:"0";}s:7:"padding";a:4:{i:0;s:0:"";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:9:"no-repeat";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}}', 'css.css', 1, '2014-11-27 20:08:29', '2014-11-29 22:38:08'),
(58, 'test', 1, 'a:5:{s:4:"name";s:4:"test";s:7:"default";s:1:"1";s:2:"h1";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"1";s:12:"border-color";s:1:"1";}s:3:"top";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}s:5:"right";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"2";s:12:"border-color";s:1:"0";}s:6:"bottom";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:7:"padding";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:8:"repeat-y";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}s:9:"paragraph";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}s:3:"top";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}s:5:"right";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}s:6:"bottom";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:7:"padding";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:8:"repeat-y";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}s:7:"article";a:10:{s:6:"border";a:4:{s:4:"left";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}s:3:"top";a:3:{s:5:"width";s:2:"00";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}s:5:"right";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}s:6:"bottom";a:3:{s:5:"width";s:1:"0";s:12:"border-style";s:1:"0";s:12:"border-color";s:1:"0";}}s:10:"font-color";s:11:"255,255,255";s:6:"margin";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:7:"padding";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}s:16:"background-image";s:26:"Geen afbeeldingen gevonden";s:12:"repeat-image";s:8:"repeat-x";s:16:"background-color";s:11:"255,255,255";s:1:"x";s:0:"";s:1:"y";s:0:"";s:13:"border-radius";a:4:{i:0;s:1:"0";i:1;s:1:"0";i:2;s:1:"0";i:3;s:1:"0";}}}', 'css.css', 0, '2014-11-27 20:57:33', '2014-11-29 22:38:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `confirmation_code`, `confirmed`, `created_at`, `updated_at`, `remember_token`, `website`, `surname`, `first_name`) VALUES
(1, 'admin', 'admin@admin.nl', '$2y$10$nATzWxq3uakN7hbA84KkpeLqOZ.1xKUKYuoeLvGKhRigaOnLbhh96', '', 1, '2014-05-22 10:40:02', '2014-11-22 14:53:34', 'YsQctpZMO4nUOcGnhsb7ViSn1eCf96BA84YiLLGmgT4yNFNOtFKj3dnPhEV5', '', '', ''),
(2, 'Dennis', 'd_sweben@hotmail.com', '$2y$10$rRfU1rdsMdRPBOw/BOODxOVkaGewZ81IhDbsA7ey4Z8Y4roIzQ95W', '', 1, '2014-05-22 12:09:42', '2014-05-22 12:09:42', NULL, '', '', ''),
(3, 'Dvangrondelle', 'd.vangrondelle@youwe.nl', '$2y$10$.odUVva.V0/aR4snVWQIh.BXOAHVe34I2Dz9uvRftdH0kgu1G.qHu', 'ff9edd50e6f1f5fba6c366d35db41213', 0, '2014-08-17 15:54:10', '2014-08-17 15:54:10', NULL, '', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `assigned_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `company_menu`
--
ALTER TABLE `company_menu`
  ADD CONSTRAINT `company_menu_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `company_menu_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`);

--
-- Constraints for table `company_module`
--
ALTER TABLE `company_module`
  ADD CONSTRAINT `company_module_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `company_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`);

--
-- Constraints for table `company_schedule`
--
ALTER TABLE `company_schedule`
  ADD CONSTRAINT `company_schedule_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `company_schedule_schedule_id_foreign` FOREIGN KEY (`schedule_id`) REFERENCES `schedules` (`id`);

--
-- Constraints for table `company_user`
--
ALTER TABLE `company_user`
  ADD CONSTRAINT `company_user_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `company_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `company_user_followers`
--
ALTER TABLE `company_user_followers`
  ADD CONSTRAINT `company_user_followers_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `company_user_followers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `modules_autogenerate`
--
ALTER TABLE `modules_autogenerate`
  ADD CONSTRAINT `modules_autogenerate_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `modules_autogenerate_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
