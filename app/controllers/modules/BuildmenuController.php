<?php namespace modules;


use Input;
use Redirect;
use View;
use Company;
use BaseController;

class BuildmenuController extends BaseController {

    public function index($id)
    {
        $data['company'] = Company::find(1);

        //dd($company->modulePages);

        return View::make('modules.buildmenu.index', $data);
    }

}