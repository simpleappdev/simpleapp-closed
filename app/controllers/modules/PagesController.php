<?php namespace modules;

use View;
use Company;
use Auth;
use Input;
use Redirect;
use ModulePages;
use Page;
use Helpers\Modules\PagesH;

class  PagesController extends \BaseController {

    public $data = array();

    public function __construct(){
        $this->data['module_name'] = 'pages';
        $this->module_id = 5;
        $this->data['module_id'] = $this->module_id;
        $this->data['modal_title'] = 'Are you sure that you want to delete this page: ';
    }
	/**
	 * Display a listing of the resource.
	 * GET /modules/pages
	 *
	 * @return Response
	 */
	public function index($company_id)
	{
		$company = Company::find($company_id);

		$this->data['company'] = $company;

        return View::make('modules.pages.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /modules/pages/create
	 *
	 * @return Response
	 */
	public function create($id)
	{
        $this->data['company'] = $company = Company::find($id);
        $this->data['default_theme'] = PagesH::generateDefault($company->themes);
        return View::make('modules.pages.new', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /modules/pages
	 *
	 * @return Response
	 */
	public function store($id)
	{


        $fields = Input::except(['_token', 'title', 'content']);
        $fields['content'] = serialize($this->createSpecialArray(Input::get('title'), Input::get('content')));
        $fields['company_id'] = $id;
        $fields['pagename'] = 'test.html';

        $page = new Page;
        $page->create($fields);



//        $theme = new \Helpers\HtmlCreator($page->company, $page->pagename);
//        $theme->createFullPage($fields);

        return Redirect::back()->with(['message' => 'Page created!']);
	}

	/**
	 * Display the specified resource.
	 * GET /modules/pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $pages = Company::find($id);

        $data['pages'] = $pages->modulePages;

		return View::make('modules.pages.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /modules/pages/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $page = Page::find($id);
        $company = Company::find($page->company_id);

        $this->data['default_theme'] = PagesH::generateDefault($company->themes, $page->theme);
        $this->data['page'] = $page;
        $this->data['company'] = $company;
        $this->data['page_data'] = unserialize($page->content);
        $data = unserialize($page->content);

        return View::make('modules.pages.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /modules/pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

        $fields = Input::except(['_token', 'title', 'content']);
        $fields['content'] = serialize($this->createSpecialArray(Input::get('title'), Input::get('content')));

		$page = Page::find($id);
        $page->update($fields);

        return Redirect::back()->with('message', 'Edited!');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /modules/pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $page = Page::find($id);

        $page->destroyed = 1;
        $page->save();

        return Redirect::to('company/modules/pages/'.$page->company_id);
	}

    /**
     * @param $titel Contains the input::title
     * @param $content Contains the input::title
     * @return array contains the array merged
     *
     * To do: replace this function to another class
     */
    private function createSpecialArray($titel, $content){
        $data = array();
        $counter = 0;
        foreach($titel AS $value){
            $data[] = array('title' => $value, 'content' => $content[$counter]);
            $counter++;
        }
        return $data;
    }

}