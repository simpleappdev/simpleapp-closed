@extends('master') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <p class="lead">Empty menu</p>
            <div class="list-group">
                <a href="#" class="list-group-item">Printable</a>
            </div>
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    @if ( Session::get('error') )
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">Oops...</h3>
                            </div>
                            <div class="panel-body">
                                {{{ Session::get('error') }}}
                            </div>
                        </div>
                    @endif

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{Lang::get('login.password_forgot_title')}}</h3>
                    </div>
                
                    <div class="panel-body">
                        <div class=" col-md-9 col-lg-9 ">
                        <p>Enter your email and we send you a new password</p>
                            <form role="form" action="" method="post">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" id="" placeholder="{{Lang::get('login.placeholder_email')}}">
                                </div>
                                <button type="submit" class="btn btn-default">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{url('user/forgot_password')}}">{{Lang::get('login.forgot_password')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
