<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIframeToSoundcloudTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('soundcloud', function(Blueprint $table)
		{
			$table->text('embed_code');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('soundcloud', function(Blueprint $table)
		{
			$table->dropColumn('embed_code');
		});
	}

}
