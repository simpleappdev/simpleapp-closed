<?php

/*
 * Made this model, because the normal User model extends ConfideUser
 * and because of that it is impossible to add data to the database.
 *
 * With Eloquent, it is possible. So for those cases, there is this model
 * which extends Eloquent and enables database traffic.
 *
 */

use Zizaco\Entrust\HasRole;

class UserPlain extends Eloquent {

    use HasRole;

    protected $guarded = [];

    public static $rules = array(
        'email' => 'required|email',
        'password' => 'required|between:4,11|confirmed',
//        'password_confirmation' => 'required'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getCompanies()
    {
        return $this->belongsToMany('Company');
    }

    public function getUser(){
        return $this->hasMany('Company');
    }

    public function getRole(){
        return $this->belongsTo('Role');
    }

    public function logs($limit = null){
        $query = $this->hasMany('Logbook');

        if($limit != null){
            $query = $this->hasMany('Logbook')->limit($limit)->get();
        }
        return $query;
    }
}