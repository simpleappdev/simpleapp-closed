<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;

class ModulesTableSeeder extends Seeder {

    public function run()
    {
        $modules[] = array(
            'id' => 1,
            'title' => 'Events',
            'type' => 'events',
            'active' => 1,
            'hard_coded' => 0,
        );

        $modules[] = array(
            'id' => 2,
            'title' => 'Soundcloud',
            'type' => 'page',
            'active' => 1,
            'hard_coded' => 0,
        );

        $modules[] = array(
            'id' => 3,
            'title' => 'Pages',
            'type' => 'page',
            'active' => 1,
            'hard_coded' => 1,
        );

        $modules[] = array(
            'id' => 4,
            'title' => 'Gallery',
            'type' => 'gallery',
            'active' => 1,
            'hard_coded' => 0,
        );



        foreach($modules as $module){
            Module::create($module);
        }



    }

}
