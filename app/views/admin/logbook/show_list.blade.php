@extends('admin.template.master')
@section('admin_content')

    <style>
        .whitespace-bottom {
            margin-bottom: 20px;
        }
    </style>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{Lang::get('logbook.header_logs')}}</h1>
            </div>

            <div class="col-lg-12">
                <h2>{{Lang::get('logbook.header_search')}}</h2>
                
            </div>

            <div class="col-lg-12">
                {{--<h2>Logs</h2>--}}

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        	<tr>                    		
	                            <th>{{Lang::get('logbook.logbook_event')}}</th>
	                            <th>{{Lang::get('logbook.logbook_action')}}</th>
	                            <th>{{Lang::get('logbook.created_at')}}</th>
	                        </tr>
                        </thead>

                        <tbody class="table-striped">
                        @foreach($logs as $log)
                            <tr 
								@if($log->level == 0)  {{'class="alert alert-default"'}} @endif
								@if($log->level == 1)  {{'class="alert alert-primary"'}} @endif
								@if($log->level == 2)  {{'class="alert alert-success"'}} @endif
								@if($log->level == 3)  {{'class="alert alert-info"'}} @endif
								@if($log->level == 4)  {{'class="alert alert-warning"'}} @endif
								@if($log->level == 5)  {{'class="alert alert-danger"'}} @endif
                            >
                               
                                <td>{{Lang::get($log->event)}}</td>
                                <td>{{Lang::get($log->action)}}</td>
                                <td>{{$log->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{$logs->links()}}
            </div>
        </div>

@endsection