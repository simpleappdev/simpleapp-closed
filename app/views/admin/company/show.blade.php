@extends('admin.template.master')
@section('admin_content')
<style>
    .hidden {
        display: none;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{Lang::get('company.companyHeader')}}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12" >

        <a href="{{URL::route('admin.companies.index') }}" class="btn btn-default">{{Lang::get('company.companyBackButton')}}</a>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{$company->company_name}}</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>

                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>{{Lang::get('general.company_slug')}}</td>
                                <td>{{$company->slug}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.branch')}}</td>
                                @if(isset($company->branch->id))
                                    <td>{{$company->branch->description}}</td>
                                @else
                                    <td></td> {{--Empty table data for layout. Otherwise there is some empty space--}}
                                @endif
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.user')}}</td>
                                @if(isset($company->user->id))
                                    <td>{{$company->user->first_name . ' ' . $company->user->surname}}</td>
                                @else
                                    <td></td> {{--Empty table data for layout. Otherwise there is some empty space--}}
                                @endif
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.email')}}</td>
                                <td>{{$company->email}}</td>
                            </tr>
                            <td>{{Lang::get('general.phone_number')}}</td>
                            <td>{{$company->phone}}</td>
                            <tr>
                            <tr>
                                <td>{{Lang::get('general.address')}}</td>
                                <td>{{$company->address}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.city')}}</td>
                                <td>{{$company->city}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.country')}}</td>
                                <td>{{$company->country}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.zipcode')}}</td>
                                <td>{{$company->zipcode}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.status')}}</td>
                                <td>{{$company->status}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.active')}}</td>
                                <td>{{$company->active == 1 ? Lang::get('general.yes') : Lang::get('general.no')}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $clicked = false;
        $('#unlock-form').click(function(){
            if($clicked == false){
                $(this).find('span').removeClass('hidden');
                $('.locked').each(function(){
                    $(this).removeAttr('disabled');
                });
                $clicked = true;
            } else {
                $('#edit-form').submit();
            }

        });
    });
</script>

@endsection