@extends('master')
@section('content')

<style>

    /*
        .fileUpload {
            position: relative;
            overflow: hidden;

        }
        .fileUpload input.upload {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
    */

</style>

<div class="container">
    <div class="row col-lg-3"  id="side-menu">
        <div class="col-md-12">
            <p class="lead" >Image Preview</p>

        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">My Images</h3>
                </div>
                <div class="panel-body">
                    <div class="row col-md-12">
                        <table width="100%">
                            <tr>
                                <td>Omschrijving</td>
                                <td>Actie</td>
                                <td>Geplaatst door</td>
                            </tr>

                        @foreach($company->privateImages AS $image)
                            <tr>
                                <td>{{$image->title}}</td>
                                <td>Verwijderen</td>
                                <td>Anoniem</td>
                            </tr>
                        @endforeach
                        </table>
                    </div>
                    @include('_partials.errors')
                </div>
            </div>
        </div>
    </div>
</div>


@endsection