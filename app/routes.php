<?php

Route::get('c_test', function(){
    //$user = User::find(2);
//    dd($user);
    $user = new User();

    $user->username = 'asdasd';
    $user->email = 'angorusadf@gmail.com';
    $user->password = '12312312';
    $user->password_confirmation = '12312312';

    $user->save();

});

Route::group(array('before' => 'auth'), function(){
    Route::controller('translations', 'Barryvdh\TranslationManager\Controller');
});

Route::group(['namespace' => 'payment'], function(){
    include(app_path().'/routes/payment.php');
});

Route::group(array('prefix' => LaravelLocalization::setLocale()), function(){

  include(app_path().'/routes/login.php');
  include(app_path().'/routes/non-member.php');

  Route::group(['before' => 'auth'], function(){
    include(app_path().'/routes/admin.php');
    include(app_path().'/routes/modules.php');
    include(app_path().'/routes/company.php');
    include(app_path().'/routes/user.php');
    include(app_path().'/routes/default.php');
    include(app_path() . '/routes/home.php');

    Route::group(['prefix' => 'app/render'], function(){
      Route::get('{any}',                           'RenderAppController@index');
      Route::post('{any}',                           'RenderAppController@save');
    });

    Route::group(['namespace' => 'modules', 'prefix' => ''], function(){
      include(app_path().'/routes/modules.php');
    });

  });

});

Route::get('test/menu','TestController@menu');





Validator::extend('alphaspacepunc', function ($attribute, $value, $parameters) {
    return preg_match('/^[a-zA-Z0-9,\.\s]*$/', $value);
});

Validator::replacer('alphaspacepunc', function ($message, $attribute, $parameters) {
    // this might not be how its done ... i will have to check the source
    return "String must contain only alphanumeric, spaces, commas or periods.";
});

Validator::extend('current_password', function ($attribute, $value, $parameters) {

    if(Hash::check($value, Auth::user()->password)) {
        return true;
    }

    Helpers\LogbookH::createLog('logbook.profile', 0, 'logbook_messages.wrong_password', 0, 1 );


});

Validator::replacer('current_password', function ($message, $attribute, $parameters) {
    // this might not be how its done ... i will have to check the source
    return Lang::get('validation.current_password_wrong');
});



Route::get('/pdf', function(){

});

Route::get('test/pdf/{bill_id}', 'TestController@pdf');

