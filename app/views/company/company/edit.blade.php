@extends('master')
@section('content')

<style>
    /* uses font awesome for social icons */
    @import url(http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);

    .page-header{
        text-align: center;
    }

    /*social buttons*/
    .btn-social{
        color: white;
        opacity:0.9;

    }
    .btn-social:hover {
        color: white;
        opacity:1;
    }
    .btn-facebook {
        background-color: #3b5998;
        opacity:0.9;
    }
    .btn-twitter {
        background-color: #00aced;
        opacity:0.9;
    }
    .btn-linkedin {
        background-color:#0e76a8;
        opacity:0.9;
    }
    .btn-github{
        background-color:#000000;
        opacity:0.9;
    }
    .btn-google {
        background-color: #c32f10;
        opacity: 0.9;
    }
    .btn-stackoverflow{
        background-color: #D38B28;
        opacity: 0.9;
    }

    /* resume stuff */

    .bs-callout {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: #eee;
        border-image: none;
        border-radius: 3px;
        border-style: solid;
        border-width: 1px 1px 1px 5px;
        margin-bottom: 5px;
        padding: 20px;
    }
    .bs-callout:last-child {
        margin-bottom: 0px;
    }
    .bs-callout h4 {
        margin-bottom: 10px;
        margin-top: 0;
    }

    .bs-callout-danger {
        border-left-color: #d9534f;
    }

    .bs-callout-danger h4{
        color: #d9534f;
    }

    .resume .list-group-item:first-child, .resume .list-group-item:last-child{
        border-radius:0;
    }

    /*makes an anchor inactive(not clickable)*/
    .inactive-link {
        pointer-events: none;
        cursor: default;
    }

    .resume-heading .social-btns{
        margin-top:15px;
    }
    .resume-heading .social-btns i.fa{
        margin-left:-5px;
    }

    .social-btn-holder {
        margin-left: 10px;
    }



    @media (max-width: 992px) {
        .resume-heading .social-btn-holder{
            padding:5px;
        }
    }


    /* skill meter in resume. copy pasted from http://bootsnipp.com/snippets/featured/progress-bar-meter */

    .progress-bar {
        text-align: left;
        white-space: nowrap;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        cursor: pointer;
    }

    .progress-bar > .progress-type {
        padding-left: 10px;
    }

    .progress-meter {
        min-height: 15px;
        border-bottom: 2px solid rgb(160, 160, 160);
        margin-bottom: 15px;
    }

    .progress-meter > .meter {
        position: relative;
        float: left;
        min-height: 15px;
        border-width: 0px;
        border-style: solid;
        border-color: rgb(160, 160, 160);
    }

    .progress-meter > .meter-left {
        border-left-width: 2px;
    }

    .progress-meter > .meter-right {
        float: right;
        border-right-width: 2px;
    }

    .progress-meter > .meter-right:last-child {
        border-left-width: 2px;
    }

    .progress-meter > .meter > .meter-text {
        position: absolute;
        display: inline-block;
        bottom: -20px;
        width: 100%;
        font-weight: 700;
        font-size: 0.85em;
        color: rgb(160, 160, 160);
        text-align: left;
    }

    .progress-meter > .meter.meter-right > .meter-text {
        text-align: right;
    }

    #editable input{
        width: 100%;
        outline: none;
        border: 0px;
    }

.clear {
    clear:both;
}
</style>


<div class="container">
<div class="resume">

    <div class="row">
        <div class="col-lg-3">
            <div class="col-lg-12">
                @include('includes.modules_menu')
            </div>
        </div>

    <div class="col-lg-9">
        <header class="page-header">
            <h1 class="page-title">{{$company->company_name}}</h1>
            <small> <i class="fa fa-clock-o"></i> Last Updated on: <time>{{$company->updated_at->format('D-M-Y H:i')}}</time></small>
        </header>
        <div class="panel panel-default">
            <div class="panel-heading resume-heading">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-xs-12 col-sm-4">
                            <figure>
                                <img class="img-circle img-responsive" alt="" src="http://placehold.it/300x300">
                            </figure>

                            <div class="row">
                                <div class="col-xs-12 social-btns">

                                    <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                        <a href="#" class="btn btn-social btn-block btn-facebook">
                                            <i class="fa fa-facebook"></i> </a>
                                    </div>

                                    <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                        <a href="#" class="btn btn-social btn-block btn-twitter">
                                            <i class="fa fa-twitter"></i> </a>
                                    </div>

                                    <div class="col-xs-3 col-md-1 col-lg-1 social-btn-holder">
                                        <a href="#" class="btn btn-social btn-block btn-linkedin">
                                            <i class="fa fa-linkedin"></i> </a>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 col-sm-8">
                            <ul class="list-group" id="editable">
                                {{Form::open(array('id' => 'edit-company-profile'))}}
                                <li class="list-group-item">{{$company->owner->username}}</li>
                                <li class="list-group-item">{{Form::text('address', $company->address)}}</li>
                                <li class="list-group-item">{{Form::text('zipcode', $company->zipcode)}}</li>
                                <li class="list-group-item">{{Form::text('city', $company->city)}}</li>
                                <li class="list-group-item">{{Form::text('country', $company->country)}}</li>
                                <li class="list-group-item">{{Form::text('address', $company->email)}}</li>
                                <li class="list-group-item">{{Form::text('address', $company->phone)}}</li>
                                {{Form::close()}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bs-callout bs-callout-danger">
                <h4>Summary</h4>
                {{$company->information}}
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
</div>
</div>


@endsection
