@extends('master')
@section('content')
<style>
    .page-title {
        text-align: center;
    }

    #ajax-response {
        text-align: center;
        padding-bottom: 15px;
        border-bottom: 1px solid #e6e6e6;
        display: none;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('includes.manage_module')
            @include('includes.modules_menu')

        </div>

        <div class="col-md-9">
            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">{{ucfirst($module_name)}}</h1>
                    @if(count($company->getGalleries()) >= 2)
                    <small> <i class="fa fa-clock-o"></i> Last Created on:
                        <time>
                            {{$company->getGalleries()->last()->created_at->format('D-M-Y H:i')}}
                        </time>
                    </small>
                    @endif
                </header>
                <h1 id="ajax-response"></h1>

                <table class="table table-striped table-bordered table-hover table-condensed">
                    <tr class="info">
                        <th class="col-md-2">{{Lang::get('modules/general.title')}}</th>
                        <th class="col-md-1">{{Lang::get('modules/general.active')}}</th>
                        <th>{{Lang::get('modules/general.description')}}</th>
                        <th class="col-md-2">{{Lang::get('modules/general.action')}}</th>
                    </tr>
                    @foreach($company->activePages AS $page)
                        <tr>
                            <td class="item-name">{{ $page->titel }}</td>
                            <td>{{$page->active == 1 ? Lang::get('general.yes') : Lang::get('general.no') }}</td>
                            <td>{{ str_limit($page->personal_description, 50) }}</td>
                            <td class="text-center">
                                <a href="{{url('modules/pages/'.$page->id.'/show')}}" class="btn btn-sm btn-info glyphicon glyphicon-eye-open"></a>
                                <a href="{{url('modules/pages/'.$page->id.'/edit')}}" class="btn btn-sm btn-default glyphicon glyphicon-pencil"></a>
                                <button class="btn btn-sm btn-danger glyphicon glyphicon-trash" data-item-type="{{$module_name}}" data-item-id="{{$page->id}}" data-toggle="modal" data-target="#show_delete_dialog"></button>
                            </td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
</div>

@endsection