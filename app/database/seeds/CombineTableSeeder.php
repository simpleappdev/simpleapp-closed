<?php

class CombineTableSeeder extends Seeder {

	public function run()
	{


        //User ID's for admins
        $admins = [1,2,3,4];
        foreach($admins as $user){
            DB::table('assigned_roles')->insert(['user_id' => $user, 'role_id' => 1]);
            $current_user = User::find($user);
            $this->command->info('Admin user: ' . $current_user->username);
        }

        foreach($admins as $user){
            DB::table('company_user')->insert(['user_id' => $user]);
            $current_user = User::find($user);
            $this->command->info('User with company 1: ' . $current_user->username);
        }

	}

}