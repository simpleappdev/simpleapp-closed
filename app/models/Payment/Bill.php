<?php

class Bill extends \Eloquent {
	protected $fillable = [];

	protected $table = 'payment_bills';

	public function getDates(){
		return ['created_at', 'payed_date'];
	}

	public function company(){
		return $this->belongsTo('Company');
	}

	public function plan(){
		return $this->belongsTo('Plan');
	}
}