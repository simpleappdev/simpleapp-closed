@extends('master')
@section('content')
    <div class="container" id="modules-pages-module">
        <div class="row col-lg-3"  id="side-menu">
            @include('includes.manage_module')
            @include('includes.modules_menu')
        </div>
        <div class="row">

            {{Form::open()}}

            <div class="col-md-9">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Add Song</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center">
                                <img alt="User Pic" src="{{url('images/modules/soundcloud_logo.png')}}" class="img-responsive"> </div>

                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    {{Form::open()}}

                                    <tr>
                                        <td>Titel</td>
                                        <td>{{Form::text('titel', $page->titel)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Theme</td>
                                        <td>{{$default_theme}}</td>
                                    </tr>
                                    <tr>
                                        <td>Active</td>
                                        <td>{{Form::select('active', array('' => 'Make your choice', 1 => 'Yes', 0 => 'No'), $page->active)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Personal Description</td>
                                        <td>{{Form::textarea(
                                                'personal_description',
                                                $page->personal_description,
                                                [
                                                    'placeholder' => 'Place here your personal info about this page',

                                                ])
                                            }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                @include('_partials.errors')
                            </div>
                        </div>
                    </div>
                </div>
                <ul id="sortable">
                    @if($page_data != null)
                        @foreach($page_data as $content)

                            <li class="content-box">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <input type="text" class="edit-page-header" placeholder="Uw tekst" value="{{$content['title']}}" name="title[]">
                                    </div>
                                    <div class="panel-body">
                                        <textarea name="content[]" value="{{$content['content']}}" class="edit-page-area" placeholder="Plaats hier uw tekst"></textarea>

                                    </div>
                                    <div class="panel-footer">
                                            <span type="button" class="btn btn-success">
                                                <i class="glyphicon glyphicon-move"></i>
                                                Replace</span>
                                        <button type="button" class="btn btn-primary add-content empty-values">
                                            Add</button>
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-danger remove-content hide">
                                                <i class="fa fa-trash-o"></i> Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        @endforeach
                    @else
                        <li class="content-box">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <input type="text" name="title[]" class="edit-page-header" placeholder="Uw tekst">
                                </div>
                                <div class="panel-body">
                                    <textarea name="content[]" class="edit-page-area" placeholder="Plaats hier uw tekst"></textarea>
                                </div>
                                <div class="panel-footer">
                                            <span type="button" class="btn btn-success">
                                                <i class="glyphicon glyphicon-move"></i>
                                                Replace</span>
                                    <button type="button" class="btn btn-primary add-content empty-values">
                                        Add</button>
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-danger remove-content hide">
                                            <i class="fa fa-trash-o"></i> Delete</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endif

                </ul>
                <div class="panel-info">
                    <div class="panel-heading">
                        {{Form::submit(Lang::get('general.save'), ['class' => 'btn btn-info'])}}
                        {{Form::close()}}
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{HTML::script('template/front/js/scripts/modules/pages_create.js')}}



@endsection

































