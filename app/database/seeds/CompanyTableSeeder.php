<?php

// Composer: "fzaninotto/faker": "v1.3.0"


class CompanyTableSeeder extends Seeder {

	public function run()
	{
		Company::truncate();

		$data = array(
			
            'company_name' => 'Triple C',
			'slug' => 'triple',
			'information' => 'OOOm is het nieuwe bedrijf dat zich verdiept in moderne internet technology en ontwikkelingen. Dit is een fictieve text die alleen geschreven is voor het seeden van de database wat erg belangrijk is om zo sneller te kunnen werken en niet telkens opnieuw een record aan te maken in de database. Dit is tevens het "Bedrijfs informatie".',
			);
		Company::create($data);

		
	}

}