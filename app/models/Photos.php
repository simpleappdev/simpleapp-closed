<?php

class Photos extends \Eloquent {
	protected $fillable = ['title', 'company_id', 'added_by', 'album_id', 'personel_description','deleted'];

	protected $table = 'photos';

    public function getDates(){
        return ['created_at', 'updated_at'];
    }

	public function getAlbum(){
		return $this->hasOne('PhotoAlbum');
	}
}