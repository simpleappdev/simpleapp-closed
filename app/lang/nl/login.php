<?php

return array (
  'password_forgot_title' => 'Uw wachtwoord vergeten?',
  'placeholder_email' => 'Vul uw email in',
  'forgot_password' => 'Wachtwoord vergeten?',
  'user_email' => 'Uw email',
  'user_password' => 'Wachtwoord',
  'placeholder_password' => 'Vul uw wachtwoord in',
  'remember_me' => 'Onthoud mij',
  'login_button' => 'Inloggen',
  'your_email' => 'Uw email',
);
