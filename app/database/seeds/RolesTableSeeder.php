<?php

class RolesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('roles')->truncate();
        
		\DB::table('roles')->insert(array (
			0 => 
			array (
				'id' => '1',
				'name' => 'admin',
				'description' => 'Webmaster',
				'created_at' => '2015-01-05 19:16:04',
				'updated_at' => '2015-01-17 21:49:17',
			),
			1 => 
			array (
				'id' => '2',
				'name' => 'user',
				'description' => 'A default signed up user',
				'created_at' => '2015-01-05 19:16:04',
				'updated_at' => '2015-01-17 21:49:38',
			),
			2 => 
			array (
				'id' => '3',
				'name' => 'translator',
				'description' => 'Manage all translations for the website',
				'created_at' => '2015-01-05 19:16:04',
				'updated_at' => '2015-01-17 21:50:03',
			),
			3 => 
			array (
				'id' => '4',
				'name' => 'helpdesk',
				'description' => '',
				'created_at' => '2015-01-05 19:16:04',
				'updated_at' => '2015-01-05 19:16:04',
			),
			4 => 
			array (
				'id' => '5',
				'name' => 'Developer',
				'description' => 'A role for users that are developing SimpleApp',
				'created_at' => '2015-01-17 21:44:03',
				'updated_at' => '2015-01-17 21:44:03',
			),
		));
	}

}
