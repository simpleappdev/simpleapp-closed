
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sample Invoice</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <style>
        @import url(http://fonts.googleapis.com/css?family=Bree+Serif);
        body, h1, h2, h3, h4, h5, h6{
            font-family: 'Bree Serif', serif;
        }
    </style>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-xs-6">
            <h1>
                <a href="https://twitter.com/tahirtaous">
                    <img src="logo.png">
                    Logo here
                </a>
            </h1>
        </div>
        <div class="col-xs-6 text-right">
            <h1>{{Lang::get('payment.invoice')}}</h1>
            <h1><small>{{Lang::get('payment.invoice')}} #001</small></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>From: <a href="#">{{Config::get('payment.company_name')}}</a></h4>
                </div>
                <div class="panel-body">
                    <p>
                        {{Config::get('payment.address')}} <br>
                        {{Config::get('payment.zip_code')}} <br>
                        {{Config::get('payment.city')}} <br>
                        {{Config::get('payment.country')}} <br>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-5 col-xs-offset-2 text-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>{{Lang::get('general.to')}} : <a href="#">{{$company->company_name}}</a></h4>
                </div>
                <div class="panel-body">
                    <p>
                        Address <br>
                        details <br>
                        more <br>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- / end client details section -->
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>
                <h4>{{Lang::get('payment.payment_plan')}}</h4>
            </th>
            <th>
                <h4>{{Lang::get('general.description')}}</h4>
            </th>
            <th>
                <h4>{{Lang::get('payment.tax_percent')}}</h4>
            </th>
            <th>
                <h4>{{Lang::get('payment.excluding_tax')}}</h4>
            </th>
            <th>
                <h4>{{Lang::get('payment.including_tax')}}</h4>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Plan type: {{$bill->plan->name}}</td>
            <td><a href="#">{{Lang::get('payment.month_subscribtion')}}</a></td>
            <td class="text-right">-</td>
            <td class="text-right">&euro; {{number_format($bill->price, 2, ',', '.')}}</td>
            <td class="text-right">&euro; {{number_format($bill->price * 1.21, 2, ',', '.')}}</td>
        </tr>
        </tbody>
    </table>
    <div class="row text-right">
        <div class="col-xs-2 col-xs-offset-8">
            <p>
                <strong>
                    {{Lang::get('payment.sub_total')}} : <br>
                    {{Lang::get('payment.tax')}} : <br>
                    {{Lang::get('payment.total')}} : <br>
                </strong>
            </p>
        </div>
        <div class="col-xs-2">
            <strong>
                 &euro; {{number_format($bill->price, 2, ',', '.')}}<br>
                 21%<br>
                 &euro; {{number_format($bill->price * 1.21, 2, ',', '.')}}<br>
            </strong>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4>{{Lang::get('payment.bank_details')}}</h4>
                </div>
                <div class="panel-body">
                    <p>Your Name</p>
                    <p>Bank Name</p>
                    <p>SWIFT : --------</p>
                    <p>Account Number : --------</p>
                    <p>IBAN : --------</p>
                </div>
            </div>
        </div>
        <div class="col-xs-7">
            <div class="span7">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>{{Lang::get('payment.contact_details')}}</h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Email : you@example.com <br><br>
                            Mobile : -------- <br> <br>
                            Twitter : <a href="https://twitter.com/tahirtaous">@TahirTaous</a>
                        </p>
                        <h4>Payment should be made by Bank Transfer</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>