<div class="col-md-2">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <h3 class="panel-title"><a href="{{url('events/new/'.$id)}}">Voeg toe</a></h3>
	    </div>
	</div>
	
	@if($user->getCompanies)
		<div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title">Bekijk agenda van</h3>
		    </div>

		    <div class="panel-body">
				@foreach($user->getCompanies AS $company)
					<a href="{{url('events/'.$company->id)}}">{{$company->company_name}}</a>
				@endforeach
		    </div>
		</div>
	@endif
</div>


