@extends('admin.template.master')
@section('admin_content')

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">{{Lang::get('permissions.PermissionHeader');}}</h1>
            </div>

            <div class="col-sm-4 col-lg-4 col-md-4">

                <h2>{{Lang::get('permissions.PermissionEdit');}}</h2>

                {{ Form::model($permission, ['route' => ['admin.permissions.update', $permission->id], 'method' => 'PUT'])}}
                {{ Form::label('permission_name', Lang::get('permissions.PermissionLabelName'))}}
                {{ Form::text('name', null, ['class' => 'form-control'])}}

                {{ Form::Label('display_name', Lang::get('permissions.PermissionLabelDisplayname'))}}
                {{ Form::text('display_name', null, ['class' => 'form-control'])}}

                {{ Form::submit(Lang::get('permissions.PermissionEdit'), ['class' => 'btn btn-default'])}}
                {{ Form::close()}}

            </div>
        </div>
    </div>
@endsection