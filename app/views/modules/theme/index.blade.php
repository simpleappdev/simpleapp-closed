@extends('master')
@section('content')
    <div class="container">
        <div class="col-lg-3">
            @include('includes.manage_module')
            @include('includes.modules_menu')
        </div>
		<div class="col-md-9">
			<table class="table table-striped table-bordered table-hover table-condensed">
                <tr class="info">
                    <th class="col-md-8">Name</th>
                    <th class="col-md-1">Default</th>
                    <th class="col-md-3">Action</th>
                </tr>
            @foreach($company->themes AS $theme)
                <tr>
                    <td>{{ $theme->title }}</td>
                    <td>{{$theme->default == 1 ? Lang::get('general.yes') : Lang::get('general.no') }}</td>
                    <td class="text-center">

                        <a href="{{url('modules/theme/'.$theme->id.'/show')}}" class="btn btn-sm btn-info glyphicon glyphicon-eye-open"></a>
                        <a href="{{url('modules/theme/'.$theme->id.'/edit')}}" class="btn btn-sm btn-default glyphicon glyphicon-pencil"></a>
                        <a href="{{url('modules/theme/'.$theme->id.'/delete')}}" class="btn btn-sm btn-danger glyphicon glyphicon-trash"></a>
                    </td>
                </tr>
            @endforeach
            </table>	
		</div>   
    </div>
@endsection
