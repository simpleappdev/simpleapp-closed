@extends('admin.template.master')
@section('admin_content')



    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{Lang::get('general.page_not_found')}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection