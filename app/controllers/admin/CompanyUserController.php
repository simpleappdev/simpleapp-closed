<?php namespace admin;

class CompanyUserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /admin/companies/{company}/users
	 *
	 * @param  int/model  $company
	 * @return Response
	 */
	public function index($company)
	{
		//
		return $company->users;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/companies/{company}/users/create
	 *
	 * @param  int/model  $company
	 * @return Response
	 */
	public function create($company)
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/companies/{company}/users
	 *
	 * @param  int/model  $company
	 * @return Response
	 */
	public function store($company)
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /admin/companies/{company}/users/{id}
	 *
	 * @param  int/model  $company
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/companies/{company}/users/{id}/edit
	 *
	 * @param  int/model  $company
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/companies/{company}/users/{id}
	 *
	 * @param  int/model  $company
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/companies/{company}/users/{id}
	 *
	 * @param  int/model  $company
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}