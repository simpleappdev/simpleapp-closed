{{--
@Requires:
    fields        => something to be wrapped in a div
    legendname    => string

    imagePreview
--}}

<fieldset>
    <legend>{{{ $legendname }}}</legend>
    {{ Helpers\ThemeBuilder::wrap('div', $fields); }}
    @if ($imagePreview)
        <img src="" class="image-preview">
    @endif
</fieldset>