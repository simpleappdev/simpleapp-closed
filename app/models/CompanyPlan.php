<?php

class CompanyPlan extends \Eloquent {
	protected $fillable = ['company_id'];

	protected $table = 'payments_companies';

	public function getDates(){
		return ['registred_date'];
	}

	public function currentPlanInfo(){
		return $this->belongsTo('Plan', 'current_plan_id');
	}

	public function switchingPlanInfo(){
		return $this->belongsTo('Plan', 'switching_plan_id');
	}

	public function showPlan(){
		return $this->belongsTo('Plan');
	}
}