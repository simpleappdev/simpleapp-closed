@extends('admin.template.master')
@section('admin_content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Users</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">

            <a href="{{URL::route('admin.users.create') }}" class="btn btn-default">{{Lang::get('users.UserCreateButton')}}</a>


        <div class="panel panel-default">
            <div class="panel-heading">
                Context Classes
            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td>{{Lang::get('users.UserId')}}</td>
                            <td>{{Lang::get('users.UserName')}}</td>
                            <td>{{Lang::get('users.UserConfirmed')}}</td>
                            <td>{{Lang::get('users.UserRegisterDate')}}</td>
                            <td colspan="3">{{Lang::get('users.UserActions')}}</td>
                        </tr>



                        @foreach($users as $user)
                         <tr class="{{$user->confirmed == 0 ? 'danger' : 'success'}}">
                            <td>{{$user->id}}</td>
                            <td>{{$user->surname}}</td>
                            <td>{{$user->confirmed == 0 ? Lang::get('general.no') : Lang::get('general.yes')}}</td>
                            <td>{{$user->created_at->format('d-m-Y')}}</td>
                            <td><a href="{{URL::route('admin.users.show', $user->id)}}" class="btn btn-sm btn-default fa fa-info"></a></td>
                            <td><a href="{{URL::route('admin.users.edit', $user->id)}}" class="btn btn-sm btn-default fa fa-pencil"></a></td>

                            {{ Form::open(['route' => ['admin.users.destroy', $user->id]])}}
                            {{ Form::hidden('_method', 'DELETE') }}
                                <td><button class="btn btn-sm btn-info fa fa-trash"></button></td>
                            {{ Form::close() }}
                        @endforeach
                        </tr>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                {{$users->links()}}
            </div>
        </div>
        <!-- /.panel -->
    </div>
</div>
@endsection