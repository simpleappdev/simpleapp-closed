@extends('master')
@section('content')
<style>
    * {
        margin: 0px;

    }
    #theme-content {
        margin: 0 auto;
    }

    #screener {
        position: fixed;
        left: 0px;
        top: 0px;
        background: rgba(116,130,140, 0.8);
        z-index: 5000;
        width: 100%;
        height: 100%;
    }

    #screener > #loading {
        color: white;
        font-weight: bold;
        font-size: 24px;
        position: relative;
        top: 300px;
        text-align: center;
    }

    .color-picker-addon {
        background: rgb(255, 255, 255);
    }

    .theme-block, .theme-save {
        display: none;
    }

    .sidebar-item {
        cursor: pointer;
    }
</style>
<div id="screener">
    <div id="loading">
        <p>Please wait...</p>
        <p>The themebuilder is loading...</p>

    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <p class="lead">Options</p>
            <div class="list-group">
                <span class="list-group-item active sidebar-item" id="theme-settings">Settings</span>
                <span class="list-group-item sidebar-item" id="theme-block">Theme</span>
                <span class="list-group-item sidebar-item" id="theme-save">Save</span>
            </div>
        </div>

        <div class="col-md-5 theme-save">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Save your theme
                    </h4>
                </div>
                <div class="panel-body">
                    <div class=" col-md-12 col-lg-12 ">
                        {{ Form::open(['role' => 'form']) }}

                        <p>You can save the theme and apply them to all pages, or use them later for seperated pages.</p>

                        <hr/>
                        {{ Form::submit('Save') }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 theme-settings">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Settings
                    </h4>
                </div>
                <div class="panel-body">
                    <div class=" col-md-12 col-lg-12 ">
                        <table class="table table-user-information">
                            <tbody>
                            {{Form::open()}}
                            <tr>
                                <td>Titel</td>
                                <td>{{Form::text('title', $theme->title)}}</td>
                            </tr>
                            <tr>
                                <td>Default</td>
                                <td>{{Form::select('default', array('' => 'Make your choice', 1 => 'Yes', 0 => 'No'), $theme->default)}}</td>
                            </tr>
                            </tbody>
                        </table>
                        @include('_partials.errors')
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5" id="preview-column">
            <div class="panel panel-info" id="preview-panel">
                <div class="panel-heading" id="style-header-preview">
                    <h4 class="panel-title">
                        Create your own design!
                    </h4>
                </div>
                <div class="panel-body" id="style-paragraph-preview">
                    <div id="theme">
                        <div id="theme-content">
                            <div class="article-article">
                                <h1 class="article-header">Dit is een nieuwskop 1</h1>
                                <p class="article-paragraph">Dit zal alle tekst bevatten 1</p>
                            </div>
                            <div class="article-article">
                                <h1 class="article-header">Dit is een nieuwskop 2</h1>
                                <p class="article-paragraph">Dit zal alle tekst bevatten 2</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-5 theme-block">



            <div class="panel-group" id="accordion">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id="article-header">
                                Header
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body" id="article-header-input" class="article-header">
                            {{ $themeForm->createTheme('h1', $values['h1']) }}
                        </div>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" id="article-paragraph">
                                Paragraaf
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group" id="article-paragraph-input" class="article-paragraph">
                                {{ $themeForm->createTheme('paragraph', $values['paragraph']) }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#articleCollapse" id="article-article">
                                Article
                            </a>
                        </h4>
                    </div>
                    <div id="articleCollapse" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group" id="article-article-input" class="article-article">
                                {{ $themeForm->createTheme('article', $values['article']) }}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>
{{HTML::script(url('javascript/scripts/ThemeBuilder.js'))}}
<script>
    $(document).ready(function(){
        $('.sidebar-item').click(function(){
            $('.sidebar-item').removeClass('active');
            $(this).addClass('active');

            var id = $(this).attr('id');
            $('.theme-block, .theme-settings, .theme-save').hide();
            $('.'+id).show();
        });
    });
</script>
@endsection
