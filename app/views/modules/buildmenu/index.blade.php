@extends('master')
@section('content')
<script>
    $(function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    });
</script>

<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead">Modules in ontwikkeling</p>
            <div class="list-group">
                <a href="#" class="list-group-item">Printable</a>
                <a href="#" class="list-group-item">Cupcake Wrappers</a>
                <a href="#" class="list-group-item">Authentic Dragon Bones</a>
            </div>
        </div>

        <div class="col-md-9">

            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Module pages</h3>
                        </div>
                        <div class="panel-body">
                            @if($company->activeModulePages != null)
                            <table id="mytable" class="table table-bordred table-striped">
                                <thead>
                                <th><input type="checkbox" id="checkall" /></th>
                                <th>Pagename</th>
                                </thead>
                                <tbody>
                                @foreach($company->activeModulePages as $page)
                                <tr>
                                    <td><input type="checkbox" class="checkthis" /></td>
                                    <td>{{$page->titel}}</td>
                                </tr>

                                @endforeach
                                </tbody>
                            </table>
                            @else
                            We could not find any active Pages
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Module pages</h3>
                        </div>
                        <div class="panel-body">
                            <table id="mytable" class="table table-bordred table-striped">
                                <thead>
                                    <th><input type="checkbox" id="checkall" /></th>
                                    <th>Modules</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox" class="checkthis" /></td>
                                        <td>Soundcloud</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" class="checkthis" /></td>
                                        <td>Image Gallery</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<script>
    $(document).ready(function(){
        $("#mytable #checkall").click(function () {
            if ($("#mytable #checkall").is(':checked')) {
                $("#mytable input[type=checkbox]").each(function () {
                    $(this).prop("checked", true);
                });

            } else {
                $("#mytable input[type=checkbox]").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });
    });
</script>
@endsection
