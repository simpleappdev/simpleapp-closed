<?php namespace Services\Validators;

use Input;


abstract class Validator {

	protected $attributes;

	public $errors;
	
	function __construct($attributes = null, $validation_type) {
		$this->attributes = $attributes ?: \Input::all();
	}

	public function passes(){
		$validation = \Validator::make($this->attributes, static::$rules);

		if($validation->passes()) return true;

		$this->errors = $validation->messages();

		return false;
	}

	public static function validate($attributes = null, $validation_type){
		$attributes = $attributes ?: Input::all();

		$validation = Validator::make($attributes, Validator::validation_rule($validation_type));

		if($validation->passes()) return true;

		return $validation->messages();
	}

	public static function validation_rule($validation_type){
        $array['company_validation'] = array(
            'company_name' => 'required',
//            'slug' => 'required|unique:companies,slug|min:3|alpha_dash',
            'use_activation_code' => '',
            'activation_code' => 'required_if:use_activation_code,1|exists:company_create_keys,key,used,0',
        );

        return $array[$validation_type];
	}
}