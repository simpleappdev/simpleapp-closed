@extends('admin.template.master')
@section('admin_content')

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{Lang::get('roles.RoleHeader');}}</h1>


	    <div class="col-sm-12 col-lg-12 col-md-12">

			<a href="{{URL::route('admin.roles.create') }}" class="btn btn-default">{{Lang::get('roles.RoleCreateButton')}}</a>

			<div class="table-responsive">
				<table class="table table-striped table-bordered" border="1" width="960">
				<tr>
					<td>{{Lang::get('roles.RoleId');}}</td>
					<td>{{Lang::get('roles.RoleName');}}</td>
					<td>{{Lang::get('roles.RoleDescription');}}</td>
					<td>{{Lang::get('roles.RoleCreated');}}</td>
					<td>{{Lang::get('roles.RoleUpdated');}}</td>
					<td colspan="3">{{Lang::get('roles.Actions');}}</td>
				</tr>
			@foreach ($roles as $role)
				<tr>
				    <td>{{ $role->id }}</td>
				    <td>{{ $role->name }}</td>
					<td>{{ $role->description }}</td>
				    <td>{{ $role->created_at }}</td>
				    <td>{{ $role->updated_at }}</td>
					<td><a href="{{URL::route('admin.roles.show', $role->id)}}" class="btn btn-sm btn-default fa fa-info"></a></td>
					<td><a href="{{URL::route('admin.roles.edit', $role->id)}}" class="btn btn-sm btn-default fa fa-pencil"></a></td>

					{{ Form::open(['route' => ['admin.roles.destroy', $role->id]])}}
					{{ Form::hidden('_method', 'DELETE') }}
					<td><button class="btn btn-sm btn-info fa fa-trash"></button></td>
					{{ Form::close() }}
				</tr>
			@endforeach

			</table>
</div>
	    </div>
	</div>
</div>
@endsection