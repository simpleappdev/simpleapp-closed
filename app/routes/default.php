<?php

/**
 * Route for page not found
 */
App::missing(function($exception)
{
    $is_admin_page = Request::segment(1);

    if($is_admin_page == 'admin'){
        return Response::view('admin.special_pages.404');
    }
    return Response::view('defaults.special_pages.404');

});

Route::get('access_denied', 						'SpecialPagesController@level');

Route::get('test', 'TestController@index');

Route::get('routes', function(){
    $routes = Route::getRoutes();

    foreach($routes AS $route){
        $a = $route->getPath();
        if (strpos($a,'{any}') !== false) {
            echo '<a href="'.str_replace("{any}","1",$route->getPath()).'">'.$a.'</a> Decoded';
        } elseif(strpos($a,'{user}') !== false){
            //echo str_replace("{user}","1",$route->getPath());
            echo '<a href="'.str_replace("{user}","1",$route->getPath()).'">'.$a.'</a> Decoded';
        } else {
            echo '<a href="'.$a.'">'.$a.'</a>';
        }
        echo '<br />';
    }
});