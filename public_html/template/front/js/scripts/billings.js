$(document).ready(function(){
    $('#select_all_bills').click(function(){
        var all = $(this);
        $('input:checkbox').each(function() {
            $(this).prop("checked", all.prop("checked"));
        });
    })
}); 


 $('input[name="payed_date"]').datetimepicker({
        format: 'd-m-Y H:i',
        step:1
    });


    $(document).ready(function(){
        var url;
        var current_date =
                moment().date() + '-' +
                (moment().month() + 1) + '-' +
                moment().year() + ' ' +
                moment().hour() + ':' + moment().minute() + ':' + moment().second();
        var custom_date;

        $('.update_bill').click(function( e ){
            $('#panel-error, #payment-date-error').addClass('hide');
            e.preventDefault();
            url = $(this).data('url');
            
            if($(this).data('payment-date') == 'now'){
                custom_date = current_date;
            }

            if($(this).data('payment-date') == 'time'){
                custom_date = $('input[name="payed_date"]').val();
            }

            $('#date-message').text(custom_date);
        });

        $('.submit_change').click(function(){
            window.location = url;
        });
    });