{{ Form::open()}}
<div class="col-lg-4 whitespace-bottom">
    {{Form::label('log_id', Lang::get('logbook.search_by_log'))}}
    {{Form::text('search[id]', null, ['class' => 'form-control search-for-id'])}}
</div>
<div class="col-lg-4 whitespace-bottom">
    {{Form::label('user_id', Lang::get('logbook.search_by_user'))}}
    {{Form::text('search[user_id]', array_key_exists('user_id', $search) ? $search['user_id'] : '', ['class' => 'form-control search-lock search-for-user_id'])}}
</div>
<div class="col-lg-4 whitespace-bottom">
    {{Form::label('company_id', Lang::get('logbook.search_by_company'))}}
    {{Form::select('search[company_id]', ['' => Lang::get('general.make_your_choice')] + $companies, array_key_exists('company_id', $search) ? $search['company_id'] : '', ['class' => 'form-control search-lock search-for-company_id'])}}
</div>
<div class="col-lg-4 whitespace-bottom">
    {{Form::label('item_id', Lang::get('logbook.search_by_item'))}}
    {{Form::text('search[item_id]', array_key_exists('item_id', $search) != 0 ? $search['item_id'] : '', ['class' => 'form-control search-lock search-for-item_id'])}}
</div>
<div class="col-lg-4 whitespace-bottom">
    {{Form::label('event_name', Lang::get('logbook.search_by_event'))}}
    {{Form::select('search[event]', ['' => Lang::get('general.make_your_choice')] +  $events, array_key_exists('event', $search) != 0 ? $search['event'] : '', ['class' => 'form-control search-lock search-for-event'])}}
</div>
<div class="col-lg-4 whitespace-bottom">
    {{Form::label('action_name', Lang::get('logbook.search_by_action'))}}
    {{Form::select('search[action]', ['' => Lang::get('general.make_your_choice')] + $actions, array_key_exists('action', $search) != 0 ? $search['action'] : '', ['class' => 'form-control search-lock search-for-action'])}}
</div>
<div class="col-lg-4 whitespace-bottom">
    {{Form::label('action_name', Lang::get('logbook.search_by_system_log'))}}
    {{Form::checkbox('search[system_log]', 1, array_key_exists('system_log', $search) == 1 ? true : '', ['id' => 'search_system_log'])}}
</div>
<div class="col-lg-4 whitespace-bottom">
    {{Form::label('level', Lang::get('logbook.search_by_level'))}}
    {{Form::select('search[level]', $levels, array_key_exists('level', $search) != 0 ? $search['level'] : '5', ['class' => 'form-control search-lock search-for-event'])}}
</div>


<div class="col-lg-12 whitespace-bottom">
    {{Form::submit(Lang::get('logbook.search_button'), ['class' => 'btn btn-default'])}}
    <button class="btn btn-default" id="reset-form">{{Lang::get('general.reset')}}</button>
</div>

{{Form::close()}}


<script>
    $(document).ready(function(){
        function block_search_log_id(){
            
        }
        $('.search-for-id').keyup(function(){
            var value = $(this).val();
            if(value.length >= 1){
                $('.search-lock, #search_system_log').attr('disabled', true);
            } else {
                $('.search-lock, #search_system_log').attr('disabled', false);
            }
        });

        $('.search-lock').change(function(){
            var value = $(this).val();
            if(value.length >= 1){
                $('.search-for-id').attr('disabled', true);
            } else {
                $('.search-for-id').attr('disabled', false);

            }
        });

        $('#search_system_log').change(function(){
            if($(this).is(':checked')){
                $('.search-for-id, .search-for-user_id').attr('disabled', true);
            } else {
                $('.search-for-id, .search-for-user_id').attr('disabled', false);

            }
        });

        $('#reset-form').click(function( e ){
            e.preventDefault();
            $('.col-lg-4 input').attr('disabled', false);
            $('.col-lg-4 input').val('');
        });
    });
</script>