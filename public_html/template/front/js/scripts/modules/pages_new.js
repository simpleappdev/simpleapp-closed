$(document).ready(function(){
        $( "#sortable" ).sortable();
        var counter = 0;
        $('.container').on('click', 'button.add-content', function (e) {
            counter++;

            if(counter >= 1){
                $('.remove-content').removeClass('hide');
            }
            var $content = $(this).parents('.content-box');
            var $clone = $content.clone();
            $clone.find('input, textarea').val('');
            $content.before($clone);
            $clone.hide().fadeIn('slow');
        });

        $('.container').on('click', 'button.remove-content', function (e) {
            counter--;
            if(counter == 0){
                $('.remove-content').addClass('hide');
            }
            var $content = $(this).parents('.content-box');
            $content.remove();
        });

        $('#update-page').click(function(){
            var valid = true;
            $('#new_page input, #new_page textarea').each(function(){
                if($(this).val() == ''){
                    valid = false;
                    $('#empty-fields').removeClass('hide');
                }
            });
            $('#new_page').submit();
            if(valid == true){

                $('#empty-fields').addClass('hide');
            }
            $('#new_page').submit();
        });
    }); 
