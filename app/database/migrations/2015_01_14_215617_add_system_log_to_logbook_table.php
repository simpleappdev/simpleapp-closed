<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSystemLogToLogbookTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('logbook', function(Blueprint $table)
		{
			$table->integer('system_log');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('logbook', function(Blueprint $table)
		{
			$table->dropColumn('system_log');
		});
	}

}
