<?php

/**
 * Route group for overviewing all the modules with availability
 */
Route::group(['prefix' => 'modules'], function(){
    Route::group(['before' => 'hasCompany'], function(){
        Route::get('/overview/{company_id}', 'ModulesController@index');
        Route::post('/overview/{company_id}', 'ModulesController@update');
    });
    Route::get('{content_type}/delete/{content_id}', 'ModulesController@delete_content');
});

/**
 * Generates Routes for the Theming
 */
Route::group(['prefix' => 'modules/theme'], function(){
    Route::group(['before' => 'hasCompany'], function(){
        Route::get('{any}',                   'ThemeController@index');
        Route::get('{any}/create',                                 'ThemeController@create');
        Route::post('{any}/create',                                'ThemeController@store');
    });
    Route::group(['before' => 'hasItemAccess'], function(){
        Route::get('{any}/delete',            'ThemeController@destroy');
        Route::get('{any}/edit',                           'ThemeController@edit');
        Route::post('{any}/edit',                           'ThemeController@update');
    });
});

/**
 * Generate Controller for Soundcloud module
 */
Route::group(['prefix' => 'modules/soundcloud'], function(){
    Route::group(['before' => 'hasCompany'],function(){
        Route::get('/{any}', 'SoundcloudController@index');
        Route::get('/{any}/create', 'SoundcloudController@create');
        Route::post('/{any}/create', 'SoundcloudController@store');
    });
    Route::group(['before' => 'hasItemAccess'], function(){
        Route::get('{any}/edit', 'SoundcloudController@edit');
        Route::post('{any}/edit', 'SoundcloudController@update');
    });
});

/**
 * Generate Controller and Routes for Module Gallery
 */
Route::group(['prefix' => 'modules/gallery'], function(){
    Route::group(['before' => 'hasCompany'], function(){
        Route::get('/{any}', 'GalleryController@index');
        Route::post('/{any}', 'GalleryController@updateAutogenerate');
        Route::get('/{any}/create', 'GalleryController@create');
        Route::post('/{any}/create', 'GalleryController@store');
    });
    Route::group(['before' => 'hasItemAccess'], function(){
        Route::get('{any}/edit', 'GalleryController@edit');
        Route::post('{any}/edit', 'GalleryController@update');
    });
});

/**
 * Generate Controller and Routes for Module Pages
 */
Route::group(['prefix' => 'modules/pages'], function(){
    Route::group(['before' => 'hasCompany'], function(){
        Route::get('{any}', 'PagesController@index');
        Route::post('{any}', 'PagesController@updateAutogenerate');
        Route::get('{any}/create', 'PagesController@create');
        Route::post('{any}/create', 'PagesController@store');
    });
    Route::group(['before' => 'hasItemAccess'], function(){
        Route::get('{any}/edit', 'PagesController@edit');
        Route::post('{any}/edit', 'PagesController@update');
    });
});

/**
 * Generate Controller and Routes for Module Events
 */
Route::group(['prefix' => 'modules/events'], function(){
    Route::group(['before' => 'hasCompany'], function(){
        Route::get('{any}',                              'ScheduleController@index');
        Route::post('{any}',                           'ScheduleController@updateAutogenerate');
        Route::get('{any}/create',                          'ScheduleController@create');
        Route::post('{any}/create',                         'ScheduleController@store');
    });
    Route::group(['before' => 'hasItemAccess'], function(){
        Route::get('{any}/edit',                          'ScheduleController@edit');
        Route::post('{any}/edit',                          'ScheduleController@update');
    });

});

/**
 * Generate Controller and Routes for Module Photos
 */
Route::group(['prefix' => 'modules/gallery'], function(){
    Route::get('{album_id}/photo/',                   'PhotoController@index');
    Route::get('{album_id}/photo/add',                  'PhotoController@create');
    Route::post('{album_id}/photo/add',                  'PhotoController@store');
});

/**
 * Create app
 */
