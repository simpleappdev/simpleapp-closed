<?php

class Branch extends \Eloquent {
	protected $guarded = [];

	protected $table = 'branches';

	public function getCompanies(){
		return $this->hasMany('Company');
	}
}