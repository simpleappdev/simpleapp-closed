<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Entrust\HasRole;

class User extends ConfideUser {

    use HasRole;

	protected $fillable = ['id', 'token', 'first_name', 'surname', 'username', 'password', 'email', 'created_at', 'updated_at', 'confirmed', 'confirmation_code', 'phonenumber', 'role'];

    public static $rules = array(
        'email' => 'required|email',
        'password' => 'required|between:4,11|confirmed',
//        'password_confirmation' => 'required'
    );

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getCompanies()
	{
		return $this->belongsToMany('Company');
	}

	public function getUser(){
		return $this->hasMany('Company');
	}

    public function getRole(){
        return $this->belongsTo('Role');
    }

	public function logs($limit = null){
		$query = $this->hasMany('Logbook')->where('system_log', '=', 0)->get();

		if($limit != null){
			$query = $this->hasMany('Logbook')->where('system_log', '=', 0)->limit($limit)->get();
		}
		return $query;
	}
}