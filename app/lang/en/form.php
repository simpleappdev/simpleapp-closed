<?php

return array (
  'company_name' => 'Company name',
  'slug' => 'Slug',
  'branch' => 'Branch',
  'select' => 'Select your choice',
  'unique_key' => 'Unique key',
  'create' => 'Create',
  'signup_attention_new_company' => '<b>Attention:</b> your company needs to be approved! Do you have a unique key? You can enter it in the form to activate your company and start building your app immediatly!',
);
