<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('module_pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titel');
			$table->string('pagename');
			$table->integer('company_id');
            $table->integer('active');
            $table->integer('destroyed');
			$table->string('content');
			$table->timestamps();
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::drop('module_pages');
       
	}

}
