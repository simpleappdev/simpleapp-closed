<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{Lang::get('messages.welcome')}}</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('template/front/css/bootstrap.css') }}

    <!-- Add custom CSS here -->
    {{ HTML::style('template/front/css/default-css.css') }}

    {{ HTML::script('//code.jquery.com/jquery-1.10.2.js') }}
    {{ HTML::script('//code.jquery.com/ui/1.10.4/jquery-ui.js') }}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

    {{ HTML::script('template/front/js/plugins/colorpicker/js/colpick.js') }}
    {{ HTML::style('template/front/js/plugins/colorpicker/css/colpick.css') }}
    {{ HTML::style('template/front/css/style.css') }}

    <base path="{{Config::get('app.url').'/'}}">
    <script>
        var base_url = $('base').attr('path');
    </script>
</head>

<body>
@include('includes.modal')
@include('navigation')


@yield('content')



{{ HTML::script('template/front/js/bootstrap.js') }}
@include('defaults.footer')

</body>

</html>
