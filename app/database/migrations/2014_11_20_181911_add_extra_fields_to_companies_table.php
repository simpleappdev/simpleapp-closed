<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddExtraFieldsToCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('companies', function(Blueprint $table)
		{
			$table->string('email');
			$table->string('phone');
			$table->string('address');
			$table->string('city');
			$table->string('country');
			$table->string('zipcode');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('companies', function(Blueprint $table)
		{
			$table->dropColumn('email');
			$table->dropColumn('phone');
			$table->dropColumn('address');
			$table->dropColumn('city');
			$table->dropColumn('country');
			$table->dropColumn('zipcode');
		});
	}

}
