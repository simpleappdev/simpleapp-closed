<?php

class Plan extends \Eloquent {
	protected $fillable = ['name', 'description', 'price'];

	protected $table = 'payment_plans';

	public function companies(){
		return $this->hasMany('Company');
	}
}