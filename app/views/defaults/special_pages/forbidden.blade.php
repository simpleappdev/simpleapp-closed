@extends('master') 
@section('content')
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Modules in ontwikkeling</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">Printable</a>
                    <a href="#" class="list-group-item">Cupcake Wrappers</a>
                    <a href="#" class="list-group-item">Authentic Dragon Bones</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row">

                    <div class="col-sm-12 col-lg-12 col-md-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Something went wrong</h3>
                            </div>
                            <div class="panel-body">
                               {{$message}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->
@endsection
