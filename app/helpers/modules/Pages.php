<?php namespace Helpers\Modules;

/**
 * Helpers for module Pages
 */

use Form;

class PagesH {

    /**
     * @param $values
     * @param bool $default
     * @return mixed
     *
     * This function generates a Form Select for creating a new page. There will
     * be checked if there is a default theme. If there is, a new page will automatically
     * contain the default theme. For page editing, the select box will contain the
     * theme that is choosen.
     */
    public static function generateDefault($values, $default = false){
        $data = array();
        foreach($values as $value){
            if($value->default == 1){
                $value->name .= ' (Default)';

                if($default == false){
                    $default = $value->id;
                }
            }

            $data[$value->id] = $value->name;
        }

        return Form::select('theme', $data, $default);
    }
}