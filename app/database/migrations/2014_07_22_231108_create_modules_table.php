<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('type');
            $table->integer('active');
            $table->string('link');
            $table->integer('hard_coded');
			$table->timestamps();
		});

        Schema::create('company_module', function($table)
        {
            $table->increments('id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies'); // assumes a users table
            $table->foreign('module_id')->references('id')->on('modules');
            $table->timestamps();
        });


	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('company_module', function(Blueprint $table) {
            $table->dropForeign('company_module_company_id_foreign');
            $table->dropForeign('company_module_module_id_foreign');
        });

        Schema::drop('company_module');
        Schema::drop('modules');
	}

}
