<?php namespace Helpers;

use \PagesM;
use File;

class RenderPages {


    /**
     * CSS
     * DATA
     *
     */
    public static function render($page_id, $theme_builder_preview = false){
        $page = PagesM::find(22);
        $theme_file = $page->getTheme->file;

        if($page->getTheme->id == null){
            $theme = Theme::where('default', '=', '1')->where('company_id', '=', 1)->first();
            $theme_file = $theme->file;
        } else

        $data = unserialize($page->content);

        $html = '';

        foreach($data as $value){
            $html .= '<div class="article">';
            $html .= '<div class="h1">'.$value['title'].'</div>';
            $html .= '<div class="paragraph">'.$value['content'].'</div>';
            $html .= '</div>';
        }


        if($theme_builder_preview == false){
            $css = File::get('simpleapp/companies/triple/website/themes/'.$theme_file);
            $save_data = HtmlCreator::html($html, $css);
            File::put('simpleapp/companies/triple/app/pages/pagename.html', $save_data);
        } else {
            return $html;
        }



    }
}