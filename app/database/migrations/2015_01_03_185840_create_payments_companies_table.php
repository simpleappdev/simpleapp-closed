<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments_companies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id');
			$table->integer('current_plan_id');
			$table->integer('switching_plan_id');
			$table->datetime('requested_switch_date');
			$table->datetime('registred_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments_companies');
	}

}
