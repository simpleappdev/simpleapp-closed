@extends('master')
@section('content')
<div class="container">
    <div class="col-lg-3">
        @include('includes.modules_menu')
    </div>
    <div class="col-lg-9">
        @include('includes.success')
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{Lang::get('modules.overview')}}</h3>
            </div>


            <table class="table table-striped">
                <tr>
                    <td>{{Lang::get('general.name')}}</td>
                    <td>{{Lang::get('general.available')}}</td>
                    <td>{{Lang::get('general.in_use')}}</td>
                    <td>{{Lang::get('general.manage')}}</td>
                </tr>
                {{Form::open()}}
                @foreach($modules as $module)
                <tr>
                    <td>{{$module->title}}</td>
                    <td>Available</td>

                    @if($module->hard_coded == 0)
                        @if(in_array($module->id, $active_modules))
                            <td>{{Form::checkbox('active_modules[]', $module->id, true)}}</td>
                        @else
                            <td>{{Form::checkbox('active_modules[]', $module->id, '')}}</td>
                        @endif
                    @else

                        <td>{{Form::checkbox('', '', true, ['disabled' => 'disabled'])}}</td>

                    @endif

                    <td>
                        @if(in_array($module->id, $active_modules))
                            <a href="{{url('modules/'.strtolower($module->title).'/'.$company->id)}}">{{Lang::get('general.manage')}}</a>
                        @endif
                    </td>
                </tr>
                @endforeach

            </table>
            <div class="panel-footer">
                {{Form::submit(Lang::get('general.save'))}}
                {{Form::close()}}
            </div>

        </div>
    </div>
</div>
<!-- /.container -->

@endsection
