@extends('master')
@section('content')
@extends('master') 
@section('content')
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Modules in ontwikkeling</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">Printable</a>
                    <a href="#" class="list-group-item">Cupcake Wrappers</a>
                    <a href="#" class="list-group-item">Authentic Dragon Bones</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row">

                    <div class="col-sm-12 col-lg-12 col-md-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Beschikbare modules</h3>
                            </div>
                            <div class="panel-body">
                                @if($modules)
                                {{Form::open(array('method' => 'delete'))}}
                                    <table border="1">
                                        <tr>
                                            <td>Verwijderen</td>
                                            <td>Module naam</td>
                                        </tr>
                                   @foreach($modules AS $module)
                                        <tr>
                                            <td>{{Form::checkbox('delete_module[]', $module->id)}}</td>
                                            <td>{{$module->name}}</td>
                                        </tr>
                                        
                                   @endforeach
                                   </table>
                                   {{Form::submit('Modules verwijderen')}}
                                @endif
                            </div>
                            @include ('_partials.errors')
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-12 col-md-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Voeg een nieuwe module toe</h3>
                            </div>
                            <div class="panel-body">
                               {{Form::open()}}
                               {{Form::label('name'),Form::input('text', 'name')}}
                               {{Form::submit('Toevoegen')}}
                            </div>
                            @include ('_partials.errors')
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Company 2013 - Template by <a href="http://maxoffsky.com/">Maks</a>
                    </p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

@endsection

@endsection