<?php

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = ['id', 'name', 'description'];

    public function getUsers()
    {
        return $this->hasMany('User');
    }

    public function getRoles()
    {
        return $this->belongsToMany('Permission');
    }
}