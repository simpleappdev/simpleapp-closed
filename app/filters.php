<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    //
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});


Route::filter('hasCompany', function() {

    $company_id = Request::segment(3);
    if (!Auth::user()->getCompanies->contains($company_id)){
        $data = array(
            'message' => 'You don\'t have the right permissions to view this page.',
        );
        return View::make('defaults.special_pages.forbidden')->with($data);
    }

});

Route::filter('hasItemAccess', function() {
    $module_type = Request::segment(2);
    $item_id = Request::segment(3);

    switch($module_type){
        case 'pages':
            $item = Page::withTrashed()->find($item_id);
            break;

        case 'theme':
            $item = Theme::withTrashed()->find($item_id);
            break;

        case 'soundcloud':
            $item = SoundcloudM::withTrashed()->find($item_id);
            break;
    }

    $companies = Auth::user()->getCompanies;
    $access = false;
    if($item != null){
        foreach($companies as $company){
            if($company->id == $item->company_id){
                $access = true;
            }
        }
    }

    if($access == false){
        $data = array(
            'message' => Lang::get('general.no_item_access'),
        );
        return View::make('defaults.special_pages.forbidden')->with($data);
    }

});




Entrust::routeNeedsRole('admin/', 'admin', Redirect::to('access_denied'));

View::composer(['modules.pages.index', 'modules.soundcloud.index'], function($view){

    $view->with('show_delete_dialog', true);
});

