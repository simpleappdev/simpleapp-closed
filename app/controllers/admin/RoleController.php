<?php

namespace admin;

use Role;
use View;
use Input;
use Redirect;
use Permission;
use Helpers\Helpers;

class RoleController extends \BaseController {

	public function index()
	{
		$roles = Role::all();
		return View::make('admin.role.index', ['roles' => $roles]);
	}

	public function create()
	{
        $permissions = Permission::all();
		return View::make('admin.role.create', ['permissions' => $permissions]);
	}

	public function store()
	{


        $role = new Role;
        $role->name = Input::get('role_name');
        $role->description = Input::get('role_description');
        $role->save();


        $checkedPermissions = Input::get('permission');
        if(is_array($checkedPermissions))
        {
            $role->perms()->sync(array_keys($checkedPermissions));
        }


        return Redirect::to('admin/roles');
	}

	public function show($id)
	{
		$role = Role::find($id);

		return View::make('admin.role.show', ['role' => $role]);
	}

	public function edit($id)
	{
        $permissions = Permission::all();

        $role = Role::with('perms')->find($id);
        $data = ['role' => $role, 'permissions' => $permissions, 'assigned_perms' => Helpers::objectToArray($role->perms) ];

        return View::make('admin.role.edit', $data);
	}

	public function update($id)
	{
		$role = Role::find($id);
		$role->fill(Input::all());


        $checkedPermissions = (is_array(Input::get('permission')) ? Input::get('permission') : []);

        $role->perms()->sync(array_keys($checkedPermissions));
		$role->save();

		return View::make('admin.role.show', ['role' => $role]);

	}

	public function destroy($id)
	{
		Role::find($id)->delete();

		return Redirect::to('admin/roles');
	}

}