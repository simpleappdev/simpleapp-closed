<?php

return array (
  'accepted' => 'Het veld :attribute moet worden geaccepteerd',
  'active_url' => 'Het veld :attribute is geen geldige URL',
  'after' => 'Het veld :attribute moet een datum na :date zijn',
  'alpha' => 'Het veld :attribute mag alleen letters bevatten',
  'alpha_dash' => 'Het veld :attribute mag alleen uit letters, cijfers en streepjes bestaan',
  'alpha_num' => 'Het veld :attribute mag alleen letters en cijfers bevatten',
  'alpha_spaces' => 'Het veld :attribute mag alleen uit letters en spaties bestaan',
  'array' => 'Het veld :attribute moet een array zijn',
  'before' => 'Het veld :attribute moet een datum voor :date zijn',
  'between' => 
  array (
    'array' => 'Het veld :attribute moet tussen :min en :max zijn',
    'file' => 'Het veld :attribute mag maximaal :min en maximaal :max kb zijn',
    'numeric' => 'Het veld :attribute moet tussen :min en :max zijn',
    'string' => 'Het veld :attribute moet minimaal :min en maximaal :max tekens bevatten',
  ),
  'confirmed' => 'Het veld :attribute moet worden geaccepteerd!',
  'custom' => 
  array (
    'activation_code' => 
    array (
      'required_if' => 'Geef een activatie code op',
    ),
    'slug' => 
    array (
      'required' => 'Sleutel is een verplicht veld',
    ),
  ),
  'date' => 'Het veld :attribute is geen geldige datum',
  'date_format' => 'Het veld :attribute voldoet niet aan het formaat :format',
  'different' => 'Het veld :attribute en :other mogen niet gelijk zijn',
  'digits' => 'Het veld :attribute moet :digits cijfers bevatten',
  'digits_between' => 'Het veld :attribute moet tussen de :min en :max cijfers zijn',
  'email' => 'Het veld :attribute formaat is ongeldig',
  'exists' => 'Het veld :attribute is ongeldig',
  'image' => 'Het veld :attribute moet een afbeelding zijn',
  'in' => 'Het veld :attribute is ongeldig',
  'integer' => 'Het veld :attribute moet uit cijfers bestaan',
  'ip' => 'Het veld :attribute moet een geldig IP adres zijn',
  'max' => 
  array (
    'array' => 'Het veld :attribute mag niet meer dan :max tekens bevatten',
  ),
);
