<?php

class AlbumController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /album
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$albums = PhotoAlbum::where('company_id', '=', $id)->get();
		$data = array('albums' => $albums);

		//dd($albums);

		return View::make('modules.gallery.album.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /album/create
	 *
	 * @return Response
	 */
	public function create($id)
	{
		//return $id;
		
		return View::make('modules.gallery.album.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /album
	 *
	 * @return Response
	 */
	public function store($id)
	{
		$album = new PhotoAlbum;
		$album->company_id = $id;
		$album->title = Input::get('title');
		$album->description = Input::get('description');
		$album->added_by = Auth::user()->id;
		$album->save();
		return $this->index($id);
	}

	/**
	 * Display the specified resource.
	 * GET /album/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /album/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /album/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /album/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function view($id)
	{
		$album = PhotoAlbum::find($id);

		$data['album'] = $album;

		return View::make('modules.gallery.album.show-album', $data);
		
	}

}