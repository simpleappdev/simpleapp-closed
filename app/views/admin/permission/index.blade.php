@extends('admin.template.master')
@section('admin_content')

    <div class="row">

        <div class="col-lg-12">
            <h1 class="page-header">{{Lang::get('permissions.PermissionHeader');}}</h1>
        </div>

        <div class="col-sm-12 col-lg-12 col-md-12">

            <a href="{{URL::route('admin.permissions.create') }}" class="btn btn-default">{{Lang::get('permissions.PermissionCreateButton');}}</a>

            <div class="table-responsive">
                <table class="table table-bordered" border="1">
                    <tr>
                        <td>{{Lang::get('permissions.PermissionId');}}</td>
                        <td>{{Lang::get('permissions.PermissionName');}}</td>
                        <td>{{Lang::get('permissions.PermissionDescription');}}</td>
                        <td>{{Lang::get('permissions.PermissionCreatedAt');}}</td>
                        <td>{{Lang::get('permissions.PermissionUpdatedAt');}}</td>
                        <td colspan="3">{{Lang::get('permissions.PermissionAction');}}</td>
                    </tr>

                    @foreach ($permissions as $permission)
                        <tr>
                            <td>{{ $permission->id }}</td>
                            <td>{{ $permission->name }}</td>
                            <td>{{ $permission->display_name }}</td>
                            <td>{{ $permission->created_at }}</td>
                            <td>{{ $permission->updated_at }}</td>
                            <td><a href="{{URL::route('admin.permissions.show', $permission->id)}}"
                                   class="btn btn-sm btn-default fa fa-info"></a></td>
                            <td><a href="{{URL::route('admin.permissions.edit', $permission->id)}}"
                                   class="btn btn-sm btn-default fa fa-pencil"></a></td>

                            {{ Form::open(['route' => ['admin.permissions.destroy', $permission->id]])}}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <td>
                                <button class="btn btn-sm btn-info fa fa-trash"></button>
                            </td>
                            {{ Form::close() }}
                        </tr>

                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection