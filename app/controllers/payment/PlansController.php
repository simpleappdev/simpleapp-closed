<?php namespace payment;

use View, Redirect, Input, Company, User, Plan, Auth, Lang, CompanyPlan;

class PlansController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /paymentplans
	 *
	 * @return Response
	 */
	public function index()
	{
		$plans = Plan::all();
		return View::make('website.payments.plans.index', ['plans' => $plans]);
	}

	public function subscribe($plan_id){

		$plan = Plan::find($plan_id);
		$user = Auth::user();
		$company_list = '';

		foreach($user->getCompanies as $company){
			if($company->currentPlan === null){
				CompanyPlan::create(['company_id' => $company->id]);
			}

			$company_list .= View::make('website.payments.plans.company_list', [
				'company' => $company,
				'current_plan' => $company->currentPlan,
				'plan_id' => $plan->id,
			]);
		}


		return View::make('website.payments.plans.select_company', ['plan' => $plan, 'company_list' => $company_list]);
	}

	public function subscribe_confirm($plan_id, $company_id){
		$company = Company::find($company_id);
		if($company->currentPlan->current_plan_id == 0){
			$company->currentPlan->current_plan_id = $plan_id;
		} else {
			$company->currentPlan->switching_plan_id = $plan_id;
		}

		if($company->currentPlan->current_plan_id == $plan_id){
			return Redirect::to('plans/'.$plan_id)->with('message', Lang::get('general.action_not_allowed'));
		}

		$company->currentPlan->save();

		return Redirect::back()->with('message', Lang::get('general.plan_saved', ['company' => $company->company_name]));

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /paymentplans/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /paymentplans
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /paymentplans/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /paymentplans/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /paymentplans/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /paymentplans/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}