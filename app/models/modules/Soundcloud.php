<?php

class Soundcloud extends \Eloquent {
    protected $softDelete = true;

    protected $fillable = ['name', 'artist', 'url', 'embed_code', 'company_id', 'active'];

    protected $table = 'soundcloud';

//    public function setCompanyIdAttribute($company_id){
//        if($company_id){
//            echo 'Company ID'. $company_id;
//
//        $this->attributes['company_id'] = $company_id;
//        }
//    }
}