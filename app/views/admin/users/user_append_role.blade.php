Bewerk gebruiker: {{$user->email}} <br /><br /><br />

<table border="1" width="300">
	<tr>
		<th>Rollen</th>
	</tr>
@foreach ($user->roles AS $userRole)
	<tr>
		<td>{{$userRole->name}}</td>
	</tr>
@endforeach
</table>
<br /><br /><br />

{{Form::open()}}

<table border="1" width="500">
	<tr>
		<td>Checkbox</td>
		<td>Role name</td>
	</tr>
	{{Form::select('role', $roles) }}

</table>

{{Form::submit('Opslaan')}}
{{Form::close()}}