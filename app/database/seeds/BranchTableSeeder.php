<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class BranchTableSeeder extends Seeder {

	public function run()
	{
		$branches = array(
			array(
				'description' => 'horeca',
				'language' => 'nl',
			),
			array(
				'description' => 'detailhandel',
				'language' => 'nl',
			),
			array(
				'description' => 'entertainment',
				'language' => 'nl',
			),
		);

		foreach($branches as $branch){
			Branch::create($branch);
			$this->command->info('Branch created: ' . $branch['description']);
		}
	}

}