<?php namespace Helpers\Modules;

/**
 * Class to render
 */

class EventsH {

    public static function databaseFilter($value){

        $value = preg_replace('/[^A-Za-z0-9\-]/', '', $value); // Removes special chars.

        return strtolower($value);
    }
}