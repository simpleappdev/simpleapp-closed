<?php

class Schedule extends \Eloquent {

    protected $fillable = ['id','name','slug', 'description', 'company_id','start', 'end', 'location','created_at','edited_at', 'active'];

    public function getDates(){
        return array('start', 'end', 'created_at');
    }

    public function setStartAttribute($start) {
        if ($start) {
            $this->attributes['start'] = date('Y-m-d H:i:s',(strtotime($start)));
        } else {
            $this->attributes['start'] = '';
        }
    }

    public function setEndAttribute($end) {
        if ($end) {
            $this->attributes['end'] = date('Y-m-d H:i:s',(strtotime($end)));
        } else {
            $this->attributes['end'] = '';
        }
    }

    public function setCompanyIdAttribute($id) {
            $this->attributes['company_id'] = $id;
    }

	public function showCompany(){
		$this->belongsTo('Company');
	}
}