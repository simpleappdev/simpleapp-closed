<p class="lead">Admin menu</p>
<div class="list-group">
    @foreach($AdminMenuLinks as $link => $title)
        <a href="{{ url('admin/'. $link) }}" class="list-group-item @if($AdminMenuActiveLink==$link) active @endif ">{{$title}}</a>
    @endforeach
</div>
