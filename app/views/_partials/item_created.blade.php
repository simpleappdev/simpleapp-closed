@if(Session::get('message'))
    <style>
        .page-header{
            text-align: center;
        }
    </style>

    <header class="page-header">
        <h1 class="page-title">{{Session::get('message')}}</h1>
    </header>
@endif