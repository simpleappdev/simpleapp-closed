<?php

namespace admin;

use View;
use Branch;
use Input;
use Redirect;

class BranchController extends \BaseController {

	private $branch;

	public function __construct(Branch $branch){
		$this->branch = $branch;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$branches = Branch::all();

		return View::make('admin.branch.index', ['branches' => $branches]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.branch.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$this->branch->create(Input::all());

		return Redirect::route('admin.branches.index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($branch)
	{
		return View::make('admin.branch.show', ['branch' => $branch]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($branch)
	{
		return View::make('admin.branch.edit', ['branch' => $branch]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($branch)
	{
		$branch->update(Input::all());

		return View::make('admin.branch.show', ['branch' => $branch]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($branch)
	{
		$branch->delete();

		return Redirect::route('admin.branches.index');

	}


}
