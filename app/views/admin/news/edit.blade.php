@extends('admin.template.master')
@section('admin_content')

<style>
.indent {
    margin-left: 360px;
}

img {
    float: left;
}
</style>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">News</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12" >

 <div class="panel panel-info">
            <div class="panel-heading">
            {{ Form::model($news, array('method' => 'PUT', 'route' => array('admin.news.update', $news->id), 'files' => 'true' ))}}
                <h3 class="panel-title">{{Lang::get('news.create_news_item_header')}}</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class=" col-md-6 col-lg-6 ">
                        <table class="table table-user-information">
                            <tr>
                                <td>{{Lang::get('news.title')}}</td>
                                <td>{{Form::text('title', null, ['id' => 'titleInput'])}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.published')}}</td>
                                <td>{{Form::select('published', ['default' => Lang::get('general.make_your_choice'), 1 => Lang::get('general.yes'), 0 => Lang::get('general.no')])}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.select_language')}}</td>
                                <td>
                                    {{Form::select('language', ['default' => Lang::get('general.make_your_choice'), 'english' => Lang::get('general.english'), 'dutch' => Lang::get('general.dutch')])}}
                                </td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('news.audience')}}</td>
                                <td>{{Form::select('audience', ['default' => Lang::get('general.make_your_choice'), 'members' => Lang::get('news.audience_members'), 'everyone' => Lang::get('news.audience_everyone')])}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('news.image')}}</td>
                                <td>{{Form::file('image')}}</td>
                            </tr>
                            <tr>
                                <td class="no_image" title="If you want to remove the image of this item, select this box. If you just want to change e.g. the title and leave the image as is, uncheck this box">{{Lang::get('news.no_image')}}</td>
                                    {{ Form::hidden('no_image', false) }}
                                <td>{{Form::checkbox('no_image', true)}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('news.body')}}</td>
                                <td>{{Form::textarea('body', null, ['id' => 'bodyInput'])}}</td>
                            </tr>
                        </table>


                    </div>

                    <div class="col-md-6 col-lg-6" >
                    <h1 class="page-header">Example</h1>

                       <img src="{{(Config::get('default.news_resized_image_location')) . $news->image}}"/>

                       <h1 class="indent" id="title">{{$news->title}}</h1>

                       <p class="indent" id="body">{{$news->body}}</p>
                    </div>
                </div>

            </div>



            </div>
            <div class="panel-footer">
                <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                <span class="pull-right">
                    {{Form::submit(Lang::get('general.save'), ['class' => 'btn btn-success'])}}
                    {{Form::close()}}
                </span>
            </div>
         </div>
       </div>
 </div>
@endsection

<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script>
$(function () {
  var $title = $('#title');
  var $titleInput = $('#titleInput');

  var $body = $("#body");
  var $bodyInput = $("#bodyInput");

  $titleInput.on('keydown', function () {
    setTimeout(function () {
      $title.html($titleInput.val());
    }, 0);
  });

  $bodyInput.on('keydown', function () {
      setTimeout(function () {
        $body.html($bodyInput.val());
      }, 0);
    });
})
</script>