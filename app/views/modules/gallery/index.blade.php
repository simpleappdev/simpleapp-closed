@extends('master')
@section('content')
<style>
    .page-header{
        text-align: center;
    }
</style>
    <div class="container">
        <div class="col-lg-3">
            @include('includes.manage_module')
            @include('includes.modules_menu')
        </div>

		
		<div class="col-md-9">
            <header class="page-header">
                <h1 class="page-title">{{ucfirst($module_name)}} items</h1>
                @if(count($company->getGalleries()) >= 2)
                    <small> <i class="fa fa-clock-o"></i> Last Created on:
                        <time>
                            {{$company->getGalleries()->last()->created_at->format('D-M-Y H:i')}}
                        </time>
                    </small>
                @endif
            </header>
			<div class="panel panel-default">
			    <div class="panel-heading">
			        <h3 class="panel-title">Events from: {{$company->company_name}}</h3>
			    </div>

			    
					<table class="table table-striped">
						<tr>
							<td>Title</td>
                            <td>Active</td>
                            <td>Actions</td>
						</tr>
						@if($company->getGalleries)
							@foreach($company->getGalleries AS $item)
								<tr>
									<td>{{$item->title}}</td>
                                    <td>{{$item->active == 1 ? 'Yes' : 'No' }}</td>
                                    <td>
                                        <a href="{{url('modules/gallery/'.$item->id.'/edit')}}">Edit</a>
                                        or
                                        <a href="{{url('modules/gallery/'.$item->id.'/destroy')}}">destroy</a>
                                    </td>
								</tr>
							@endforeach
						@endif
					</table>
                <div class="panel-footer">
                    Regenerate
                    {{Form::open()}}
                    {{Form::checkbox('autogenerate', $module_id, $autogenerate)}}
                    {{Form::submit('Save')}}
                    {{Form::close()}}
                </div>
				
			</div>
		</div>	
    </div>
    <!-- /.container -->

@endsection
