@extends('master')
@section('content')

<style>

/*
    .fileUpload {
        position: relative;
        overflow: hidden;

    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
*/

</style>

<div class="container">
    <div class="row col-lg-3"  id="side-menu">
        <div class="col-md-12">
            <p class="lead" >Shop Name</p>
            <div class="list-group">
                <a href="#" class="list-group-item">Printable</a>
                <a href="#" class="list-group-item">Cupcake Wrappers</a>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            {{Form::open(array('files' => true))}}

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Default settings</h3>
                </div>
                <div class="panel-body">
                    <div class="row col-md-12">
                        {{Form::text('title', '')}}
                        <input type="file" class="upload" id="file" name="image" />
                        {{Form::submit('Opslaan')}}

                    </div>
                    @include('_partials.errors')
                </div>
            </div>
        </div>
    </div>
</div>


@endsection