<?php namespace admin;

use Auth;
use Illuminate\Support\Facades\Log;
use Redirect;
use View;
use Logbook;
use Input;
use Lang;
use User;
use Company;
use App;

class LogbookController extends \BaseController
{
    protected $user;
    protected $logbook;
    protected $company;

    public function __construct(Logbook $lb, User $user, Company $company){
        $this->logbook = $lb;
        $this->user = $user;
        $this->company = $company;
        $this->levels = [];
        for ($i = 0; $i <= 5; $i++){
            $this->levels[$i] = Lang::get('system_log.level', ['level_id' => $i]);
        }
    }

    public function index()
    {
        $companies = $this->company->lists('company_name');
        $search = Input::get('search' , []);
        $events = $this->logbook->groupBy('event')->lists('event', 'event');

        foreach ($events as $key => &$value) {
          $value = Lang::get($key);
        }

        $actions = $this->logbook->groupBy('action')->lists('action', 'action');
        foreach ($actions as $key => &$value) {
          $value = Lang::get($key);
        }

        $logbook = $this->logbook->newQuery();
         
        foreach ($search as $key => $value) {
            if(trim($value) != ''){
                switch($key){
                    case "user_id":
                        if(filter_var($value, FILTER_VALIDATE_EMAIL)){
                            $user = $this->user->whereEmail($value)->first();
                        } elseif (!is_numeric($value)){
                            $user = $this->user->whereUsername($value)->first();
                        }

                        if(isset($user->id)){
                            $value = $user->id;
                        }

                        $logbook->where($key, '=', $value); 

                    break;

                    default:
                    $logbook->where($key, '=', $value);

                    break;
                }
            }
        }
         
        $logs = $logbook->paginate(20);

        return View::make('admin.logbook.index', [
            'logs' => $logs,
            'companies' => $companies, 
            'events' => $events,
            'actions' => $actions,
            'levels' => $this->levels,
            'search' => $search,
            ]);
    }

    public function show_list($type, $id){
        switch($type){
            case 'user':
                $lb = $this->logbook->whereUserId($id);
            break;

            case 'company':
                $lb = $this->logbook->whereCompanyId($id);
            break;

            case 'level':
                $lb = $this->logbook->whereLevel($id);
            break;

            default:
                return App::abort(404);
            break;
        }

        $logs = $lb->paginate(20);

        return View::make('admin.logbook.show_list', ['logs' => $logs]);
    }
}