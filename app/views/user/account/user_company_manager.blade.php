@extends('master')
@section('content')
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">{{Lang::get('general.my_account')}}</p>
                <div class="list-group">
                    <a href="{{url('account/')}}" class="list-group-item">{{Lang::get('general.my_settings')}}</a>
                    <a href="{{url('account/companies/')}}" class="list-group-item">{{Lang::get('general.my_activities')}}</a>
                </div>
                @include('includes.account_menu')
            </div>

            @include('company.company.sidebar.overview')

            <div class="col-md-9">

              
            @if($user)
                <div class="row">
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{Lang::get('general.welcome_back')}} {{Auth::user()->username}}</h3>
                            </div>
                            <div class="panel-body">
                                <p>{{Lang::get('general.info_about_dashboard')}}</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    {{Lang::get('general.last_info_about_modules')}}
                                </h3>
                            </div>
                            <div class="panel-body">
                                Alle updates over modules
                            </div>
                            <div class="panel-footer">
                                <a href="{{url('#')}}">{{Lang::get('general.show_more')}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    {{Lang::get('general.invoices_information')}}
                                </h3>
                            </div>
                            <div class="panel-body">
                                Info over rekeningen
                            </div>
                            <div class="panel-footer">
                                <a href="{{url('#')}}">{{Lang::get('general.show_more')}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    {{Lang::get('general.logbook_info')}}
                                </h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <tr>
                                        <td>{{Lang::get('general.events')}}</td>
                                        <td>{{Lang::get('general.company')}}</td>
                                        <td>{{Lang::get('logbook.actions')}}</td>
                                    </tr>
                                    @foreach(Auth::user()->logs(10) as $log)
                                    <tr>
                                        @if(isset($log->company->company_name))
                                        <td>{{Lang::get($log->event)}}</td>
                                            <td>{{$log->company->company_name}}</td>
                                            <td>{{Lang::get($log->action)}}</td>

                                        @else
                                            <td>{{Lang::get($log->event)}}</td>
                                        <td>&nbsp;</td>
                                            <td>{{Lang::get($log->action)}}</td>

                                        @endif
                                    </tr>
                                    @endforeach
                                </table>

                            </div>
                            <div class="panel-footer">
                                <a href="{{url('#')}}">{{Lang::get('general.show_more')}}</a>
                            </div>
                        </div>
                    </div>

                </div>
            @endif

            </div>

        </div>

    </div>
    <!-- /.container -->

@endsection
