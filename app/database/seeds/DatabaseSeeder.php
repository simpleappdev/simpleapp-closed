<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$tables = DB::select('SHOW TABLES');

		foreach($tables as $table){
			$name = $table->Tables_in_simpleapp;
			if($name != 'migrations'){
				$increment = "ALTER TABLE $name AUTO_INCREMENT = 0";
				$foreign_check = "SET foreign_key_checks = 0";

				DB::unprepared($increment);
				$this->command->info('Increments: Table ' . $name . ' increments set to 0.');

				DB::statement($foreign_check);
				$this->command->info('Foreign key: Table ' . $name . ' foreign_key check removed');

				DB::table($name)->truncate();
				$this->command->info('Truncated: Table ' . $name . ' truncated.');
				$this->call('PermissionsTableSeeder');
	}
		}


		$this->call('RolesTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('CompanyTableSeeder');
		$this->call('ModulesTableSeeder');
		$this->call('PaymentsTableSeeder');
		$this->call('BranchTableSeeder');
		$this->call('CombineTableSeeder');
		$this->command->info('Tables seeded!');
	}

}