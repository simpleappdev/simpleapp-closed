<?php

namespace admin;

use Helpers\LogbookH;
use View;

class IndexController extends \BaseController {

	 
	public function index()
	{
		return View::make('admin.dashboard.index');
	}
}