@extends('admin.template.master')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">News</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12" >


        <div class="panel panel-info">
            <div class="panel-heading">
                {{ Form::open(['route' => 'admin.news.store', 'role' => 'form', 'files' => true]) }}
                <h3 class="panel-title">{{Lang::get('news.create_news_item_header')}}</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class=" col-md-12 col-lg-12 ">
                        <table class="table table-user-information">
                            <tr>
                                <td>{{Lang::get('news.title')}}</td>
                                <td>{{Form::text('title')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.published')}}</td>
                                <td>{{Form::select('published', ['default' => Lang::get('general.make_your_choice'), 1 => Lang::get('general.yes'), 0 => Lang::get('general.no')])}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('general.select_language')}}</td>
                                <td>{{Form::select('language', ['default' => Lang::get('general.make_your_choice'), 'english' => Lang::get('general.english'), 'dutch' => Lang::get('general.dutch')])}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('news.audience')}}</td>
                                <td>{{Form::select('audience', ['default' => Lang::get('general.make_your_choice'), 'members' => Lang::get('news.audience_members'), 'everyone' => Lang::get('news.audience_everyone')])}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('news.image')}}</td>
                                <td>{{Form::file('image')}}</td>
                            </tr>
                            <tr>
                                <td>{{Lang::get('news.body')}}</td>
                                <td>{{Form::textarea('body')}}</td>
                            </tr>
                        </table>


                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                <span class="pull-right">
                    {{Form::submit(Lang::get('general.save'), ['class' => 'btn btn-success'])}}
                    {{Form::close()}}
                </span>
            </div>

        </div>
    </div>
</div>

@endsection