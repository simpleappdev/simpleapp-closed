@extends('admin.template.master')
@section('admin_content')

    <style>
        .whitespace-bottom {
            margin-bottom: 20px;
        }
    </style>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{Lang::get('logbook.header_logs')}}</h1>
            </div>

            <div class="col-lg-12">
                <h2>{{Lang::get('logbook.header_search')}}</h2>

                @include('admin.logbook.search_form')

            </div>

            <div class="col-lg-12">
                {{--<h2>Logs</h2>--}}

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{Lang::get('logbook.tableheader_hashtag')}}</th>
                            <th>{{Lang::get('logbook.tableheader_user')}}</th>
                            <th>{{Lang::get('logbook.tableheader_company')}}</th>
                            <th>{{Lang::get('logbook.tableheader_item')}}</th>
                            <th>{{Lang::get('logbook.tableheader_event')}}</th>
                            <th>{{Lang::get('logbook.tableheader_action')}}</th>
                            <th>{{Lang::get('logbook.tableheader_time')}}</th>
                        </tr>
                        </thead>

                        <tbody class="table-striped">
                        @foreach($logs as $log)
                            <tr class="@if($log->system_log == 1)  {{'alert alert-danger'}} @endif">
                                <td>{{$log->id}}</td>
                                <td>
                                @if($log->user_id != 0)
                                    <a href="{{url('admin/logbook/user/'.$log->user_id)}}">{{$log->user->email}}</a>
                                @endif
                                </td>
                                <td>
                                    @if($log->company_id != 0)
                                        <a href="{{url('admin/logbook/company/'.$log->company_id)}}">{{$log->company->company_name}}</a>
                                    @endif
                                </td>
                                <td>{{$log->item_id}}</td>
                                <td>{{Lang::get($log->event)}}</td>
                                <td>{{Lang::get($log->action)}}</td>
                                <td>{{$log->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{$logs->links()}}
            </div>
        </div>

@endsection