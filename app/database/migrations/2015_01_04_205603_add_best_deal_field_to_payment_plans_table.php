<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddBestDealFieldToPaymentPlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payment_plans', function(Blueprint $table)
		{
			$table->integer('recommended');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payment_plans', function(Blueprint $table)
		{
			$table->dropColumn('recommended');
		});
	}

}
