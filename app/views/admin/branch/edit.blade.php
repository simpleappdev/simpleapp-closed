@extends('admin.template.master')
@section('admin_content')

    <div class="row">

        <div class="col-lg-12">
            <h1 class="page-header">{{Lang::get('branch.branchHeader')}}</h1>
        </div>

        <div class="col-sm-4 col-lg-4 col-md-4">

            <h2>{{Lang::get('branch.branchEdit')}}</h2>

            {{ Form::model($branch, array('method' => 'PUT', 'route' => array('admin.branches.update', $branch->id), ))}}

            {{ Form::label('branch_description', Lang::get('branch.branchDescription'))}}
            {{ Form::text('description', null, ['class' => 'form-control'])}}

            {{ Form::Label('branch_language', Lang::get('branch.branchLanguage'))}}
            {{ Form::text('language', null, ['class' => 'form-control'])}}

            {{ Form::submit(Lang::get('branch.branchEdit'), ['class' => 'btn btn-default'])}}

            {{ Form::close()}}

        </div>
    </div>
    </div>
@endsection