@extends('master')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3">
            @include('admin.defaults.sidemenu')
        </div>

	    <div class="col-sm-4 col-lg-4 col-md-4">
	    	{{$message}}
	    </div>
	</div>
</div>
@endsection