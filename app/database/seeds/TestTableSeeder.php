<?php

class TestTableSeeder extends Seeder {

	public function run()
	{
		for($i = 0; $i <= 200; $i++){
			$amount = rand(10, 50) / 3;

			$bill = new Bill;
			$bill->plan_id = 2;
			$bill->company_id = 1;
			$bill->price = $amount;
			$bill->save();
			echo $i;
		}
	}

}