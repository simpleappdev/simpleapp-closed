<?php

return array (
  'company_name' => 'Bedrijfsnaam',
  'slug' => 'Sleutel',
  'branch' => 'Branche',
  'select' => 'Maak een keuze',
  'unique_key' => 'Unieke code',
  'create' => 'Aanmaken',
  'signup_attention_new_company' => '<b>Let op:</b> uw bedrijf moet eerst goed gekeurd worden! Heeft u een unieke sleutel? Deze kunt u toevoegen in het formulier, uw bedrijf word direct goed gekeurd.',
);
