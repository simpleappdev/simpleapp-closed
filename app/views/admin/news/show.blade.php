@extends('admin.template.master')
@section('admin_content')

<style>
.indent {
    margin-left: 360px;
}

img {
    float: left;
}
</style>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{Lang::get('news.header');}}</h1>


            <div class="col-sm-12 col-lg-12 col-md-12">

                <a href="{{URL::route('admin.news.index') }}" class="btn btn-default">{{Lang::get('news.back_button')}}</a>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
              <div class="col-md-6 col-lg-6">
                <p><strong>{{Lang::get('news.title')}}</strong></p>
                <p>{{$news->title}}</p>

                <p><strong>{{Lang::get('news.language')}}</strong></p>
                <p>{{$news->language}}</p>

                <p><strong>{{Lang::get('news.published')}}</strong></p>
                <p>{{$news->published}}</p>

                <p><strong>{{Lang::get('news.audience')}}</strong></p>
                <p>{{$news->audience}}</p>


                <p><strong>{{Lang::get('news.body')}}</strong></p>
                <p>{{$news->body}}</p>


              </div>
                <div class="col-md-6 col-lg-6" >
                   <h1 class="page-header">Example</h1>

                      <img src="{{(Config::get('default.news_resized_image_location')) . $news->image}}"/>

                      <h1 class="indent" id="title">{{$news->title}}</h1>

                      <p class="indent" id="body">{{$news->body}}</p>
                   </div>
              </div>
        </div>
    </div>
@endsection