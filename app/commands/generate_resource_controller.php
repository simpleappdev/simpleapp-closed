<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Helpers\CompanyH;

class generate_resource_controller extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'generate_resource';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Renders resource controller';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        /**
         * Prefix
         */
        $get_model = $this->argument('model');
        $model = new $get_model;
        $model_fields = $model->getFillable();
        $table = '';
        foreach($model_fields as $field){
            $table .= '
            <tr>
                <td>{{Lang::get(\'general.artisan_generator_'.$field.'\')}}</td>
                <td>{{Form::text(\''.$field.'\')}}</td>
            </tr>';
        }

        /**
         * Generate Controller
         */
        $controller = file_get_contents(app_path().'/templates/resources/controller.txt');
        $controller = str_replace("{{NAMESPACE}}", 'namespace '.$this->argument('namespace'),$controller);
        $controller = str_replace("{{CONSTRUCTOR}}",$this->argument('constructor'),$controller);
        $controller = str_replace("{{VIEW}}",$this->argument('view'),$controller);
        $controller = str_replace("{{VAR_PLURAL}}", $this->argument('var_plural'),$controller);
        $controller = str_replace("{{MODEL}}",$this->argument('model'),$controller);
        $controller = str_replace("{{CONTROLLER}}",$this->argument('controller_name'),$controller);
        file_put_contents(app_path().'/controllers/'.$this->argument('namespace').'/'.$this->argument('controller_name').'.php', $controller);

        /**
         * Generate Edit View
         */
        $edit_view = file_get_contents(app_path().'/templates/resources/views/admin/edit.txt');
        $edit_view = str_replace("{{NAMESPACE}}",$this->argument('namespace'),$edit_view);
        $edit_view = str_replace("{{CONSTRUCTOR}}",$this->argument('constructor'),$edit_view);
        $edit_view = str_replace("{{VIEW}}",$this->argument('view'),$edit_view);
        $edit_view = str_replace("{{VAR_PLURAL}}", '$'.$this->argument('var_plural'),$edit_view);
        $edit_view = str_replace("{{MODEL}}",$this->argument('model'),$edit_view);
        $edit_view = str_replace("{{CONTROLLER}}",$this->argument('controller_name'),$edit_view);
        $edit_view = str_replace("{{TABLE_FIELDS}}", $table, $edit_view);
        file_put_contents(app_path().'/views/admin/'.$this->argument('view').'/edit.blade.php', $edit_view);

        /**
         * Generate Create View
         */
        $create_view = file_get_contents(app_path().'/templates/resources/views/admin/create.txt');
        $create_view = str_replace("{{NAMESPACE}}",$this->argument('namespace'),$create_view);
        $create_view = str_replace("{{CONSTRUCTOR}}",$this->argument('constructor'),$create_view);
        $create_view = str_replace("{{VIEW}}",$this->argument('view'),$create_view);
        $create_view = str_replace("{{VAR_PLURAL}}", '$'.$this->argument('var_plural'),$create_view);
        $create_view = str_replace("{{MODEL}}",$this->argument('model'),$create_view);
        $create_view = str_replace("{{CONTROLLER}}",$this->argument('controller_name'),$create_view);
        $create_view = str_replace("{{TABLE_FIELDS}}", $table, $create_view);
        file_put_contents(app_path().'/views/admin/'.$this->argument('view').'/create.blade.php', $create_view);

        /**
         * Generate Index View
         */
        $index_view = file_get_contents(app_path().'/templates/resources/views/admin/index.txt');
        $index_view = str_replace("{{NAMESPACE}}",$this->argument('namespace'),$index_view);
        $index_view = str_replace("{{CONSTRUCTOR}}",$this->argument('constructor'),$index_view);
        $index_view = str_replace("{{VIEW}}",$this->argument('view'),$index_view);
        $index_view = str_replace("{{VAR_PLURAL}}", $this->argument('var_plural'),$index_view);
        $index_view = str_replace("{{MODEL}}",$this->argument('model'),$index_view);
        $index_view = str_replace("{{CONTROLLER}}",$this->argument('controller_name'),$index_view);
        $index_view = str_replace("{{TABLE_FIELDS}}", $table, $index_view);
        file_put_contents(app_path().'/views/admin/'.$this->argument('view').'/index.blade.php', $index_view);


        $table = '';
        foreach($model_fields as $field){
            $table .= '
            <tr>
                <td>{{Lang::get(\'general.artisan_generator_'.$field.'\')}}</td>
                <td>{{$'.$this->argument('constructor').'->'.$field.'}}</td>
            </tr>';
        }
        /**
         * Generate Show View
         */
        $show_view = file_get_contents(app_path().'/templates/resources/views/admin/create.txt');
        $show_view = str_replace("{{NAMESPACE}}",$this->argument('namespace'),$show_view);
        $show_view = str_replace("{{CONSTRUCTOR}}",$this->argument('constructor'),$show_view);
        $show_view = str_replace("{{VIEW}}",$this->argument('view'),$show_view);
        $show_view = str_replace("{{VAR_PLURAL}}", '$'.$this->argument('var_plural'),$show_view);
        $show_view = str_replace("{{MODEL}}",$this->argument('model'),$show_view);
        $show_view = str_replace("{{CONTROLLER}}",$this->argument('controller_name'),$show_view);
        $show_view = str_replace("{{TABLE_FIELDS}}", $table, $show_view);
        file_put_contents(app_path().'/views/admin/'.$this->argument('view').'/show.blade.php', $show_view);




        //var_dump($model);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
            array('namespace', InputArgument::REQUIRED, 'Set the namespace'),
            array('constructor', InputArgument::REQUIRED, 'Set the constructor'),
            array('view', InputArgument::REQUIRED, 'Set the view'),
            array('var_plural', InputArgument::REQUIRED, 'Set the var_plural'),
            array('model', InputArgument::REQUIRED, 'Set the model'),
            array('controller_name', InputArgument::REQUIRED, 'Set the controller name'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
            array('namespace', null, InputOption::VALUE_OPTIONAL, 'Set the namespace', null),
            array('constructor', null, InputOption::VALUE_OPTIONAL, 'Set the constructor', null),
            array('view', null, InputOption::VALUE_OPTIONAL, 'Set the view', null),
            array('var_plural', null, InputOption::VALUE_OPTIONAL, 'Set the plural variable', null),
            array('controller_name', null, InputOption::VALUE_OPTIONAL, 'Set the controller name', null),
		);
	}

}
