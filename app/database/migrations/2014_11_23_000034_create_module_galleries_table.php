<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModuleGalleriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('module_galleries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('company_id');
			$table->text('description');
			$table->integer('active');
			$table->integer('destroyed');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('module_galleries');
	}

}
