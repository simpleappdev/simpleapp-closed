@extends('master')
@section('content')
<style>
    .page-title {
        text-align: center;
    }

    #ajax-response {
        text-align: center;
        padding-bottom: 15px;
        border-bottom: 1px solid #e6e6e6;
        display: none;
    }

    .page-header {
        text-align: center;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('includes.manage_module')
            @include('includes.modules_menu')

        </div>

        <div class="col-md-9">
            <div class="row">
                <header class="page-header">
                    <h1 class="page-title">{{ucfirst($module_name)}}</h1>
                    @if(count($company->getSoundcloud) >= 2)
                    <small> <i class="fa fa-clock-o"></i> Last Created on:
                        <time>
                            {{$company->getSoundcloud->last()->created_at->format('D-M-Y H:i')}}
                        </time>
                    </small>
                    @endif
                </header>
                <h1 id="ajax-response"></h1>

                <table class="table table-striped table-bordered table-hover table-condensed">
                    <tr class="info">
                        <th class="col-md-5">{{Lang::get('modules/general.artist')}}</th>
                        <th class="col-md-4">{{Lang::get('modules/general.name')}}</th>
                        <th class="col-md-1">{{Lang::get('modules/general.active')}}</th>
                        <th class="col-md-2">{{Lang::get('modules/general.action')}}</th>
                    </tr>
                    @foreach($company->getSoundcloud AS $item)
                    <tr>
                        <td class="item-name">{{ $item->artist }}</td>
                        <td class="item-name">{{$item->name}}</td>
                        <td>{{$item->active == 1 ? Lang::get('general.yes') : Lang::get('general.no') }}</td>
                        <td class="text-center">
                            <a href="{{url('modules/soundcloud/'.$item->id.'/show')}}" class="btn btn-sm btn-info glyphicon glyphicon-eye-open"></a>
                            <a href="{{url('modules/soundcloud/'.$item->id.'/edit')}}" class="btn btn-sm btn-default glyphicon glyphicon-pencil"></a>
                            <button class="btn btn-sm btn-danger glyphicon glyphicon-trash" data-item-type="{{$module_name}}" data-item-id="{{$item->id}}" data-toggle="modal" data-target="#show_delete_dialog"></button>
                        </td>
                    </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
</div>

@endsection


