<?php namespace Helpers;

use Helpers\PrivateFile;
use Company;
use File;

/**
 * Class ThemeCreator
 * @package Helpers
 *
 * NOTICE
 *
 * The ThemeBuilder Class is extremly sensitive! Make sure that when a field is added and not
 * related for styling, to add this field in the controller to the Input::except() or otherwise
 * the ThemeBuilder will crash!
 *
 */
class ThemeCreator
{

	public static function createCss($value)
	{

		$css = '';
		foreach($value AS $class => $data){

			$css .= '.'.$class.' {';

			$css .= 'color: rgb('.$data['font-color'].');';
			$css .= 'margin: '.
						$data['margin'][0] . 'px '.
						$data['margin'][1] . 'px '.
						$data['margin'][2] . 'px '.
						$data['margin'][3] . 'px; '.PHP_EOL;
			$css .= 'padding: '.
						$data['padding'][0] . 'px '.
						$data['padding'][1] . 'px '.
						$data['padding'][2] . 'px '.
						$data['padding'][3] . 'px;'.PHP_EOL;

			$css .= 'background: rgb('.$data['font-color'].')';

			if($data['background-image'] == true){
				$css .= ' url('.$data['background-image'].') '. $data['repeat-image'] . ' '.$data['x'].' '. $data['y'];
			}

			$css .= '; '.PHP_EOL;
			$css .= 'border-radius: '.
						$data['border-radius'][0] . 'px '.
						$data['border-radius'][1] . 'px '.
						$data['border-radius'][2] . 'px '.
						$data['border-radius'][3] . 'px;'.PHP_EOL;

			$css .= '}'.PHP_EOL.PHP_EOL;
		}


		return $css;
	}

    public function setCssProperties(){

    }

	public function createFullPage($data){

		$html = '';
        print_r($data);
        foreach($data AS $key => $value){

        }

	//	$html .= 'background-color: '.$value[''];


	}
}