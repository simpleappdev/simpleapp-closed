@if(isset($company->id))
<?php $company_id = $company->id ?>
@endif



<p class="lead">Options</p>
<div class="list-group">
    <a href="{{url('modules/overview/'.$company_id)}}" class="list-group-item">Manage Modules</a>
    <a href="{{url('theme/'.$company_id)}}" class="list-group-item">Manage Themes</a>
    <a href="#" class="list-group-item">Manage Other</a>
</div>
<p class="lead">My Modules</p>
<div class="list-group">
    @foreach($company->modules as $module)
        <a href="{{url('modules/'.strtolower($module->title).'/'.$company_id)}}" class="list-group-item">{{ucfirst($module->title)}}</a>
    @endforeach
</div>