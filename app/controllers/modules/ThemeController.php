<?php namespace modules;

use Company;
use View;
use Redirect;
use Input;
use Theme;
use \Helpers\ThemeBuilder;
use \Helpers\ThemeCreator;
use \Helpers\Helpers;
use File;

class ThemeController extends \BaseController {

    public $data = array();

    public function __construct(){
        $this->data['module_name'] = 'theme';
        $this->module_id = 0;
        $this->data['module_id'] = $this->module_id;
    }

	/**
	 * Display a listing of the resource.
	 * GET /theme
	 *
	 * @return Response
	 */
	public function index($id)
	{
        $this->data['company'] = Company::find($id);

        return View::make('modules.theme.index', $this->data);

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /theme/create
	 *
	 * @return Response
	 */
	public function create($id)
	{

		// think themebuilder is going to get changed into a static helper or a facade
		//	it will get used in the partials
		$theme = new ThemeBuilder(
			'header',
            $id,
			array(url('simpleapp/companies/test-company/images/pattern.png'))
		);

		// $data = array();
		//$data['images'] = array(url('simpleapp/companies/test-company/images/image.jpg'));
		// $data['theme'] = $theme;

		return View::make('modules.theme.create', compact('theme'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /theme
	 *
	 * @return Response
	 */
	public function store($id)
	{

		$company = Company::find($id);
		$theme = new Theme();

        $this->checkDefault(Input::get('default'), $id);

        $theme_builder_input = Input::except(['_token', 'default', 'name']);
        $theme['file'] = Helpers::randomString(20, true).'.css';
        $theme['name'] = Input::get('name');
        $theme['default'] = Input::get('default');
		$theme['company_id'] = $company->id;
        $theme['theme'] = serialize($theme_builder_input);
        $theme->save();

        $css = ThemeCreator::createCss($theme_builder_input);

        File::put('simpleapp/companies/triple/website/themes/'.$theme->file, $css);

        return Redirect::to('modules/theme/'.$theme->company_id.'/')->with(['message' => 'Theme updated!']);


	}

	/**
	 * Display the specified resource.
	 * GET /theme/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['company'] = Company::find($id);

		return View::make('modules.theme.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /theme/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$theme = Theme::find($id);
		$field_data = unserialize($theme->theme);

        $themeForm = new ThemeBuilder();
        //return;
		$data = array(
			'images' => array(url('simpleapp/companies/test-company/images/image.jpg')),
			'themeForm' => $themeForm,
			'values' => $field_data,
            'theme' => $theme,

			);

		return View::make('modules.theme.edit', $data);


	}

	/**
	 * Update the specified resource in storage.
	 * PUT /theme/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $theme = Theme::find($id);
        $this->checkDefault($theme->company_id);
        $input = Input::except('_token', 'name', 'default');
        $theme->name = Input::get('name');
        $theme->default = Input::get('default');
        $theme->theme = serialize($input);

        $theme->save();


        $css = ThemeCreator::createCss($input);

        File::put('simpleapp/companies/triple/website/themes/'.$theme->file, $css);

        return Redirect::to('modules/theme/'.$theme->company_id.'/')->with(['message' => 'Theme updated!']);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /theme/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    private function checkDefault($company_id){
        /**
         * 2 = Set as default and append to all pages immediatly
         * 1 = Set as default and DONT append to all pages
         * 0 = Dont set as default
         */

        //Search all themes where default = 1
        $themes_default = Theme::where('company_id', '=', $company_id)
            ->where('default', '=', 1)
            ->update(array('default' => 0));

    }
}