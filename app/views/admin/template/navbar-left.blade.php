<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">

        <ul class="nav" id="side-menu">



            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> {{Lang::get('menu.companies')}}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('admin/companies')}}">{{Lang::get('menu.show_companies')}}</a>
                    </li>
                    <li>
                        <a href="{{url('admin/companies/create')}}">{{Lang::get('menu.create_company')}}</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> {{Lang::get('menu.users')}}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('admin/users')}}">{{Lang::get('menu.show_users')}}</a>
                    </li>
                    <li>
                        <a href="{{url('admin/users/create')}}">{{Lang::get('menu.create_user')}}</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> {{Lang::get('menu.news')}}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('admin/news')}}">{{Lang::get('menu.show_news')}}</a>
                    </li>
                    <li>
                        <a href="{{url('admin/news/create')}}">{{Lang::get('menu.create_news')}}</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="{{url('admin/logbook')}}"><i class="fa fa-table fa-fw"></i> {{Lang::get('menu.logbook')}}</a>
            </li>
            <li>
                <a href="{{url('admin/news')}}"><i class="fa fa-table fa-fw"></i> {{Lang::get('menu.news')}}</a>
            </li>
            <li>
                <a href="{{url('admin/invoices')}}"><i class="fa fa-edit fa-fw"></i> {{Lang::get('menu.invoices')}}</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> {{Lang::get('menu.settings')}}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('admin/roles')}}">{{Lang::get('menu.roles')}}</a>
                    </li>
                    <li>
                        <a href="{{url('admin/permissions')}}">{{Lang::get('menu.permissions')}}</a>
                    </li>


                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i> {{Lang::get('menu.payment_plans')}}<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">{{Lang::get('menu.show_plans')}}</a>
                    </li>
                    <li>
                        <a href="#">{{Lang::get('menu.create_plan')}}</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="blank.html">Blank Page</a>
                    </li>
                    <li>
                        <a href="login.html">Login Page</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>