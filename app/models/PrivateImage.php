<?php

class PrivateImage extends \Eloquent {
	protected $fillable = [];

    protected $table = 'company_private_images';

    public function company(){
        return $this->belongsTo('Company');
    }
}