<?php

return array (
  'menu_item_news' => 'News',
  'menu_item_logbook' => 'Logbook',
  'PermissionId' => 'Permission ID',
  'PermissionName' => 'Permission name',
  'PermissionCreated' => 'Permission Created',
  'PermissionUpdated' => 'Permission Updated',
  'PermissionAction' => 'Action',
  'RoleId' => 'Role ID',
  'RoleName' => 'Role name',
  'RoleCreated' => 'Role Created',
  'RoleUpdated' => 'Role Updated',
  'RoleAction' => 'Action',
  'menu_item_roles' => 'Roles',
  'menu_item_permissions' => 'Permissions',
  'menu_item_users' => 'Users',
  'menu_item_companies' => 'Companies',
  'menu_item_branches' => 'Branches',
  'payment_plans' => 'Payment Plans',
);
