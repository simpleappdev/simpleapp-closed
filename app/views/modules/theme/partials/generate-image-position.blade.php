{{--
@Requires:
    name      => string
    position  => string
    value     => string

    type      => string | needed | $this->type
@Fix:
    move $options out of template
--}}
<div class="form-group">
    <label for="{{{ $name }}}-image-position-{{{ $position }}}" class="theme-builder-label">
        {{{ $label }}}
    </label>
    <!-- <div class="theme-builder-row"> -->
    <select name="{{ $name }}[{{$position}}]" id="" class="{{{ $position }}}-alignment input-sm form-control">
        <option value="">Make your choice</option>
        @foreach ($options as $option)
            <option value="{{{ $option }}}" {{ $option == $value ? 'selected="selected"' : '' }}>
                {{{ ucfirst($option) }}}
            </option>
        @endforeach
    </select>
    <!-- </div> -->
</div>
