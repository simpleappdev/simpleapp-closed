<?php namespace modules;

use Helpers\Helpers;
use Company;
use View;
use Redirect;
use Module;
use Input;
use PagesM;
use Lang;
use SoundcloudM;

class ModulesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /modules
	 *
	 * @return Response
	 */
	public function index($id)
	{
        $modules = Module::all();
        $company = Company::find($id);

        $data['active_modules'] = Helpers::renderArray($company->modules);
        $data['company'] = $company;
        $data['modules'] = $modules;

		return View::make('modules.overview.index', $data);
	}

	public function update($id)
	{
        $company = Company::find($id);

        if(is_array(Input::get('active_modules'))){
            $active = Input::get('active_modules');
            //Module pages is number 3
            $active[] = 3;
            $company->modules()->sync($active);
        } else {
            $company->modules()->sync([]);
        }

        return Redirect::back()->with('modules_save_message', Lang::get('general.modules_saved'));
	}

    public function delete_content($content_type, $id){

        switch($content_type){
            case "pages":
                $item = PagesM::find($id);
                break;

            case "soundcloud":
                $item = SoundcloudM::find($id);
                break;

            default:

                break;
        }

        /**
         * If $item is not found or deleted already:
         */

        if($item == false){
            return Lang::get('modules/general.could_not_delete');
        }

        $item->delete();
        return Lang::get('modules/general.item_deleted');
    }

}