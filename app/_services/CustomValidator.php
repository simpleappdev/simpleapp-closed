<?php namespace Services;

use Input;
use Redirect;
use Session;


class CustomValidator {

    public static function validate($input, $type){

        $rules = static::rules($type);

        $validator = \Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            Session::flash('validation_errors', $validator->messages());
            return false;
        }
    }

    public static function rules($validation_type){
        $array['company_validation'] = array(
            'company_name' => 'required',
            'use_activation_code' => '',
            'activation_code' => 'required_if:use_activation_code,1|exists:company_create_keys,key,used,0',
        );

        $array['user_signup'] = array(
            'surname' => 'required|min:3',
            'email' => 'required|unique:users',
            'password' => 'required|min:8|max:32|confirmed',
            'password_confirmation' => 'required|min:8|max:32',
        );

        $array['user_profile_edit'] = array(
            'website' => 'url',
//            'email' => 'required|unique:users',
            'phonenumber' => 'required|digits:10'
        );

        $array['change_password'] = array(
            'password' => 'required|between:8,20|alpha_num|confirmed',
            'current_password' => 'required|current_password'
        );

        $array['new_user'] = array(
            'first_name' => 'required',
            'surname' => 'required',
            'username' => 'required|between:3,30|alpha_num',
            'email' => 'required',
        );

        $array['new_news'] = array(
            'title' => 'required|min:3',
            'published' => 'not_in:default',
            'language' => 'not_in:default',
            'audience' => 'not_in:default',
            'image' => 'mimes:jpeg,bmp,jpg,gif,png',
            'body' => 'required|min:10'
        );

        return $array[$validation_type];
    }
}