<?php

/**
 * This file contains all routes for company related issues. Add
 * a new company, edit private files, etc. etc.
 */

Route::group([],function(){
    Route::get('account/companies',                     'CompanyController@index');
    Route::get('company/edit/{any}', 					'CompanyController@show');
    Route::post('company/edit/{any}', 					'CompanyController@edit');
    Route::get('signup/company', 						'CompanyController@create');
    Route::post('signup/company', 						'CompanyController@store');
});

Route::group([
    'before' => '',
    'prefix' => 'company'
], function () {
    Route::get('images/{any}', 'PrivateImageController@index');
    Route::get('images/{any}/new', 'PrivateImageController@create');
    Route::post('images/{any}/new', 'PrivateImageController@store');
    Route::get('images/{any}/edit', 'PrivateImageController@update');
    Route::post('images/{any}/edit', 'PrivateImageController@update');
    Route::post('images/{any}/delete', 'PrivateImageController@destroy');
});



Route::group(['namespace' => 'payment'], function(){
    Route::get('payment/bills/{company_id}', 'BillsController@index');
    Route::get('payment/bills/{company_id}/history', 'BillsController@payment_history');
    Route::get('payment/bills/{bill_id}/download', 'BillsController@download');
});
