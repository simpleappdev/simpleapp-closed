@extends('master')
@section('content')
<style>
    .page-header{
        text-align: center;
    }
</style>
    <div class="container">
        <div class="col-lg-3">
            @include('includes.manage_module')
            @include('includes.modules_menu')
        </div>

		
		<div class="col-md-9">
            @include('includes.success')
            <header class="page-header">
                <h1 class="page-title">{{ucfirst($module_name)}} items</h1>
                @if(count($company->getSchedule) >= 2)
                    <small> <i class="fa fa-clock-o"></i> {{Lang::get('general.last_created_item')}}
                        <time>
                            {{$company->getSchedule->last()->created_at->format('D-M-Y H:i')}}
                        </time>
                    </small>
                @endif
            </header>

            <table class="table table-striped table-bordered table-hover table-condensed">
                <tr class="info">
                    <td>{{Lang::get('general.title')}}</td>
                    <td>{{Lang::get('general.start_date')}}</td>
                    <td>{{Lang::get('general.end_date')}}</td>
                    <td>{{Lang::get('general.active')}}</td>

                    <td>{{Lang::get('general.actions')}}</td>
                </tr>
                @if($company->getSchedule)
                @foreach($company->getSchedule AS $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->start->format('d-m-Y H:i')}}</td>
                    <td>{{$item->end->format('d-m-Y H:i')}}</td>
                    <td>{{$item->active == 1 ? Lang::get('general.yes') : Lang::get('general.no') }}</td>
                    <td class="text-center">
                        <a href="{{url('modules/events/'.$item->id.'/show')}}" class="btn btn-sm btn-info glyphicon glyphicon-eye-open"></a>
                        <a href="{{url('modules/events/'.$item->id.'/edit')}}" class="btn btn-sm btn-default glyphicon glyphicon-pencil"></a>
                        <button class="btn btn-sm btn-danger glyphicon glyphicon-trash" data-item-type="{{$module_name}}" data-item-id="{{$item->id}}" data-toggle="modal" data-target="#show_delete_dialog"></button>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>
		</div>	
    </div>
    <!-- /.container -->

@endsection
