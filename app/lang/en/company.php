<?php

return array (
  'my_companies' => 'My companies',
  'company_name' => 'Company name',
  'signup_new' => 'Signup a new company',
  'create_company' => 'Create company',
  'companyCreated' => 'Created at',
  'companyUpdated' => 'Company is updated',
  'companyDeleted' => 'Deleted at',
  'companyHeader' => 'Companies',
  'companyCreateButton' => 'Create Company',
  'companyNumber' => 'ID',
  'companyName' => 'Company Name',
  'companyConfirmed' => 'Confirmed',
  'companyRegisterDate' => 'Signup Date',
  'companyActions' => 'Actions',
  'companySaveButton' => 'Save',
  'companyBackButton' => 'Return to all companies',
  'companyChooseBranch' => 'Choose branch',
  'companyChooseUser' => 'Owner',
);
