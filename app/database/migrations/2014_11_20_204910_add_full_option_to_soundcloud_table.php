<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFullOptionToSoundcloudTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('soundcloud', function(Blueprint $table)
		{
			$table->string('name');
			$table->string('url');
			$table->string('artist');
			$table->integer('active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('soundcloud', function(Blueprint $table)
		{
			$table->dropColumn('name');
			$table->dropColumn('url');
			$table->dropColumn('artist');
			$table->dropColumn('active');
		});
	}

}
