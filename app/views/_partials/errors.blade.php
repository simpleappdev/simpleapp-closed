@if ($errors->any())
    <div class="panel panel-warning">
        <div class="panel-heading">
            <h3 class="panel-title">{{Lang::get('general.validation_error')}}</h3>
        </div>
        <div class="panel-body">
            {{implode('', $errors->all('<li>:message</li>'))}}
        </div>
    </div>
@endif

@if(Session::get('updated'))
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">{{Lang::get('general.updated_title')}}</h3>
        </div>
        <div class="panel-body">
            {{Session::get('updated')}}
        </div>
    </div>
@endif