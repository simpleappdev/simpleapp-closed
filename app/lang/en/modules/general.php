<?php

return array(
    'could_not_delete'  => 'This item could not be deleted. Please refresh the page and try again.',
    'item_deleted'      => 'Deleting succesfull!',
    'modal_title'       => 'Your about to delete a item',
    'active'            => 'Active',
    'action'            => 'Action',
    'description'       => 'Description',
    'title'             => 'Title',
    'artist'            => 'Artist',
    'name'              => 'Title'
);