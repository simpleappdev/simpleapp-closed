<?php namespace modules;

use Input;
use View;
use Redirect;
use Gallery;
use Company;
use Helpers\Helpers;

class GalleryController extends \BaseController {

    public $data = array();

    public function __construct(){
        $this->module_id = 4;
        $this->data['module_id'] = $this->module_id;
        $this->data['module_name'] = 'gallery';
    }

	/**
	 * Display a listing of the resource.
	 * GET /modules/gallery
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$company = Company::find($id);

        $autoGenerate = Helpers::relationToArray($company->autogenerateModules, $this->module_id);

        $this->data['autogenerate'] = $autoGenerate;
        $this->data['company'] = $company;

        return View::make('modules.gallery.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /modules/gallery/create
	 *
	 * @return Response
	 */
	public function create($id)
	{
		$company = Company::find($id);

        $this->data['company'] = $company;

        return View::make('modules.gallery.create', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /modules/gallery
	 *
	 * @return Response
	 */
	public function store($id)
	{
		$gallery = new Gallery;

        $input = Input::all();
        $input['company_id'] = $id;

        $gallery->create($input);

        return Redirect::back()->with(['message' => 'Album created!']);
	}

	/**
	 * Display the specified resource.
	 * GET /modules/gallery/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /modules/gallery/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $gallery = Gallery::find($id);
        $company = Company::find($gallery->company_id);

        $this->data['gallery'] = $gallery;
        $this->data['company'] = $company;

        return View::make('modules.gallery.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /modules/gallery/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $gallery = Gallery::find($id);

        $gallery->update(Input::all());

        return Redirect::back()->with(['message' => 'Album edited!']);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /modules/gallery/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function updateAutogenerate($id){

        Helpers::autogenerate($this->module_id, $id);
        return Redirect::to(url('modules/'.$this->data['module_name'].'/'.$id));
    }
}