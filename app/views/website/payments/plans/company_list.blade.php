<div class="well">
    <div class="media">

        <div class="media-body">
            <h4 class="media-heading">{{$company->company_name}}</h4>
            <p class="text-right">{{Lang::get('general.company_owner').' : '. $company->owner->username}}</p>
            <p class="text-left">{{Lang::get('payment.show_switching_history')}}</p>
            <div class="clear"></div>
            <ul class="list-inline list-unstyled">
                @if($current_plan->current_plan_id != 0)
                    <li>{{Lang::get('payment.current_plan') . ': ' . ucfirst($current_plan->currentPlanInfo->name)}}</li>
                    @if($current_plan->switching_plan_id != 0)
                        <li>{{Lang::get('payment.switching_to')}}: {{$current_plan->switchingPlanInfo->name}}</li>
                    @endif

                    @if($current_plan->current_plan_id == $plan_id)
                        <li>{{Lang::get('payment.already_have_plan')}}</li>
                    @else
                        @if($current_plan->switching_plan_id == $plan_id)
                            <li>{{Lang::get('payment.switching_already')}}</li>
                        @else
                            <li>
                                <a href="{{url('plans/'.$plan_id.'/'.$company->id)}}">{{Lang::get('payment.switch_to_plan')}}</a></li>
                        @endif
                    @endif

                @else
                    <li>{{Lang::get('payment.no_current_plan')}}
                        <a href="{{url('plans/'.$plan_id.'/'.$company->id)}}">{{Lang::get('payment.request')}}</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>


