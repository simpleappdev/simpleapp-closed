<?php

use Services\CustomValidator;

class ProfileController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /profile
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();

		return View::make('website.user.profile.index', ['user' => $user]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /profile/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /profile
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /profile/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /profile/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	public function update()
	{
		if(CustomValidator::validate(Input::all(), 'user_profile_edit') === false){

			Session::flash('disabled', 'disabled');

			return Redirect::back()->withInput()->withErrors(Session::get('validation_errors'));
		}

		Helpers\LogbookH::createLog('logbook.profile', 0, 'logbook_messages.changed_profile_info', 0 );

		$user = User::find(Auth::user()->id);

		$user->update(Input::all());

		return Redirect::back()->with(['updated' => Lang::get('general.profile_updated')]);

	}

	public function change_password(){

		return View::make('website.user.profile.change_password');

	}

	public function update_password(){
		if(CustomValidator::validate(Input::all(), 'change_password') === false){

			return Redirect::back()->withErrors(Session::get('validation_errors'));
		}


		$user = User::find(Auth::id());

		$user->password = Input::get('password');
		$user->save();

		Helpers\LogbookH::createLog('logbook.profile', 0, 'logbook_messages.changed_password', 0 );


		return Redirect::back()->with(['updated' => Lang::get('general.password_updated')]);
	}
	/**
	 * Remove the specified resource from storage.
	 * DELETE /profile/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}