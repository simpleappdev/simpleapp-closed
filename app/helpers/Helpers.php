<?php namespace Helpers;

use Company;
use Input;

class Helpers {

    public static function seoUrl($string) {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }

    public static function randomString($length, $lowercase = false) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        if($lowercase == true){
            return strtolower($randomString);
        }
        return $randomString;
    }

    public static function active($value){
        if($value){
            switch($value){
                case 0:
                    return 'No';
                    break;

                case 1:
                    return 'Yes';
                    break;
            }
        }
    }

    public static function objectToArray($object){
        $new_array = [];

        if($object == true){
            foreach($object as $item){
                $new_array[] = $item->id;
            }
        } else {
            //Gooi hier een exception
            die('Ecception!');
        }
//        print_r($new_array);
//dd($new_array);
        return $new_array;
    }

    /**
     * @param $values
     * @param $module_id
     * @return bool
     *
     * Generate a array from a many-to-many relationship
     */
    public static function relationToArray($values, $module_id){
        $new_array = array();
        foreach($values as $value){
            $new_array[] = $value->id;
        }

        if(in_array($module_id, $new_array)){
            return true;
        } else {
            return false;
        }
    }

    public static function renderArray($array){
        $new_array = array();

        foreach($array as $item){
            $new_array[] = $item->id;
        }

        return $new_array;
    }
}