<?php namespace Helpers;

use Helpers\HtmlCreator;
use File;

class RenderSoundcloudH {

    public static function renderPage($data, $slug){

        $new_html = '';

        foreach($data as $item){
            $new_html .= $item->embed_code;
        }

        $html = HtmlCreator::html($new_html);

        $path = 'simpleapp/companies/'.$slug.'/app/pages/soundcloud.html';

        File::put($path, $html);


    }
}