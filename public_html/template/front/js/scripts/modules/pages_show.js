$('.confirmDelete').on('click', function(e){
        var $form=$(this).closest('form');
        e.preventDefault();
        $('#confirmDeleteModal').modal({ backdrop: 'static', keyboard: false })
            .one('click', '#delete', function (e) {
                $form.trigger('submit');
            });
    }); 
