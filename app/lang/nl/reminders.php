<?php

return array (
  'password' => 'Wachtwoord moet minimaal zes tekens bevatten',
  'sent' => 'Nieuw wachtwoord verzonden!',
  'token' => 'Het "wachtwoord vergeten" token is ongeldig',
  'user' => 'We konden geen gebruiker vinden met het opgegeven email adres',
);
