<?php namespace modules;

use Company;
use Auth;
use Redirect;
use View;
use Input;
use \Services\Validators\ScheduleV;
use Validator;
use Schedule;
use Image;
use Helpers\Helpers;
use Helpers\Modules\EventsH;
use Lang;

class ScheduleController extends \BaseController {

    public $data = array();

    public function __construct(){
        $this->module_id = 1;
        $this->data['module_id'] = $this->module_id;
        $this->data['module_name'] = 'events';
    }


	public function index($id)
	{
		$company = Company::find($id);

        $this->data['company']  = $company;

		return View::make('modules.schedule.overview', $this->data);
	}

	public function create($id)
	{
		$company = Company::find($id);

        $this->data['company'] = $company;

		return View::make('modules.schedule.create', $this->data);
	}

	public function store($id)
	{
	    $validator = Validator::make(Input::all(), ScheduleV::rules());

	    if ($validator->fails())
	    {
	        //return Redirect::back()->withInput()->withErrors($validator);
	    }

        $schedule = new Schedule;
        $input = Input::all();
        $input['company_id'] = $id;
        $input['slug'] = EventsH::databaseFilter(Input::get('name')).mt_rand(9999,99999);
        $input['description'] = serialize($this->createSpecialArray(Input::get('title'), Input::get('content')));
        $input['image_name'] = Input::get('name').Input::get('start');
        $image = Input::file('image');

        $schedule->create($input);

        //Image::make($image->getRealPath())->resize(460,460,true,false)->save('simpleapp/companies/triple/events/images/test.jpg');
        //Image::make($image->getRealPath())->save('images/photo_albums/original/'.$image_name);

        return Redirect::to('modules/events/'.$id)->with('modules_save_message', Lang::get('general.event_item_saved'));

	}

	/**
	 * Display the specified resource.
	 * GET /schedule/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /schedule/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$event = Schedule::find($id);
        $company = Company::find($event->company_id);

        $this->data['company'] = $company;
        $this->data['schedule'] = $event;
        $this->data['page_data'] = unserialize($event->description);


        return View::make('modules.schedule.edit', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /schedule/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $validator = Validator::make(Input::all(), ScheduleV::rules());

        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $input = Input::all();

        if(is_file(Input::file('image'))){
            $image = Input::file('image');
            Image::make($image->getRealPath())->resize(460,460,true,false)->save('simpleapp/companies/triple/events/images/test.jpg');
            Image::make($image->getRealPath())->save('images/photo_albums/original/'.$image_name);
            $input['image_name'] = Input::get('name').Input::get('start');
        }
        $input['description'] = serialize($this->createSpecialArray(Input::get('title'), Input::get('content')));
        $schedule = Schedule::find($id);
        $schedule->update($input);

        return Redirect::to('modules/events/'.$schedule->company_id)->with('modules_save_message', Lang::get('general.event_item_saved'));
    }

	/**
	 * Remove the specified resource from storage.
	 * DELETE /schedule/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    private function createSpecialArray($titel, $content){
        $data = array();
        $counter = 0;
        foreach($titel AS $value){
            $data[] = array('title' => $value, 'content' => $content[$counter]);
            $counter++;
        }
        return $data;
    }

}