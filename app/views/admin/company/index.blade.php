@extends('admin.template.master')
@section('admin_content')



    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{Lang::get('company.companyHeader')}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{URL::route('admin.companies.create') }}" class="btn btn-default">{{Lang::get('company.companyCreateButton')}}</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{Lang::get('company.companyNumber')}}</th>
                                <th>{{Lang::get('company.companyName')}}</th>
                                <th>{{Lang::get('company.companyConfirmed')}}</th>
                                <th>{{Lang::get('company.companyRegisterDate')}}</th>
                                <th colspan="3">{{Lang::get('company.companyActions')}}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($companies as $company)

                                <tr class="{{$company->confirmed == 0 ? 'danger' : 'success'}}">
                                    <td>{{$company->id}}</td>
                                    <td>{{$company->company_name}}</td>
                                    <td>{{$company->confirmed == 0 ? Lang::get('general.no') : Lang::get('general.yes')}}</td>
                                    <td>{{$company->created_at->format('d-m-Y')}}</td>
                                    <td><a href="{{URL::route('admin.companies.show', $company->id)}}" class="btn btn-sm btn-default fa fa-info"></a></td>
                                    <td><a href="{{URL::route('admin.companies.edit', $company->id)}}" class="btn btn-sm btn-default fa fa-pencil"></a></td>

                                    {{ Form::open(['route' => ['admin.companies.destroy', $company->id]])}}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                            <td><button class="btn btn-sm btn-info fa fa-trash"></button></td>
                                    {{ Form::close() }}
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                    {{$companies->links()}}
                </div>
            </div>
            <!-- /.panel -->
        </div>
    </div>
@endsection