@extends('master')
@section('content')

<style>
    * {
        margin: 0px;
        padding: 0px;
    }
    #sortable { list-style-type: none; }



    .edit-page-header, .edit-page-header:focus {
        color: white;
        background: #9954bb;
        outline: none;
        border: none;
        width: 100%;
    }

    .edit-page-area, .edit-page-area:focus {
        color: black;
        background: white;
        outline: none;
        border: none;
        width: 100%;
        resize: vertical;
    }

    input::-webkit-input-placeholder { /* WebKit browsers */
        color:    white;
        font-weight: bold;
    }
    input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
        color:    white;
        font-weight: bold;
        opacity:  1;
    }
    input::-moz-placeholder { /* Mozilla Firefox 19+ */
        color:    white;
        font-weight: bold;
        opacity:  1;
    }
    input:-ms-input-placeholder { /* Internet Explorer 10+ */
        color:    white;
        font-weight: bold;
    }

</style>
<div class="container">
    <div class="row col-lg-3"  id="side-menu">
        @include('includes.manage_module')
        @include('includes.modules_menu')
    </div>
    <div class="row">

        {{Form::open()}}

        <div class="col-md-9">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{{Lang::get('general.edit_event')}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center">
                            <img alt="User Pic" src="{{url('images/modules/soundcloud_logo.png')}}" class="img-responsive"> </div>

                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                {{Form::open()}}

                                <tr>
                                    <td>{{Lang::get('general.name')}}</td>
                                    <td>{{Form::text('name', $schedule->name)}}</td>
                                </tr>
                                <tr>
                                    <td>{{Lang::get('general.start_date')}}</td>
                                    <td>{{Form::text('start', $schedule->start)}}</td>
                                </tr>
                                <tr>
                                    <td>{{Lang::get('general.end_date')}}</td>
                                    <td>{{Form::text('end', $schedule->end)}}</td>
                                </tr>
                                <tr>
                                    <td>{{Lang::get('general.location')}}</td>
                                    <td>{{Form::text('location', $schedule->location)}}</td>
                                </tr>
                                <tr>
                                    <td>{{Lang::get('general.active')}}</td>
                                    <td>{{Form::select('active', array('' => Lang::get('general.make_your_choice'), 1 => Lang::get('general.yes'), 0 => Lang::get('general.no')), $schedule->active)}}</td>
                                </tr>
                                </tbody>
                            </table>
                            @include('_partials.errors')
                        </div>
                    </div>
                </div>
            </div>
            <ul id="sortable">
                @if($page_data != null)
                @foreach($page_data as $content)

                <li class="content-box">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <input type="text" class="edit-page-header" placeholder="{{Lang::get('general.placeholder_events_title')}}" value="{{$content['title']}}" name="title[]">
                        </div>
                        <div class="panel-body">
                            {{Form::textarea(
                            'content[]',
                            $content['content'],
                            [
                            'placeholder' => Lang::get('general.placeholder_events_body'),
                            'class' => 'edit-page-area'
                            ])
                            }}
                        </div>
                        <div class="panel-footer">
                                            <span type="button" class="btn btn-success">
                                                <i class="glyphicon glyphicon-move"></i>
                                                {{Lang::get('general.replace')}}</span>
                            <button type="button" class="btn btn-primary add-content empty-values">
                                {{Lang::get('general.add')}}</button>
                            <div class="pull-right">
                                <button type="button" class="btn btn-danger remove-content hide">
                                    <i class="fa fa-trash-o"></i> {{Lang::get('general.delete')}}</button>
                            </div>
                        </div>
                    </div>
                </li>

                @endforeach
                @else
                <li class="content-box">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <input type="text" name="title[]" class="edit-page-header" placeholder="{{Lang::get('general.placeholder_events_title')}}">
                        </div>
                        <div class="panel-body">
                            {{Form::textarea(
                            'content[]',
                            '',
                            [
                            'placeholder' => Lang::get('general.placeholder_events_body'),
                            'class' => 'edit-page-area'
                            ])
                            }}
                        </div>
                        <div class="panel-footer">
                                            <span type="button" class="btn btn-success">
                                                <i class="glyphicon glyphicon-move"></i>
                                                {{Lang::get('general.replace')}}</span>
                            <button type="button" class="btn btn-primary add-content empty-values">
                                {{Lang::get('general.add')}}</button>
                            <div class="pull-right">
                                <button type="button" class="btn btn-danger remove-content hide">
                                    <i class="fa fa-trash-o"></i> {{Lang::get('general.delete')}}</button>
                            </div>
                        </div>
                    </div>
                </li>
                @endif

            </ul>
            <div class="panel-info">
                <div class="panel-heading">
                    {{Form::submit(Lang::get('general.edit'), ['class' => 'btn btn-info'])}}
                    {{Form::close()}}
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $( "#sortable" ).sortable();
        var counter = 0;
        $('.container').on('click', 'button.add-content', function (e) {
            counter++;

            if(counter >= 1){
                $('.remove-content').removeClass('hide');
            }
            var $content = $(this).parents('.content-box');
            var $clone = $content.clone();
            $clone.find('input, textarea').val('');
            $content.before($clone);
            $clone.hide().fadeIn('slow');
        });

        $('.container').on('click', 'button.remove-content', function (e) {
            counter--;
            if(counter == 0){
                $('.remove-content').addClass('hide');
            }
            var $content = $(this).parents('.content-box');
            $content.remove();
        });

        $('#update-page').click(function(){
            var valid = true;
            $('#new_page input, #new_page textarea').each(function(){
                if($(this).val() == ''){
                    valid = false;
                    $('#empty-fields').removeClass('hide');
                }
            });

            if(valid == true){
                $('#new_page').submit();
                $('#empty-fields').addClass('hide');
            }
        });
    });

</script>

@endsection

































