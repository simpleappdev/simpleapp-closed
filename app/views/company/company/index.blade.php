@extends('master')
@section('content')
    <div class="container">

        <div class="row">

            <div class="col-md-2">
                @include('includes.account_menu')
            </div>

            @include('company.company.sidebar.overview')

            <div class="col-md-10">


                @if($user)
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{Lang::get('company.my_companies')}}</h3>
                                </div>

                                <table class="table table-striped">
                                    <tr>
                                        <td>{{Lang::get('company.company_name')}}</td>
                                        <td>{{Lang::get('general.app_status')}}</td>
                                        <td>{{Lang::get('table_options.edit')}}</td>
                                        <td>{{Lang::get('payment.bills')}}</td>
                                        <td>{{Lang::get('modules.modules')}}</td>
                                        <td>{{Lang::get('theme.themes')}}</td>
                                    </tr>

                                    @foreach($user->getCompanies AS $company)
                                        <tr>
                                            <td>{{$company->company_name}}</td>
                                            <td>
                                                @if($company->status == 0)
                                                    {{Lang::get('general.not_yet_pushed_to_store')}}
                                                @elseif($company->status == 1)
                                                    {{Lang::get('general.pushed_to_store')}}
                                                @elseif($company->status == 2)
                                                    {{Lang::get('general.app_waiting_approvement')}}
                                                @elseif($company->status == 2)
                                                    {{Lang::get('general.app_rejected')}}
                                                @else
                                                    {{Lang::get('general.status_unknown')}}
                                                @endif
                                            </td>

                                            <td><a href="{{url('edit/'.$company->id.'/')}}">{{Lang::get('table_options.view')}}</a></td>
                                            <td><a href="{{url('payment/bills/'.$company->id.'/')}}">{{Lang::get('table_options.manage')}}</a></td>
                                            <td><a href="{{url('modules/overview/'.$company->id.'/')}}">{{Lang::get('table_options.manage')}}</a></td>
                                            <td><a href="{{url('modules/theme/'.$company->id)}}">{{Lang::get('table_options.manage')}}</a></td>
                                        </tr>
                                    @endforeach
                                </table>


                            </div>
                        </div>
                    </div>
                @endif

            </div>

        </div>

    </div>
    <!-- /.container -->

@endsection
