@if(Session::get('modules_save_message'))
    <div class="panel panel-success" id="message-saved">
        <div class="panel-heading">
            <h3 class="panel-title">{{Lang::get('general.succeeded')}}</h3>
        </div>
        <div class="panel-body">
            {{Session::get('modules_save_message')}}
        </div>
    </div>
<script>
    $(document).ready(function(){
        $('#message-saved').fadeOut(3000);
    });
</script>
@endif

