@extends('master')
@section('content')
    <div class="container" id="change_password_page">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">{{Lang::get('general.my_account')}}</p>
                <div class="list-group">
                    <a href="{{url('account/')}}" class="list-group-item">{{Lang::get('general.my_settings')}}</a>
                    <a href="{{url('account/companies/')}}" class="list-group-item">{{Lang::get('general.my_activities')}}</a>
                </div>
                @include('includes.account_menu')
            </div>


            <div class="col-md-9">
                <div class="row">
                    @include('_partials.errors')

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{Lang::get('general.change_password')}}</h3>
                        </div>
                        <div class="panel-body" id="edit-profile">
                            <div class="row">
                                {{Form::open()}}
                                <div class="col-md-9 col-lg-9 ">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td class="col-md-4">{{Lang::get('general.new_password')}}</td>
                                            <td class="col-md-8">{{Form::password('password')}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{Lang::get('general.repeat_password')}}</td>
                                            <td>{{Form::password('password_confirmation')}}</td>
                                        </tr>

                                        </tbody>
                                    </table>


                                </div>
                                <div class="col-md-9 col-lg-9 ">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td class="col-md-4">{{Lang::get('general.current_password')}}</td>
                                            <td class="col-md-8">{{Form::password('current_password')}}</td>
                                        </tr>

                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>

                        <span class="pull-right">
                            {{Form::submit('Save', ['class' => 'btn btn-sm btn-warning'])}}
                            {{Form::close()}}
                        </span>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

@endsection
