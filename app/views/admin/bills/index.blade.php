@extends('admin.template.master')
@section('admin_content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{{Session::get('message') ? Session::get('message') : Lang::get('payment.open_bills')}}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{Lang::get('payment.open_bills')}}
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        {{Form::open()}}
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{Form::checkbox(null, null, false, ['id' => 'select_all_bills'])}}</th>
                                    <th>{{Lang::get('payment.bill_number')}}</th>
                                    <th>{{Lang::get('payment.company_name')}}</th>
                                    <th>{{Lang::get('payment.price')}}</th>
                                    <th>{{Lang::get('payment.bill_date')}}</th>
                                    <th>{{Lang::get('payment.more_info')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bills as $bill)
                                <tr>
                                    <td>{{Form::checkbox('payment[]', $bill->id)}}</td>
                                    <td>{{$bill->price}}</td>
                                    <td>{{$bill->company->company_name}}</td>
                                    <td>&euro; {{number_format($bill->price, 2, ',', '.')}}</td>
                                    <td>{{$bill->created_at->format('d-m-Y')}}</td>
                                    <td><a href="{{url('admin/bills/'.$bill->id)}}">{{Lang::get('payment.show')}}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                    {{Form::submit(Lang::get('payment.mark_as_payed'))}}
                    {{Form::close()}}
                </div>
                <div class="panel-footer">
                    {{$bills->links()}}
                </div>
            </div>
            <!-- /.panel -->
        </div>
    </div>
@endsection