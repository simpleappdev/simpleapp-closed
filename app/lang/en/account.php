<?php

return array (
  'signup' => 'Signup',
  'website' => 'Website',
  'password' => 'Password',
  'repeat_password' => 'Repeat password',
  'create_account' => 'Create account',
  'my_account' => 'My account',
  'login' => 'Login',
  'change_photo' => 'Change photo',
  'change_password' => 'Change password',
  'logout' => 'Logout',
  'username' => 'Username',
  'email' => 'Email',
  'first_name' => 'First name',
  'surname' => 'Surname',
);
