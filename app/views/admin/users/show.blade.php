@extends('admin.template.master')
@section('admin_content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Users</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12" >

            <a href="{{URL::route('admin.users.index') }}" class="btn btn-default">{{Lang::get('users.UserBackButton')}}</a>


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{$user->username}}</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>

                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>{{Form::label('first_name', Lang::get('users.UserFirstname'))}}</td>
                                <td>{{$user->first_name}}</td>
                            </tr>
                            <tr>
                                <td>{{Form::label('surname', Lang::get('users.UserLastname'))}}</td>
                                <td>{{$user->surname}}</td>
                            </tr>
                            <tr>
                                <td>{{Form::label('email', Lang::get('users.UserEmail'))}}</td>
                                <td>{{$user->email}}</td>
                            </tr>
                                <td>{{Form::label('phonenumber', Lang::get('users.UserPhonenumber'))}}</td>
                                <td>{{$user->phonenumber}}</td>
                            <tr>
                            <tr>
                                <td>{{Form::label('confirmed', Lang::get('users.UserAccountConfirmed'))}}</td>
                                <td>{{$user->confirmed}}</td>
                            </tr>
                            <tr>
                                <td>{{Form::label('role', Lang::get('users.UserRoles'))}}</td>
                                <td>{{$user->role}}</td>
                            </tr>
                             </tr>

                            </tbody>
                        </table>

                        <a href="#" class="btn btn-primary">{{Lang::get('users.UserSales')}}</a>
                        <a href="#" class="btn btn-primary">{{Lang::get('users.UserTeamSales')}}</a>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

@endsection