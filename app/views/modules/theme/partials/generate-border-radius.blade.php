{{--
@Requires:
    name          => string
    value         => array

@Missing:
    name attribute on input group
--}}
<div class="form-group">
    <label for="" class="theme-builder-label">{{{ $label }}}</label>
    <div class="theme-builder-row border-radius-fields form-inline">
        @foreach($value as $v)
        <input type="number" class="border-radius form-control input-sm" min="0" max="20" value="{{{ $v }}}" name="{{ $name }}[border-radius][]">
        @endforeach
    </div>
</div>