@extends('admin.template.master')
@section('admin_content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Users</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12" >


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{Lang::get('users.UserPanelTitle')}}r</h3>
            </div>
            <div class="panel-body">
                {{ Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PUT'])}}
                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>

                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>{{Form::label('first_name', Lang::get('users.UserFirstname'))}}</td>
                                <td>{{Form::text('first_name', null, ['class' => 'form-control'])}}</td>
                            </tr>
                            <tr>
                                <td>{{Form::label('surname', Lang::get('users.UserLastname'))}}</td>
                                <td>{{Form::text('surname', null, ['class' => 'form-control'] )}}</td>
                            </tr>
                            <tr>
                                <td>{{Form::label('email', Lang::get('users.UserEmail'))}}</td>
                                <td>{{Form::text('email', null, ['class' => 'form-control'] )}}</td>
                            </tr>
                            <tr>
                                <td>{{Form::label('website', Lang::get('users.UserWebsite'))}}</td>
                                <td>{{Form::text('website', null, ['class' => 'form-control'] )}}</td>
                            </tr>
                            <tr>
                                <td>{{Form::label('username', Lang::get('users.UserUsername'))}}</td>
                                <td>{{Form::text('username', null, ['class' => 'form-control'] )}}</td>
                            </tr>

                                <td>{{Form::label('phonenumber', Lang::get('users.UserPhonenumber'))}}</td>
                                <td>{{Form::text('phonenumber', null, ['class' => 'form-control'])}}</td>
                            <tr>
                            <tr>
                                <td>{{Form::label('confirmed', Lang::get('users.UserAccountConfirmed'))}}</td>
                                <td>{{Form::select('confirmed', [1 => Lang::get('general.yes'), 0 => Lang::get('general.no')], $user->confirmed)}}</td>
                            </tr>
                            <tr>
                                <td>{{Form::label('role', Lang::get('users.UserRoles'))}}</td>
                                <td>{{Form::select('role', $roles, $user->role)}}</td>
                            </tr>
                             </tr>

                            </tbody>
                        </table>

                        <a href="#" class="btn btn-primary">{{Lang::get('users.UserSales')}}</a>
                        <a href="#" class="btn btn-primary">{{Lang::get('users.UserTeamSales')}}</a>

                        {{ Form::submit(Lang::get('users.UserEdit'), ['class' => 'btn btn-primary pull-right'])}}

                    </div>
                </div>

                {{Form::close()}}
            </div>


        </div>
    </div>
</div>

@endsection