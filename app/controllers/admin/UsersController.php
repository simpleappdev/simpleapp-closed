<?php

namespace admin;

use UserPlain, View, Redirect, Input, Services\CustomValidator, Role, Illuminate\Support\Facades\Session;

class UsersController extends \BaseController {

    public $user;

    public function __construct(UserPlain $user){
        $this->user = $user;
    }
	/**
	 * Display a listing of the resource.
	 * GET /admin/users
	 *
	 * @return Response
	 */
	public function index()
	{

		$data = ['users' => $this->user->paginate(5)];

		return View::make('admin.users.index', $data);

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/users/create
	 *
	 * @return Response
	 */
	public function create()
	{
        $roles = Role::all()->lists('name', 'id');
        return View::make('admin.users.create', ['roles' => $roles]);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin/users
	 *
	 * @return Response
	 */
	public function store()
	{
        if(CustomValidator::validate(Input::all(), 'new_user') === false) {
            return Redirect::back()->withInput()->withErrors(Session::get('validation_errors'));
        }

        $this->user->create(Input::all());

        return Redirect::route('admin.users.index');
	}

	/**
	 * Display the specified resource.
	 * GET /admin/users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($user)
	{
        return View::make('admin.users.show', ['user' => $user]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($user)
	{
        $roles = Role::all()->lists('name', 'id');
        return View::make('admin.users.edit', ['user' => $user, 'roles' => $roles]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($user)
	{
        if(CustomValidator::validate(Input::all(), 'new_user') === false) {
            return Redirect::back()->withInput()->withErrors(Session::get('validation_errors'));
        }

        $user->update(Input::all());

		return Redirect::route('admin.users.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($user)
	{
        $user->destroy($user->id);
		
		return Redirect::route('admin.users.index');
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /admin/users
	 *
	 * @return Response
	 */
	public function search()
	{
		//
		$users = \User::where(\Input::get('search_for'), 'like', '%'.\Input::get('search_value').'%')->paginate(20);

		$data = array('users' => $users);

		return \View::make('admin/users/index', $data);
	}

}