@if(Session::get('error'))
<div class="panel panel-warning">
    <div class="panel-heading">
        <h3 class="panel-title">{{Lang::get('general.validation_error')}}</h3>
    </div>
    <div class="panel-body">
        @foreach(Session::get('error') as $msg)
        {{$msg}}<br />
        @endforeach
    </div>
</div>
@endif