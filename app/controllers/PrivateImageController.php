<?php

use Helpers\Helpers;

class PrivateImageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /privateimage
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$data['company'] = Company::find($id);

        return View::make('company.private_images.index',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /privateimage/create
	 *
	 * @return Response
	 */
	public function create($id)
	{
        $data['company'] = Company::find($id);

        return View::make('company.private_images.create',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /privateimage
	 *
	 * @return Response
	 */
	public function store($id)
	{
        $company = Company::find($id);

        $rules = new Services\Validators\PrivateImage;

        $validator = Validator::make(Input::all(), $rules->signUp());

        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $folder = 'simpleapp/companies/'.$company->slug.'/website/private_image_gallery/';
        $image_name = Helpers::randomString(75).'-'.Input::file('image')->getClientOriginalName();
        Image::make(Input::file('image')
            ->getRealPath())
            ->save($folder.$image_name);

        $imageGallery = new PrivateImage;
        $imageGallery->title = Input::get('title');
        $imageGallery->file_name = $folder.$image_name;
        $imageGallery->company_id = $id;
        $imageGallery->save();

        //$image = new PrivateImage;
        return View::make('company.private_images.create',['id' => $id]);
	}

	/**
	 * Display the specified resource.
	 * GET /privateimage/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make();
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /privateimage/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make();
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /privateimage/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        return View::make();
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /privateimage/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        return View::make();
	}

}