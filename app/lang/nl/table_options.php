<?php

return array (
  'edit' => 'Wijzigen',
  'view' => 'Bekijken',
  'manage' => 'Beheren',
  'add' => 'Toevoegen',
  'create' => 'Aanmaken',
  'delete' => 'Verwijderen',
);
