@extends('master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-sm-4 col-lg-8 col-md-4">
            @include('_partials.errors_create_account')

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{{Lang::get('account.signup')}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>

                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                {{Form::open(['action' => 'UserController@store'])}}

                                <tr>
                                    <td>Surname name</td>
                                    <td>{{Form::text('surname')}}</td>
                                </tr>
                                <tr>
                                    <td>Your email (needed for validation)</td>
                                    <td>{{Form::email('email')}}</td>
                                </tr>
                                <tr>
                                    <td>{{Lang::get('account.website')}}</td>
                                    <td>{{Form::text('website')}}</td>
                                </tr>
                                <tr>
                                    <td>{{Lang::get('account.password')}}</td>
                                    <td>{{Form::password('password')}}</td>
                                </tr>
                                <tr>
                                    <td>{{Lang::get('account.repeat_password')}}</td>
                                    <td>{{Form::password('password_confirmation')}}</td>
                                </tr>



                                <tr>
                                    <td>&nbsp;</td>
                                    <td>{{Form::submit(Lang::get('account.create_account'))}} {{Form::close()}}</td>
                                </tr>


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
            <!-- </div> -->

        </div>
    </div>
</div>
<style>
    .user-row {
        margin-bottom: 14px;
    }

    .user-row:last-child {
        margin-bottom: 0;
    }

    .dropdown-user {
        margin: 13px 0;
        padding: 5px;
        height: 100%;
    }

    .dropdown-user:hover {
        cursor: pointer;
    }
</style>

@endsection
