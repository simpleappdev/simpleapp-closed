<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Helpers\LogbookH;

class checkPlans extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'payments:check_plans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks all the plans and re-assign if needed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //Get all Companies
        $date = date('m')-1;

        if($date == 0){
            $companies = Company::where('last_bill', '==', 12)->has('Plan')->get();
            echo 'In de if';
        } else {
            $companies = Company::where('last_bill', '<=', $date)->get();
        }

        //Get Current date
        $date               = date('d-m-Y');
        //Set first day of the month
        $first_day_month    = date('Y-m-01');
        //Check the last day of the month
        $last_day_month     = date("Y-m-t", strtotime($date));
        //Get the last day of of the month in a integer
        $last_day_month_day = date("d", strtotime($last_day_month));


        //Run this script when its the last day of the month
        if($date != $last_day_month){
            foreach($companies as $company){

                //Set a variable for the current plan
                $current_plan = $company->currentPlan->currentPlanInfo;
                //Set a variable for the switching plan
                $switching_plan = $company->currentPlan->switchingPlanInfo;
                //Get all the bills between date and date
                $all_bills = $company->bills($first_day_month, $last_day_month);
                //Pre-set $bill_price with the current plan pricing
                $bill_price = $current_plan->price;

                //If the company wants to switch to another plan
                if($switching_plan != null){
                    if(!empty($switching_plan->id) != 0){
                        //Set the new current plan id
                        $company->currentPlan->current_plan_id = $switching_plan->id;
                        //Set the switching plan ID to 0
                        $company->currentPlan->switching_plan_id = 0;
                        //Save the plan
                        $company->currentPlan->save();
                        $this->info('Switched plan');
                    }
                }


                if(count($all_bills) >= 1){
                    //If $all_bills have data, then don't create a bill
                    $this->info('Already found a bill for this month!');
                } else {
                    //Variable for registered the payment plan date
                    $registred_date = $company->plan->registred_date;

                    //Check if the registered month is equal to the current, so we only create
                    //a bill for the company used the app
                    if($registred_date->format('m') == date('m')){
                        //Set variable $registered with format
                        $registered = new DateTime($registred_date->format('Y-m-d'));
                        //Set variable $last_day to check the last day of the month
                        $last_day = new DateTime($last_day_month);
                        //Check the days between the 2 dates
                        $days_between = $registered->diff($last_day)->format("%a");
                        //Get the price a day by a formula price / days
                        $price_a_day = $current_plan->price / $last_day_month_day;
                        //Set the bill price by formula: days used x price a day
                        $bill_price = $price_a_day * $days_between;
                    }

                    $company->last_bill = date('m');
                    $company->save();

                    // Save the bill
                    $bill = new Bill;
                    $bill->price        = $bill_price;
                    $bill->company_id   = $company->id;
                    $bill->plan_id      = $current_plan->id;
                    $bill->save();
                    $this->info('Bill created');

                    LogbookH::createLog(
                        Lang::get('system.fire_artisan_command_check_plans'),
                        $company->id,
                        Lang::get('system.created_bill'),
                        0,
                        1);


                }
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
