<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhoneMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
            $table->string('module');
            $table->string('pagename');
			$table->timestamps();
		});

		Schema::create('company_menu', function($table)
        {
            $table->increments('id')->unsigned();
            $table->integer('menu_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->foreign('menu_id')->references('id')->on('menus'); // assumes a users table
            $table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('company_menu', function(Blueprint $table) {
            $table->dropForeign('company_menu_menu_id_foreign');
            $table->dropForeign('company_menu_company_id_foreign');
        });

		Schema::drop('company_menu');
		Schema::drop('menus');
	}

}
