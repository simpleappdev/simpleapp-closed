<?php namespace Helpers;

use PagesM;
use File;

class RenderAppH {

    public static function renderPage($page, $module_type, $button_id){

        $data = '<button'.$button_id.'>
		<name>'.$page->title.'</name>
		<module>'.$module_type.'</module>
		<pagename>'.$page->name.'</pagename>
		</button'.$button_id.'>';

        return $data;
    }

    public static function renderModule($module, $button_id){
        $data = '<button'.$button_id.'>
		<name>'.$module->title.'</name>
		<module>'.$module->type.'</module>
		<pagename>'.$module->title.'</pagename>
		</button'.$button_id.'>';

        return $data;
    }

    public static function setMenu($company, $data){
        $company_slug = $company->slug;

        $dir = public_path().'simpleapp/companies/'.$company->slug.'/buttons.xml';

        return File::put($dir, $data);
    }
}