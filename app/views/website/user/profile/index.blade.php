@extends('master')
@section('content')
    <style>
        input {
            width: 100%;
        }

        .disabled {
            background: white;
            border: none;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('input[disabled="disabled"]').addClass('disabled');

            $submitted = false;
            $('#edit-user, #edit-user-resubmit').click(function( e ){

                if($submitted == false){
                    $('input[disabled="disabled"]').removeAttr('disabled');
                    $('input').removeClass('disabled');
                    $(this).text($(this).data('save-text'))
                }

                if($(this).attr('id') == 'edit-user-resubmit' || $submitted == true){
                    $('#edit-profile form').submit();
                }

                $submitted = true;


            });
        });
    </script>
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">{{Lang::get('general.my_account')}}</p>
                <div class="list-group">
                    <a href="{{url('account/')}}" class="list-group-item">{{Lang::get('general.my_settings')}}</a>
                    <a href="{{url('account/companies/')}}" class="list-group-item">{{Lang::get('general.my_activities')}}</a>
                </div>
                @include('includes.account_menu')
            </div>


            <div class="col-md-9">
                <div class="row">
                    @include('_partials.errors')

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{$user->first_name}} {{$user->last_name}}</h3>
                        </div>
                        <div class="panel-body" id="edit-profile">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>
                                {{Form::open()}}
                                <div class=" col-md-9 col-lg-9 ">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td class="col-lg-4">{{Lang::get('general.first_name')}}</td>
                                            <td class="col-lg-8">{{$user->first_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{Lang::get('general.surname')}}</td>
                                            <td>{{$user->surname}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{Lang::get('general.email')}}</td>
                                            <td>{{$user->email}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{Lang::get('general.phone_number')}}</td>
                                            <td>{{Form::text('phonenumber', $user->phonenumber, [Session::get('disabled') == 'disabled' ? '' : 'disabled'])}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{Lang::get('general.website')}}</td>
                                            <td>{{Form::text('website', $user->website, [Session::get('disabled') == 'disabled' ? '' : 'disabled'])}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{Lang::get('general.member_since')}}</td>
                                            <td>{{$user->created_at->format('d-m-Y')}}</td>
                                        </tr>

                                        </tbody>
                                    </table>

                                    <a href="{{url('account/password')}}" class="btn btn-primary">{{Lang::get('general.change_password')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>

                        <span class="pull-right">
                            <span data-original-title="Edit profile" data-save-text="{{Lang::get('general.save_profile')}}"  id="{{Session::get('disabled') == 'disabled' ? 'edit-user-resubmit' : 'edit-user'}}" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i> {{Session::get('disabled') == 'disabled' ? Lang::get('general.save_profile') : Lang::get('general.edit_profile')}}</span>
                        </span>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

@endsection
