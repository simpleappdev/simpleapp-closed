@extends('admin.template.master')
@section('admin_content')

    <div class="row">

        <div class="col-lg-12">
            <h1 class="page-header">{{Lang::get('roles.RoleHeader')}}</h1>
        </div>


        <div class="col-sm-12 col-lg-12 col-md-12">

            <h2>{{Lang::get('roles.RoleShow')}}</h2>

            <a href="{{URL::route('admin.roles.index') }}" class="btn btn-default">{{Lang::get('roles.RoleBackButton')}}</a>

            <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <td>{{Lang::get('roles.RoleId');}}</td>
                    <td>{{Lang::get('roles.RoleName');}}</td>
                    <td>{{Lang::get('roles.RoleDescription');}}</td>
                    <td>{{Lang::get('roles.RoleCreated');}}</td>
                    <td>{{Lang::get('roles.RoleUpdated');}}</td>
                </tr>

                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->description }}</td>
                    <td>{{ $role->created_at }}</td>
                    <td>{{ $role->updated_at }}</td>
                </tr>
            </table>
                </div>
        </div>
    </div>
    </div>
@endsection