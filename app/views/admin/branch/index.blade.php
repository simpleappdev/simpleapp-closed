@extends('admin.template.master')
@section('admin_content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{Lang::get('branch.branchHeader');}}</h1>


            <div class="col-sm-12 col-lg-12 col-md-12">

                <a href="{{URL::route('admin.branches.create') }}" class="btn btn-default">{{Lang::get('branch.branchCreateButton')}}</a>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered" border="1" width="960">
                        <tr>
                            <td>{{Lang::get('branch.branchId');}}</td>
                            <td>{{Lang::get('branch.branchDescription');}}</td>
                            <td>{{Lang::get('branch.branchLanguage');}}</td>
                            <td>{{Lang::get('branch.branchCreatedAt');}}</td>
                            <td>{{Lang::get('branch.branchUpdatedAt');}}</td>
                            <td colspan="3">{{Lang::get('branch.branchActions');}}</td>
                        </tr>

                        @foreach($branches as $branch)
                        <tr>
                            <td>{{$branch->id}}</td>
                            <td>{{$branch->description}}</td>
                            <td>{{$branch->language}}</td>
                            <td>{{$branch->created_at}}</td>
                            <td>{{$branch->updated_at}}</td>
                            <td><a href="{{URL::route('admin.branches.show', $branch->id)}}" class="btn btn-sm btn-default fa fa-info"></a></td>
                            <td><a href="{{URL::route('admin.branches.edit', $branch->id)}}" class="btn btn-sm btn-default fa fa-pencil"></a></td>

                            {{ Form::open(['route' => ['admin.branches.destroy', $branch->id]])}}
                            {{ Form::hidden('_method', 'DELETE') }}
                            <td><button class="btn btn-sm btn-info fa fa-trash"></button></td>
                            {{ Form::close() }}
                        </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection