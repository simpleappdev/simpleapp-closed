<?php

use Services\Validators\CompanyV;
use Helpers\CompanyH;
use Services\Validators\CustomValidator;

class CompanyController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /company
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();

		return View::make('company.company.index', ['user' => $user]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /company/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['branches'] = Branch::lists('description', 'id', Lang::get('general.select_choice'));

		//$data = array('branches' => $branches);
		return View::make('company.company.signup', $data);

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /company
	 *
	 * @return Response
	 */
	public function store()
	{

		if(CustomValidator::validate(Input::all(), 'company_validation') == false){
			return Redirect::back()->withInput()->withErrors(Session::get('validation_errors'));
		}
		/*
         * Insert new company into the Database
         */
		$a_key 		= CompanyCreateKeys::where('key', '=' , Input::get('activation_code'))->update(['used' => 1]);
		$company 	= new Company;
		if($a_key == true){
			$company->active = 1;
		}


		$company->company_name  = Input::get('company_name');
		$company->slug          = Input::get('slug');
		$company->information   = 'empty';
		$company->status = 0;
		$company->save();
		$company->users()->attach(Auth::user()->id);


		if($company->id){
			CompanyH::render_folder_structure($company->slug, true);
		}

		return Redirect::to('myaccount');
	}

	/**
	 * Display the specified resource.
	 * GET /company/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$company = Company::find($id);

		return View::make('company.company.edit', $data = array( 'company' => $company));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /company/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company = Company::find($id);

        $value = Input::get('information');



        $input = Input::except('_token','default','title');

		$company->company_name = Input::get('company_name');
		$company->information = $value;
		$company->save();


		return $this->show($id);

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /company/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return 'Update';
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /company/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}