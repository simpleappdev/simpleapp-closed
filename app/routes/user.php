<?php

/**
 * This file routes all user paths that are not related
 * to a company
 */

Route::get('myaccount', 		array('before' => 'auth', 'uses' => 'AccountController@index'));
Route::post('myaccount', 		array('before' => 'auth', 'uses' => 'AccountController@update'));

Route::get('account', 'ProfileController@index');
Route::post('account', 'ProfileController@update');
Route::get('account/password', 'ProfileController@change_password');
Route::post('account/password', 'ProfileController@update_password');