<?php

return array (
  'companies' => 'Companies',
  'support' => 'Support',
  'create_company' => 'Create Company',
  'logbook' => 'Logbook',
  'invoices' => 'Invoices',
  'settings' => 'Settings',
  'roles' => 'Roles',
  'permissions' => 'Permissions',
  'show_plans' => 'Show Plans',
  'create_plan' => 'Create Plan',
  'payment_plans' => 'Payment Plans',
  'plan_options' => 'Plan options',
  'show_companies' => 'Show Companies',
  'users' => 'Users',
  'show_users' => 'Show Users',
  'create_user' => 'Create User',
  'news' => 'News',
  'admin_menu' => 'Admin',
);
