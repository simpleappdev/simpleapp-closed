@extends('master')
@section('content')
<div class="container">
	<div class="col-md-3">
		<div class="col-md-12">
            @include('admin.defaults.sidemenu')
        </div>
    </div>
    <div class="col-md-9">
	
	    <div class="col-sm-12 col-lg-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Bewerk gebruiker {{$user->username}} ({{$user->id}})</h3>
				</div>
				<div class="panel-body">
					{{Form::open(array('method' => 'put'))}}
					{{Form::label('email', 'Email'), Form::email('email', $user->email )}}
					{{Form::label('', 'Laatst bewerkt'), Form::text('last_edit', $user->updated_at, $attributes = array('disabled' => 'disabled') )}}
					{{Form::label('', 'Aangemaakt op'), Form::email('email', $user->created_at, $attributes = array('disabled' => 'disabled') )}}
					{{Form::submit('Opslaan')}}
				</div>
			</div>
	    </div>
	    <div class="col-sm-12 col-lg-12 col-md-12">
		    <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Huidige rechten</h3>
				</div>
				<div class="panel-body">
					<table border="1">
						@foreach($user->roles AS $role)
							<tr>
								<td>{{Form::checkbox('delete_role[]', $role->id)}}</td>
								<td>{{$role->name}}</td>
							</tr> 
						@endforeach
					</table>
					{{Form::submit('Opslaan')}}
				</div>
			</div>
	    </div>

	
	    <div class="col-sm-12 col-lg-12 col-md-12">
		    <div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Geef rechten</h3>
				</div>
				<div class="panel-body">
					<table border="1">
				    	@foreach($roles AS $role => $value)
				    		<tr>
					    		
					    		<td>{{Form::checkbox('give_role[]', $role)}}</td>	
					    		<td>{{$value}}</td> 
				    		</tr>
				    	@endforeach
			    	</table>
					{{Form::submit('Opslaan')}}
			    	{{Form::close()}}
				</div>
			</div>
	    </div>

	</div>
</div>
@endsection