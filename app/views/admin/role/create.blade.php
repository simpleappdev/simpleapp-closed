@extends('admin.template.master')
@section('admin_content')

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">{{Lang::get('roles.RoleHeader')}}</h1>
            </div>

            <div class="col-sm-4 col-lg-4 col-md-4">

                <h2>{{Lang::get('roles.RoleCreate')}}</h2>

                {{ Form::open(['route' => 'admin.roles.store'])}}
                {{ Form::label('role_name', Lang::get('roles.RoleLabelName'))}}
                {{ Form::text('role_name', null, ['placeholder' => Lang::get('roles.RolePlaceholderName'), 'class' => 'form-control'])}}

                {{ Form::Label('role_description', Lang::get('roles.RoleLabelDescription'))}}
                {{ Form::text('role_description', null, ['placeholder' => Lang::get('roles.RolePlaceholderDescription'), 'class' => 'form-control'])}}

                <h2>Assign to permissions</h2>

                @foreach($permissions as $permission)
                    <div class="checkbox">
                        <label>
                            {{Form::checkbox('permission[' . $permission->id . ']', $permission->name)}}
                            {{ucfirst($permission->name)}}
                        </label>
                    </div>
                @endforeach

                {{ Form::submit(Lang::get('roles.RoleCreate'), ['class' => 'btn btn-default'])}}
                {{ Form::close()}}

            </div>
        </div>
    </div>
@endsection