<?php

return [
    'company_name' => 'SimpleApp',
    'address' => 'Tamboerstraat 13',
    'zip_code' => '3034PT',
    'city' => 'Rotterdam',
    'country' => Lang::get('language.netherlands'),

    'bank_details_name' => 'Donny van Grondelle'
];