<?php namespace admin;

use Codesleeve\Stapler\File\File;
use Helpers\Helpers;
use News;
use Input;
use View;
use LaravelLocalization;
use Redirect;
use Lang;
use ModelNotFoundException;
use NotFoundHttpException;
use Intervention\Image\Facades\Image;
use Services\CustomValidator;
use Illuminate\Support\Facades\Session;
use Config;


class NewsController extends \BaseController
{

    public function __construct(News $news)
    {
        $this->news = $news;
    }

    public function index()
    {
        $news = $this->news->withTrashed()->paginate(20);

        return View::make('admin.news.index', ['news' => $news]);
    }

    public function create()
    {
        return View::make('admin.news.create');
    }

    public function store()
    {
        //Check if the fields are valid
        if (CustomValidator::validate(Input::all(), 'new_news') === false) {
            return Redirect::back()->withInput()->withErrors(Session::get('validation_errors'));
        }

        //Create the correct image
        $this->createImage();

        //set the the inputs and create the item
        $input = Input::except('image');
        $input['image'] = $this->createImage()['image'];
        $input['slug'] = Helpers::seoUrl($input['title']);

        $this->news->create($input);

        return Redirect::route('admin.news.index')->with('message', Lang::get('general.news_created'));
    }

    public function show($news)
    {
        return View::make('admin.news.show', ['news' => $news]);
    }

    public function edit($news)
    {
        return View::make('admin.news.edit', ['news' => $news]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /admin/news/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($news)
    {
        //check if valid
        if (CustomValidator::validate(Input::all(), 'new_news') === false) {
            return Redirect::back()->withInput()->withErrors(Session::get('validation_errors'));
        }

        //create the correct image
        $this->createImage();

        $input = Input::all();

        /*
         * if no_image is selected, create the correct image. The if else in that function will handle it correctly
         * If it is not set and the user has chosen a new file to upload, create that new file
         * else, do not change the file.
         *
         */
        if ($input['no_image'] == 1) {
            $input['image'] = $this->createImage()['image'];
        } else {
            if (Input::hasFile('image')) {
                $input['image'] = $this->createImage()['image'];
            } else {
                $input['image'] = $news->image;
            }
        }

        //Create the slug and update the data
        $input['slug'] = Helpers::seoUrl($input['title']);

        $news->update($input);

        return Redirect::route('admin.news.index')->with('message', Lang::get('general.news_created'));
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /admin/news/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($news)
    {
        $news->destroy($news->id);
        return Redirect::route('admin.news.index')->with('message', Lang::get('news.deleted'));
    }

    public function createImage()
    {
        if (Input::hasFile('image')) {
            $file = Input::file('image');
            //Get the name of the image
            $image_name = Input::file('image')->getClientOriginalName();

            //Set timestamp to prevent overwritten false and make sure they are unique
            $timestamp = date('d-m-Y');

            //Create the new image name
            $image_name_save = $timestamp . '-' . $image_name;

            //Resize and save the image
            Image::make($file)->resize(50, 50)->save(public_path() . Config::get('default.news_thumbnail_image_location') . $image_name_save);
            Image::make($file)->resize(320, 250)->save(public_path() . Config::get('default.news_resized_image_location') . $image_name_save);
            Image::make($file)->save(public_path() . Config::get('default.news_original_image_location') . $image_name_save);

        } else {
            $image_name_save = 'default_image.jpg';
        }

        return ['image' => $image_name_save];
    }

}