{{--
@Requires:
name          => string
input_field   => string
type          => string
--}}
<div class="form-group">
    <label class="theme-builder-label">{{{ $label }}}</label>
    <div class="theme-builder-row border-radius-fields form-inline">
        @for ($i=0; $i <= 3; $i++)
        <input type="number" name="{{ $name }}[{{$type}}][]" class="{{{ $type }}}-selection form-control input-sm" min="0" max="20" value="{{{ isset($value[$i]) ? $value[$i] : 0}}}">
        @endfor
    </div>
</div>