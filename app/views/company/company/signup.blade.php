@extends('master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">

        </div>

        <div class="col-sm-4 col-lg-8 col-md-4">
            @include('_partials.errors')

            <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">{{Lang::get('company.signup_new')}}</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> </div>

                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                    {{Form::open()}}
                      <tr>
                        <td>{{Lang::get('form.company_name')}}</td>
                        <td>{{Form::text('company_name')}}</td>
                      </tr>
                      <tr>
                        <td>{{Lang::get('form.slug')}}</td>
                        <td>{{Form::text('slug')}}</td>
                      </tr>
                      <tr>
                        <td>{{Lang::get('form.branch')}}</td>
                        <td>{{Form::select('branch', array('default' => Lang::get('form.select')) + $branches, 'default')}}</td>
                      </tr>
                      <tr>
                          <td>{{Lang::get('form.unique_key')}} {{Form::checkbox('use_activation_code', '1');}}</td>
                          <td>{{Form::input('hidden', 'activation_code')}}</td>
                      </tr>
                      <tr>
                      	  <td>{{Lang::get('form.create')}}</td>
                          <td>{{Form::submit(Lang::get('form.create'))}} {{Form::close()}}</td>
                      </tr>
                      
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
                 <div class="panel-footer">
                        <p>
                            {{Lang::get('form.signup_attention_new_company')}}
                        </p>
                </div>

          </div>
        <!-- </div> -->

        </div>
    </div>
</div>

    {{HTML::script('template/front/js/scripts/company.js')}}
@endsection
