Please do not edit the files in app/config to make local changes and push them.
Read below, there needs to be no changes to the config files in app.

## SimpleApp ... why ? ... just because.

### Install ###

*Will require PHP >= 5.4.*

1. Get your self composer. https://getcomposer.org/download/

2. Run `composer install` from the root of the project.

3. Config changes

    1. `bootstrap/start.php` L29, add your hostname to the array

    2. local configs `app/config/local/`

        1. app.php

                return array(
                    'debug'     => true,
                    'url'       => 'simpleapp.dev',
                );

        2. database.php

                return array(
                    'connections' => array(
                        'mysql' => array(
                            'driver'    => 'mysql',
                            'host'      => 'localhost',
                            'database'  => 'yourdb',
                            'username'  => 'yourusername',
                            'password'  => 'yourpassword',
                            'charset'   => 'utf8',
                            'collation' => 'utf8_unicode_ci',
                            'prefix'    => '',
                        )
                    )
                );