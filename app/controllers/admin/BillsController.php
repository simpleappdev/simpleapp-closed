<?php namespace admin;

use View, Input, Redirect, Company, Bill, Carbon\Carbon, Lang, App;
use Response;

class BillsController extends \BaseController {

	public function index()
	{
		$bills = Bill::where('payed_date', '=', '0000-00-00 00:00:00')->orderBy('created_at')->paginate(20);

		return View::make('admin.bills.index', ['bills' => $bills]);
	}

	public function update_selected()
	{
		$bills = Bill::find(Input::get('payment'));

		foreach($bills as $bill){
			$bill->payed_date = Carbon::now();
			$bill->save();
		}

		return Redirect::back()->with(['message' => Lang::get('payment.check_payed', ['amount' => count($bills)])]);
	}

	public function mark_bill($bill_id, $time_type)
	{

		$bill = Bill::find($bill_id);

		switch($time_type){
			case 'now':
				$bill->payed_date = Carbon::now();
			break;

			case 'time':
//				$time = date('Y-m-d',strtotime();
//				$bill->payed_date = Carbon::dateStringToCarbon('payed_date');
			break;

			default:
				return App::abort(404);
			break;
		}

		$bill->save();

		return Redirect::to('admin/bills');
	}

	public function create()
	{
		//
	}

	public function store()
	{
		//
	}

	public function show($id)
	{


		$bill = Bill::find($id);

		return View::make('admin.bills.show', ['bill' => $bill]);
	}

	public function edit($id)
	{
		//
	}

	public function update($id)
	{
		//
	}

	public function destroy($id)
	{
		//
	}

}