<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;


class AddThemeColumnFromModulePages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('module_pages', function($table)
        {
            $table->renameColumn('theme', 'theme_id');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('module_pages', function(Blueprint $table)
		{
            $table->renameColumn('theme_id', 'theme');
        });
	}

}
