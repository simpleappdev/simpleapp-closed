<?php

class LoginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		if(Auth::check()) return View::make('user.account.user_company_mager');

		return View::make('user.account.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		

		$credentials = [
	        'email' => Input::get('email'),
	        'password' => Input::get('password')
	    ];
	
		if( Auth::attempt($credentials) ) return View::make('user.account.login_succes');
		
		return View::make('user.account.index');
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		dd(User::find(Auth::user()));
		$user->email = Input::get('email');
		$user->save();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		//Auth::logout();
		$email = Auth::user()->email;

		if (Auth::check())
		{
		    return $email;
		}

	}

}