<?php

return [
    'news_original_image_location' => '/images/news/original/',
    'news_thumbnail_image_location' => '/images/news/thumbnail/',
    'news_resized_image_location' => '/images/news/resized/',
    'news_default_image_location' => '/images/news/default/'
];