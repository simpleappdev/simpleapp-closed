<?php namespace Helpers;

use View;
use Lang;
use Company;

/*
the createTheme method will probably end up just rendering a template
    and that template will call the partials

    adds a bit more flexibility to use cases

all the methods have been moved over to render partials
    this allows for much easier styling and changing of the "generators content"

views: modules.theme.partials.*

would prefer this class to become a static helper if possible
if not will I will create a facade

 */

class ThemeBuilder
{

    // public $type;
    private $images;
    private $name;

    public $imagePreview = false;

    public static $divPreffix = '<div class="theme-row-block">';
    public static $divSuffix = '</div>';

    public $xyoptions = [
        'x' => ['left', 'center', 'right'],
        'y' => ['bottom', 'center', 'top']
    ];

    // probably should be top, right, bottom, left for css consistancy
    public $borderPositions = ['left', 'top', 'right', 'bottom'];

    public function __construct($name = null, $company_id, $images = array())
    {
        //$this->name = $name;
        $this->images = $images;
    }

    public function name($name = null)
    {
        if($name)
            $this->name = $name;
        return $this;
    }

    public static function wrap($type, $content)
    {
        return static::$divPreffix ."\n\t $content \n". static::$divSuffix ."\n";
    }

    public function generateFontColor($description, $name, $value = '255,255,255')
    {
        return View::make(
            'modules.theme.partials.generate-font-color',
            compact('description', 'name', 'value')
            )->render();
    }

    public function generateBackgroundImage($label, $description)
    {
        $images = $this->images;
        $name = $this->name;
        return View::make(
            'modules.theme.partials.generate-background-image',
            compact('label', 'description', 'images', 'name')
            )->render();
    }

    public function generateBackground($description, $type, $value = '255,255,255')
    {
        $this->imagePreview = true;
        $name = $this->name;
        return View::make(
            'modules.theme.partials.generate-background',
            compact('description', 'type', 'name', 'value')
            )->render();
    }

    public function generateFieldset($fields, $legendname)
    {
        $imagePreview = $this->imagePreview;
        return View::make(
            'modules.theme.partials.generate-fieldset',
            compact('fields', 'legendname', 'imagePreview')
            )->render();
    }

    /*
     * This function is for creating margin and padding fields
     */
    public function generateSpacing($label, $type, $value = null)
    {


        $name = $this->name;
        return View::make(
            'modules.theme.partials.generate-spacing',
            compact('label', 'input_field', 'type', 'value', 'name')
            )->render();
    }

    public function generateBorderRadius($label, $value = null)
    {
        $value = array('lt' => 0, 'rt' => 0, 'rb' => 0, 'lb' => 0, );

        $name = $this->name;

        return View::make(
            'modules.theme.partials.generate-border-radius',
            compact('value', 'name', 'label')
            )->render();
    }

    public function repeatImage($description, $type, $value = null)
    {
        $fields = array(
            'no-repeat' => 'Niet herhalen',
            'repeat-y' => 'Horizontaal herhalen',
            'repeat-x' => 'Verticaal herhalen',
            'repeat' => 'Herhalen in beide richtingen'
        );
        //print_r($fields);
        $name = $this->name;

        return View::make(
            'modules.theme.partials.repeat-image',
            compact('fields', 'description', 'name', 'value', 'type')
            )->render();
    }

    public function generateImagePosition($label, $position, $value = null)
    {
        if (array_key_exists($position, $this->xyoptions)) // isset($this->xyoptions[$position])
            $options = $this->xyoptions[$position];
        else
            throw new Exception('Position must be of value x or y');

        $name = $this->name;

        return View::make(
            'modules.theme.partials.generate-image-position',
            compact('name', 'position', 'value', 'options', 'type', 'label')
            )->render();
    }

    public function help()
    {
        return 'Developer mode enabled, function list will be shown';
    }

    public function generateBorder($input_name, $value = null){

        $styles = ['solid' => 'Solid', 'dashed' => 'Dashed', 'dotted' => 'Dotted'];
        $positions = ['Left', 'Top', 'Right', 'Bottom'];
        $view = '';

        foreach($positions AS $position){
            $title = $position;
            $position = strtolower($position);

            $view .= View::make(
                'modules.theme.partials.generate-border',
                compact('title','input_name','value', 'position', 'styles')
            )->render();
        }
        return $view;
    }


    public function createTheme($input_name, $data = null)
    {
        $this->name = $input_name;
        $theme = '';

        $theme .= $this->generateBorder(
            //Lang::get('themebuilder.borderTekst'),
            $input_name,
            $data['border']
        );

        $theme .= $this->generateFontColor(
            Lang::get('themebuilder.font_color'),
            $input_name
            //$input_name //. 'font-color'
            //$data['font-color']
        );
        $theme .= $this->generateSpacing(
            'Margin',
            //$input_name . '-margin',
            'margin',
            $data['margin']
        );
        $theme .= $this->generateSpacing(
            'Padding',
            // $input_name . '-padding',
            'padding',
            $data['padding']
        );
        $theme .= $this->generateFieldset(
            $this->generateBackgroundImage(
                'Image background',
                'background'
                // ,
                // $input_name . '-background-image'
            ).
            $this->repeatImage(
                'Herhalen van afbeelding',
                // $input_name . '-image-repeat',
                'repeat-image',
                $data['repeat-image']
            ).
            $this->generateBackground(
                'Achtergrond kleur',
                // $input_name . '-background-color',
                'background'
            ).
            $this->generateImagePosition('Horizontal Alignment', 'x') .
            $this->generateImagePosition('Vertical Alignment', 'y'),

            'Genereer achtergrond' //Fieldset value
        );
        $theme .= $this->generateBorderRadius('Border Radius');

        return $theme;
    }

    /**
     * Get all pages and Events
     */

    public function getThemeAbleItems(){

        $theme_able = array('None' => '');


        return $theme_able;
    }
}