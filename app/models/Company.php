<?php


class Company extends\Eloquent
{
    protected $softDelete = true;

    protected $guarded = ['id', 'created_at', 'updated_at'];

	protected $table = 'companies';

    public function subscriptions(){
        return $this->hasMany('Subscription')
            ->where('active', 1)
            ->orderBy('expire_date', 'asc');
    }

    public function users()
    {
        return $this->belongsToMany('User');
    }

    public function getSchedule(){
        return $this->hasMany('Schedule');
    }

    public function getSoundcloudSongs(){
        return $this->hasMany('Soundcloud');
    }

	public function saveSchedule(){
		return $this->belongsToMany('Schedule');//->withTimestamps();
	}

	public function getMenu(){
		return $this->belongsToMany('Menu');//->withTimestamps();
	}

	public function getAlbums(){
		return $this->hasMany('PhotoAlbum');
	}

    //Get branch of the company
	public function branch(){
		return $this->belongsTo('Branch');
	}

    //Gets the user account of the company owner
	public function user(){
		return $this->belongsTo('User');
	}


    //Gets all private images from the company
    public function privateImages(){
        return $this->hasMany('PrivateImage');
    }

    /**
     * @return mixed
     * This is checking or unchecking autogenerate XML or HTML pages after
     * editing
     */
    public function autogenerateModules(){
        return $this->belongsToMany('Module', 'modules_autogenerate');
    }

    public function modules(){
        return $this->belongsToMany('Module');
    }

    /***********************************************************************************************************/
    /**
     * Module Pages
     * Controller: modules/PagesController
     * Model: modules/Page.php
     */

    public function activePages(){
        return $this->hasMany('Page');
    }

    public function defaultTheme($company_id){
        $theme = Theme::where('company_id', '=', $company->id)
            ->where('default', '=', 1)
            ->first();

        if(isset($theme->id)){
            return $theme->id;
        }
    }

    /**
     * Module Gallery
     * Controller: modules/GalleryController
     * Model: Gallery
     */
    public function getGalleries(){
        return $this->hasMany('Gallery');
    }

    /**
     * Module Themes
     * Controller: modules/ThemeController
     * Model: Theme
     */
    public function themes(){
        return $this->hasMany('Theme');
    }

    /**
     * Module RenderApp
     * Controller: RenderAppController
     * Model: App
     */

    public function buttonsXML(){
        return $this->hasOne('SaveApp');
    }

    /**
     * Module Soundcloud
     * Controller: modules/SoundcloudController
     * Model: Soundcloud
     */

    public function getSoundcloud(){
        return $this->hasMany('Soundcloud');
    }

    /******************* Payments ********************/

    public function currentPlan(){
        return $this->hasOne('CompanyPlan');
    }

    public function bills($first_day, $last_day){
        return $this->hasMany('Bill')
        ->whereBetween('created_at', array($first_day, $last_day))
        ->limit(1)
        ->get();
    }

    public function payedBills(){
        return $this->hasMany('Bill')->where('payed_date', '>', '0000-00-00 00:00:00');
    }

    public function openBills(){
        return $this->hasMany('Bill')->where('payed_date', '=', null);
    }

    public function companyBillCron(){
        return $this->hasMany('Bill');

    }

    public function plan(){
        return $this->hasOne('CompanyPlan', 'company_id');
    }
}
